$(function () {
    var $gallery = $('.gallery a').simpleLightbox();

    $gallery.on('show.simplelightbox', function () {
            console.log('Requested for showing');
        })
        .on('shown.simplelightbox', function () {
            console.log('Shown');
        })
        .on('close.simplelightbox', function () {
            console.log('Requested for closing');
        })
        .on('closed.simplelightbox', function () {
            console.log('Closed');
        })
        .on('change.simplelightbox', function () {
            console.log('Requested for change');
        })
        .on('next.simplelightbox', function () {
            console.log('Requested for next');
        })
        .on('prev.simplelightbox', function () {
            console.log('Requested for prev');
        })
        .on('nextImageLoaded.simplelightbox', function () {
            console.log('Next image loaded');
        })
        .on('prevImageLoaded.simplelightbox', function () {
            console.log('Prev image loaded');
        })
        .on('changed.simplelightbox', function () {
            console.log('Image changed');
        })
        .on('nextDone.simplelightbox', function () {
            console.log('Image changed to next');
        })
        .on('prevDone.simplelightbox', function () {
            console.log('Image changed to prev');
        })
        .on('error.simplelightbox', function (e) {
            console.log('No image found, go to the next/prev');
            console.log(e);
        });
});


// testimonial
$('.testimonials .owl-carousel').owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    autoplay: false,
    items: 1
})

//youtube video
jQuery(function () {
    jQuery("a.bla-1").YouTubePopUp();
    jQuery("a.bla-2").YouTubePopUp({
        autoplay: 0
    }); // Disable autoplay
});



//app slider
$('.app-slides .owl-carousel').owlCarousel({
    loop: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 2000,
    nav: false,
    dots: false,
    items: 1
})


////navigation
$('.click-me').navScroll({
    navHeight: 0
});

$('.nav').on('click', '.nav-mobile', function (e) {
    e.preventDefault();
    $('.nav ul').slideToggle('fast');
});

///fixed header
$(function () {
    //caches a jQuery object containing the header element
    var header = $("header");
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 150) {
            header.addClass("fixedHeader");
        } else {
            header.removeClass("fixedHeader");
        }
    });
});


