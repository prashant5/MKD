-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 02, 2018 at 02:30 PM
-- Server version: 5.7.22
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MKD`
--

-- --------------------------------------------------------

--
-- Table structure for table `AdminOffer`
--

CREATE TABLE `AdminOffer` (
  `Id` int(11) NOT NULL,
  `OfferTitle` varchar(30) NOT NULL,
  `Icon` varchar(500) NOT NULL,
  `Description` varchar(256) NOT NULL,
  `Terms` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `Status` int(11) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  `OrgId` int(11) NOT NULL,
  `StoreId` int(11) NOT NULL,
  `AutoApply` int(11) NOT NULL,
  `MaxAvailLimit` int(11) NOT NULL,
  `MaxAmount` decimal(10,0) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `ProductCategoryId` int(11) NOT NULL,
  `ProductSubCategoryId` int(11) NOT NULL,
  `OfferType` varchar(30) NOT NULL,
  `OfferValue` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AdminSetting`
--

CREATE TABLE `AdminSetting` (
  `Id` int(11) NOT NULL,
  `ReferralAmount` decimal(10,0) NOT NULL,
  `ReferralExpiryDays` int(11) NOT NULL,
  `ReferralEnable` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AdminSetting`
--

INSERT INTO `AdminSetting` (`Id`, `ReferralAmount`, `ReferralExpiryDays`, `ReferralEnable`) VALUES
(1, '100', 10, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `AppVersion`
--

CREATE TABLE `AppVersion` (
  `Id` int(10) NOT NULL,
  `CurrentVersion` int(10) NOT NULL,
  `MinVersion` int(10) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AppVersion`
--

INSERT INTO `AppVersion` (`Id`, `CurrentVersion`, `MinVersion`, `CreateOn`) VALUES
(1, 1, 1, '2018-05-02 00:00:00'),
(2, 2, 1, '2018-05-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `Area`
--

CREATE TABLE `Area` (
  `id` int(11) NOT NULL,
  `StateId` int(10) NOT NULL,
  `CityId` int(10) NOT NULL,
  `AreaName` varchar(100) NOT NULL,
  `CreateOn` date NOT NULL,
  `UpdateOn` date NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Area`
--

INSERT INTO `Area` (`id`, `StateId`, `CityId`, `AreaName`, `CreateOn`, `UpdateOn`, `Status`) VALUES
(1, 1, 1, 'Vijay Nagar', '2018-05-07', '2018-05-07', 1),
(2, 1, 1, 'Geeta Bhawan', '2018-05-07', '2018-05-07', 1),
(3, 1, 2, 'MahakalNagar', '2018-05-07', '2018-05-07', 1),
(4, 1, 2, 'Vishnupuri', '2018-05-07', '2018-05-07', 1),
(5, 2, 3, 'Baliya', '2018-05-07', '2018-05-07', 1),
(6, 2, 4, 'Bhind', '2018-05-07', '2018-05-07', 1),
(7, 2, 4, 'Murena', '2018-05-07', '2018-05-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `City`
--

CREATE TABLE `City` (
  `id` int(11) NOT NULL,
  `CityName` varchar(100) NOT NULL,
  `StateId` int(11) NOT NULL,
  `CreateOn` date NOT NULL,
  `UpdateOn` date NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `City`
--

INSERT INTO `City` (`id`, `CityName`, `StateId`, `CreateOn`, `UpdateOn`, `Status`) VALUES
(1, 'Indore', 1, '2018-05-07', '2018-05-07', 1),
(2, 'Ujjain', 1, '2018-05-07', '2018-05-07', 1),
(3, 'Bihar', 2, '2018-05-07', '2018-05-07', 1),
(4, 'Kaanpur', 2, '2018-05-07', '2018-05-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ContactUs`
--

CREATE TABLE `ContactUs` (
  `Id` int(11) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `Email` varchar(256) NOT NULL,
  `ContactNum` varchar(20) NOT NULL,
  `Message` varchar(1000) NOT NULL,
  `AddedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ContactUs`
--

INSERT INTO `ContactUs` (`Id`, `Name`, `Email`, `ContactNum`, `Message`, `AddedOn`) VALUES
(1, 'Mann ka dabba', 'mannkadabba@gmail.com', '+919754743271', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `FAQ`
--

CREATE TABLE `FAQ` (
  `Id` int(11) NOT NULL,
  `Question` varchar(256) NOT NULL,
  `Answer` varchar(2000) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `Sequence` int(11) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Feedback`
--

CREATE TABLE `Feedback` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `ProductId` int(11) DEFAULT NULL,
  `StoreId` int(11) NOT NULL,
  `Message` varchar(1000) NOT NULL,
  `Rating` decimal(10,0) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `IsPrivate` int(11) NOT NULL,
  `OrderId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LocalArea`
--

CREATE TABLE `LocalArea` (
  `LocalAreaId` int(10) NOT NULL,
  `AreaId` int(10) NOT NULL,
  `LocalAreaName` varchar(300) NOT NULL,
  `Status` int(1) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `UpdateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LocalArea`
--

INSERT INTO `LocalArea` (`LocalAreaId`, `AreaId`, `LocalAreaName`, `Status`, `CreateOn`, `UpdateOn`) VALUES
(1, 0, 'OTHER', 1, '2018-06-02 00:00:00', '2018-06-02 00:00:00'),
(2, 1, 'Malviya Nagar', 1, '2018-06-02 00:00:00', '2018-06-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `MkdValidityPlan`
--

CREATE TABLE `MkdValidityPlan` (
  `ValidityId` int(10) NOT NULL,
  `MinAmount` varchar(20) NOT NULL,
  `MaxAmount` varchar(20) NOT NULL,
  `ValidityDays` varchar(10) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `UpdateOn` datetime NOT NULL,
  `Status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MkdValidityPlan`
--

INSERT INTO `MkdValidityPlan` (`ValidityId`, `MinAmount`, `MaxAmount`, `ValidityDays`, `CreateOn`, `UpdateOn`, `Status`) VALUES
(1, '100', '500', '10', '2018-05-23 00:00:00', '2018-06-02 00:00:00', b'1'),
(2, '501', '1000', '10', '2018-05-23 00:00:00', '2018-05-23 00:00:00', b'1'),
(3, '250', '', '3', '2018-05-23 00:00:00', '2018-05-23 00:00:00', b'1'),
(4, '1001', '1500', '15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1'),
(5, '1501', '2000', '20', '2018-05-24 00:00:00', '2018-05-24 00:00:00', b'1'),
(6, '2001', '2500', '25', '2018-05-24 00:00:00', '2018-05-24 00:00:00', b'1'),
(7, '2501', '3000', '30', '2018-05-24 00:00:00', '2018-05-24 00:00:00', b'1'),
(8, '3001', '4000', '45', '2018-05-24 00:00:00', '2018-05-24 00:00:00', b'1'),
(11, '4001', '4500', '50', '2018-05-29 00:00:00', '2018-05-29 00:00:00', b'0'),
(12, '4501', '5000', '55', '2018-05-29 00:00:00', '2018-05-29 00:00:00', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `OrderData`
--

CREATE TABLE `OrderData` (
  `Id` int(11) NOT NULL,
  `OrderCode` varchar(20) NOT NULL,
  `StoreId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `IsFavourite` int(11) NOT NULL,
  `OrderOn` datetime NOT NULL,
  `TotalAmount` decimal(10,0) NOT NULL,
  `GST` decimal(10,0) NOT NULL,
  `Tax` decimal(10,0) NOT NULL,
  `Status` varchar(15) NOT NULL,
  `OrderType` varchar(15) NOT NULL,
  `RequestRemark` varchar(100) DEFAULT NULL,
  `DeliveryCharge` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `OrderDelivery`
--

CREATE TABLE `OrderDelivery` (
  `Id` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `PickUpTime` datetime DEFAULT NULL,
  `AssignTo` int(11) NOT NULL,
  `DeliverTime` datetime DEFAULT NULL,
  `Distance` int(11) DEFAULT NULL,
  `DeliveryCharge` decimal(10,0) NOT NULL,
  `Status` varchar(20) NOT NULL,
  `Rating` decimal(10,0) DEFAULT NULL,
  `Feedback` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `OrderItem`
--

CREATE TABLE `OrderItem` (
  `Id` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Amount` decimal(10,0) NOT NULL,
  `Status` varchar(20) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `DiscountAmount` decimal(10,0) NOT NULL,
  `RequestRemark` varchar(100) DEFAULT NULL,
  `Quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `OrderItemOptionRelation`
--

CREATE TABLE `OrderItemOptionRelation` (
  `Id` int(11) NOT NULL,
  `OrderItemId` int(11) NOT NULL,
  `ProductOptionItemId` int(11) NOT NULL,
  `UpdatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `OrderPayment`
--

CREATE TABLE `OrderPayment` (
  `Id` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `PaymentType` varchar(20) NOT NULL,
  `PaymentOn` datetime NOT NULL,
  `PaymentCode` varchar(256) DEFAULT NULL,
  `UserPaymentAccountId` int(11) DEFAULT NULL,
  `ReferenceNo` varchar(100) DEFAULT NULL,
  `TotalAmount` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `OrderTracking`
--

CREATE TABLE `OrderTracking` (
  `Id` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `Latitude` varchar(30) NOT NULL,
  `Longitude` varchar(30) NOT NULL,
  `AddedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Organization`
--

CREATE TABLE `Organization` (
  `Id` int(11) NOT NULL,
  `Name` varchar(256) DEFAULT NULL,
  `NameAbbr` varchar(50) NOT NULL,
  `Logo` varchar(500) NOT NULL,
  `ContactPerson` varchar(50) NOT NULL,
  `Address` varchar(256) NOT NULL,
  `City` varchar(50) NOT NULL,
  `LandMark` varchar(50) NOT NULL,
  `State` varchar(40) NOT NULL,
  `Zip` varchar(20) NOT NULL,
  `WorkPhone` varchar(20) NOT NULL,
  `CellPhone` varchar(20) NOT NULL,
  `Fax` varchar(20) NOT NULL,
  `Email` varchar(256) NOT NULL,
  `SecondEmail` varchar(256) NOT NULL,
  `Country` varchar(50) NOT NULL,
  `CountryId` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `OrgType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Organization`
--

INSERT INTO `Organization` (`Id`, `Name`, `NameAbbr`, `Logo`, `ContactPerson`, `Address`, `City`, `LandMark`, `State`, `Zip`, `WorkPhone`, `CellPhone`, `Fax`, `Email`, `SecondEmail`, `Country`, `CountryId`, `Status`, `OrgType`) VALUES
(1, 'admin', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, 1, 1),
(2, 'Apna Sweets', '', '', '', 'Indore', 'indore', '', 'madhyapradesh', '', '', '', '', '', '', '', 1, 1, 1),
(3, 'mcdonalds', 'md', '', '', 'indore', 'indore', '', 'madhyapradesh', '', '', '', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `OrganizationType`
--

CREATE TABLE `OrganizationType` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Sequence` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `OrganizationType`
--

INSERT INTO `OrganizationType` (`Id`, `Name`, `Sequence`) VALUES
(1, 'Food', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Page_content`
--

CREATE TABLE `Page_content` (
  `id` int(11) NOT NULL,
  `page_title` varchar(50) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Page_content`
--

INSERT INTO `Page_content` (`id`, `page_title`, `content`) VALUES
(1, 'Privacy policy', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.'),
(2, 'Tersms&condition', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.'),
(3, 'About us', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.');

-- --------------------------------------------------------

--
-- Table structure for table `PauseResume`
--

CREATE TABLE `PauseResume` (
  `PRId` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `PauseDate` varchar(20) NOT NULL,
  `ResumeDate` varchar(20) NOT NULL,
  `pause_date` datetime NOT NULL,
  `resume_date` datetime NOT NULL,
  `Status` int(1) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PauseResume`
--

INSERT INTO `PauseResume` (`PRId`, `user_id`, `PauseDate`, `ResumeDate`, `pause_date`, `resume_date`, `Status`, `CreateOn`) VALUES
(1, 2, '30-MAY-2018', '02-JUN-2018', '2018-05-30 00:00:00', '2018-06-02 00:00:00', 0, '2018-05-28 19:29:15'),
(2, 3, '29-MAY-2018', '31-MAY-2018', '2018-05-29 00:00:00', '2018-05-31 00:00:00', 0, '2018-05-28 19:30:25'),
(3, 20, '31-MAY-2018', '01-JUN-2018', '2018-05-31 00:00:00', '2018-06-01 00:00:00', 0, '2018-05-30 17:05:47'),
(4, 27, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '2018-05-31 11:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `Product`
--

CREATE TABLE `Product` (
  `Id` int(11) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `ProductAbbr` varchar(20) NOT NULL,
  `ProductCode` varchar(20) NOT NULL,
  `IsFeatured` int(11) NOT NULL,
  `SEOContent` varchar(500) DEFAULT NULL,
  `ProductUrl` varchar(256) DEFAULT NULL,
  `Logo` varchar(256) NOT NULL,
  `Banner` varchar(256) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `AddedOn` datetime NOT NULL,
  `StoreId` int(11) DEFAULT NULL,
  `Amount` decimal(10,0) NOT NULL,
  `TakeOutAmount` decimal(10,0) NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  `IsMaster` bit(1) NOT NULL DEFAULT b'1' COMMENT 'If Master, then thali  by admin',
  `Description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Product`
--

INSERT INTO `Product` (`Id`, `CategoryId`, `Name`, `ProductAbbr`, `ProductCode`, `IsFeatured`, `SEOContent`, `ProductUrl`, `Logo`, `Banner`, `Status`, `AddedOn`, `StoreId`, `Amount`, `TakeOutAmount`, `UserId`, `IsMaster`, `Description`) VALUES
(1, 4, 'Daal Makhani', '', '', 0, NULL, NULL, '', '', 1, '2018-05-08 13:17:31', NULL, '100', '0', NULL, b'0', 'Full plate'),
(2, 4, 'Daal Tadka', '', '', 0, NULL, NULL, '', '', 1, '2018-05-08 13:19:43', NULL, '80', '0', NULL, b'0', 'Tadka daal'),
(3, 6, 'Jira rice', '', '', 0, NULL, NULL, '', '', 1, '2018-05-08 13:20:00', NULL, '120', '0', NULL, b'0', 'full jira'),
(4, 6, 'Plain rice', '', '', 0, NULL, NULL, '', '', 1, '2018-05-08 13:20:56', NULL, '90', '0', NULL, b'0', 'small size plate'),
(5, 4, 'Daal kadhayi', '', '', 0, NULL, NULL, '', '', 1, '2018-05-09 11:33:15', NULL, '210', '0', NULL, b'1', 'nice for all'),
(6, 4, 'Plain daal', '', '', 0, NULL, NULL, '', '', 1, '2018-05-09 11:33:49', NULL, '150', '0', NULL, b'1', 'tuar daal'),
(7, 4, 'Rosted Daal', '', '', 0, NULL, NULL, '', '', 1, '2018-05-09 11:34:12', NULL, '80', '0', NULL, b'1', 'Roasted daal'),
(11, 5, 'Plain Roti', '', '', 0, '', '', '', '', 1, '2018-05-19 11:03:15', 0, '2', '0', 0, b'1', '2 per roti'),
(12, 5, 'Butter Roti', '', '', 0, '', '', '', '', 1, '2018-06-02 08:00:14', 0, '5', '0', 0, b'1', 'per roti'),
(13, 5, 'Naan', '', '', 0, '', '', '', '', 1, '2018-06-01 07:26:37', 0, '12', '0', 0, b'1', 'per naan 12 rs'),
(14, 5, 'Paratha', '', '', 0, '', '', '', '', 1, '2018-06-01 07:26:37', 0, '16', '0', 0, b'1', 'half ');

-- --------------------------------------------------------

--
-- Table structure for table `ProductCategory`
--

CREATE TABLE `ProductCategory` (
  `Id` int(11) NOT NULL,
  `CategoryName` varchar(50) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `Sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ProductCategory`
--

INSERT INTO `ProductCategory` (`Id`, `CategoryName`, `AddedOn`, `Sequence`) VALUES
(4, 'Daal', '2018-05-08 00:00:00', 1),
(5, 'Roti', '2018-05-08 00:00:00', 2),
(6, 'Rice', '2018-05-08 00:00:00', 3),
(7, 'Vegitables', '2018-05-08 00:00:00', 4);

-- --------------------------------------------------------

--
-- Table structure for table `ProductImages`
--

CREATE TABLE `ProductImages` (
  `Id` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `Title` varchar(256) DEFAULT NULL,
  `ContentType` varchar(50) DEFAULT NULL,
  `FileUrl` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ProductSubCategory`
--

CREATE TABLE `ProductSubCategory` (
  `Id` int(11) NOT NULL,
  `SubCategoryName` varchar(50) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `CategoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ProductSubCategory`
--

INSERT INTO `ProductSubCategory` (`Id`, `SubCategoryName`, `AddedOn`, `CategoryId`) VALUES
(1, 'Fast food ', '2018-04-12 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Promotions`
--

CREATE TABLE `Promotions` (
  `Id` int(11) NOT NULL,
  `Title` varchar(256) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `Status` int(1) NOT NULL,
  `BannerUrl` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Promotions`
--

INSERT INTO `Promotions` (`Id`, `Title`, `AddedOn`, `Status`, `BannerUrl`) VALUES
(1, '', '2018-05-23 00:00:00', 1, 'e2d9f7fa749302209ac982060eaedae0.jpg'),
(2, '', '2018-05-23 00:00:00', 1, '8d3bb93db30721ec0305c2fba5e5ae99.jpeg'),
(3, '', '2018-05-23 00:00:00', 1, '345643dc9d5a4c94ff5eaa1d50f58597.jpg'),
(4, '', '2018-05-23 00:00:00', 1, 'e2d9f7fa749302209ac982060eaedae01.jpg'),
(5, '', '2018-05-23 00:00:00', 1, '8d3bb93db30721ec0305c2fba5e5ae991.jpeg'),
(6, '', '2018-05-23 00:00:00', 1, '345643dc9d5a4c94ff5eaa1d50f585971.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ReferralByDeviceId`
--

CREATE TABLE `ReferralByDeviceId` (
  `Id` int(10) NOT NULL,
  `Refer_user_id` int(10) NOT NULL,
  `Referral_user_id` int(10) NOT NULL,
  `Device_id` varchar(100) NOT NULL DEFAULT '',
  `Refer_Code` varchar(10) NOT NULL,
  `Refer_Amount` varchar(20) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ReferralByDeviceId`
--

INSERT INTO `ReferralByDeviceId` (`Id`, `Refer_user_id`, `Referral_user_id`, `Device_id`, `Refer_Code`, `Refer_Amount`, `CreateOn`) VALUES
(1, 3, 4, '11111111111111111', 'MKD189521', '40', '2018-05-23 16:26:18'),
(2, 20, 21, '9ae99f901a195166', 'MKD41104', '286.5', '2018-05-30 18:09:14'),
(3, 21, 22, '9ae99f901a195166', 'MKD96615', '10', '2018-05-30 18:23:03'),
(4, 22, 23, '9ae99f901a195166', 'MKD68260', '10', '2018-05-30 18:59:13'),
(5, 19, 27, '9ae99f901a195166', 'MKD37930', '10', '2018-05-31 10:53:30'),
(6, 27, 28, '9ae99f901a195166', 'MKD24732', '10', '2018-05-31 11:13:18'),
(7, 19, 29, 'e8327967ec53e729', 'MKD37930', '10', '2018-05-31 11:19:59'),
(8, 31, 32, 'e8327967ec53e729', 'MKD52147', '10', '2018-05-31 12:57:03');

-- --------------------------------------------------------

--
-- Table structure for table `State`
--

CREATE TABLE `State` (
  `id` int(11) NOT NULL,
  `StateName` varchar(100) NOT NULL,
  `CreateOn` date NOT NULL,
  `UpdateOn` date NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `State`
--

INSERT INTO `State` (`id`, `StateName`, `CreateOn`, `UpdateOn`, `Status`) VALUES
(1, 'Madhya Pradesh', '2018-05-07', '2018-05-07', 1),
(2, 'Uttar Pradesh', '2018-05-07', '2018-05-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Store`
--

CREATE TABLE `Store` (
  `Id` int(11) NOT NULL,
  `OrgId` int(11) NOT NULL,
  `Orgadmin_id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `NameAbbr` varchar(30) DEFAULT NULL,
  `Image` varchar(255) NOT NULL,
  `LocationDesc` varchar(256) DEFAULT NULL,
  `Address` varchar(50) DEFAULT NULL,
  `LandMark` varchar(30) DEFAULT NULL,
  `City` varchar(30) DEFAULT NULL,
  `State` varchar(30) DEFAULT NULL,
  `Zip` varchar(10) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Email` varchar(256) DEFAULT NULL,
  `LocationCode` varchar(30) NOT NULL,
  `Sequence` int(11) DEFAULT NULL,
  `ManagerId` int(11) DEFAULT NULL,
  `Country` varchar(50) DEFAULT NULL,
  `Latitude` varchar(30) DEFAULT NULL,
  `Longitude` varchar(30) DEFAULT NULL,
  `AddedOn` datetime NOT NULL,
  `Status` int(11) NOT NULL,
  `BannerUrl` varchar(500) DEFAULT NULL,
  `OpenTime` varchar(10) NOT NULL,
  `CloseTime` varchar(10) NOT NULL,
  `IsOpen` int(1) NOT NULL COMMENT '0=open,1=close'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Store`
--

INSERT INTO `Store` (`Id`, `OrgId`, `Orgadmin_id`, `Name`, `NameAbbr`, `Image`, `LocationDesc`, `Address`, `LandMark`, `City`, `State`, `Zip`, `Phone`, `Email`, `LocationCode`, `Sequence`, `ManagerId`, `Country`, `Latitude`, `Longitude`, `AddedOn`, `Status`, `BannerUrl`, `OpenTime`, `CloseTime`, `IsOpen`) VALUES
(1, 2, 2, 'Indore Food Junction', NULL, '', NULL, 'indore', NULL, 'indore', 'mp', NULL, '9617822121', 'indore@gmail.com', '', NULL, NULL, NULL, '', '', '2018-04-26 13:01:36', 1, NULL, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `SubscriptionPlan`
--

CREATE TABLE `SubscriptionPlan` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `WeedDay` int(1) NOT NULL,
  `MainCategory` int(1) NOT NULL COMMENT '1=break fast, 2=lunch, 3=dinner',
  `Price` varchar(20) NOT NULL,
  `ValidTillMonthCount` varchar(20) NOT NULL,
  `ProductCategory` text NOT NULL,
  `ProductCategoryQty` varchar(250) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `Status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SubscriptionPlan`
--

INSERT INTO `SubscriptionPlan` (`id`, `Name`, `WeedDay`, `MainCategory`, `Price`, `ValidTillMonthCount`, `ProductCategory`, `ProductCategoryQty`, `CreateOn`, `Status`) VALUES
(1, 'STANDARD MEAL', 0, 2, '1200', '1', 'Daal,Rice', '2,1', '2018-05-15 08:14:14', 1),
(2, 'EXECUTIVE MEAL', 0, 2, '1250', '2', 'Daal', '1', '2018-05-15 08:15:12', 1),
(3, 'PREMIUM THALI', 0, 2, '1500', '1', 'Daal,Rice', '1,2', '2018-05-15 08:16:04', 1),
(4, 'STANDARD MEAL', 0, 3, '1200', '1', 'Daal,Rice', '1,1', '2018-05-15 14:15:31', 1),
(5, 'EXECUTIVE MEAL', 0, 3, '1250', '1', 'Daal,Rice,Roti', '2,0,1', '2018-05-18 13:39:53', 1),
(6, 'PREMIUM THALI', 0, 3, '1500', '1', 'Vegitables,Daal,Rice,Roti', '2,1,1,0', '2018-05-18 13:40:51', 1),
(7, '', 0, 1, '850', '1', '', '', '2018-05-18 14:03:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `TiffenProductSchedule`
--

CREATE TABLE `TiffenProductSchedule` (
  `Id` int(11) NOT NULL,
  `WeekDay` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `IsInBreakFast` bit(1) NOT NULL,
  `IsInLunch` bit(1) NOT NULL,
  `IsInDinner` bit(1) NOT NULL,
  `IsEnabled` bit(1) NOT NULL,
  `AddedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Admin Thali';

--
-- Dumping data for table `TiffenProductSchedule`
--

INSERT INTO `TiffenProductSchedule` (`Id`, `WeekDay`, `ProductId`, `IsInBreakFast`, `IsInLunch`, `IsInDinner`, `IsEnabled`, `AddedOn`) VALUES
(1, 1, 1, b'1', b'0', b'0', b'1', '2018-05-09 12:49:39'),
(2, 1, 3, b'1', b'0', b'0', b'1', '2018-05-09 12:49:39'),
(3, 1, 7, b'0', b'1', b'0', b'1', '2018-05-09 12:51:02'),
(4, 1, 5, b'0', b'1', b'0', b'1', '2018-05-09 12:51:02'),
(5, 1, 4, b'0', b'1', b'0', b'1', '2018-05-09 12:51:02'),
(6, 1, 3, b'0', b'1', b'0', b'1', '2018-05-09 12:51:02'),
(7, 1, 1, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(8, 1, 6, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(9, 1, 2, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(10, 1, 7, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(11, 1, 3, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(12, 1, 4, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14');

-- --------------------------------------------------------

--
-- Table structure for table `TiffenTiming`
--

CREATE TABLE `TiffenTiming` (
  `Id` int(11) NOT NULL,
  `DayTime` int(1) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `Image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TiffenTiming`
--

INSERT INTO `TiffenTiming` (`Id`, `DayTime`, `StartTime`, `EndTime`, `Image`) VALUES
(1, 1, '09:00:00', '11:30:00', 'e2d9f7fa749302209ac982060eaedae0.jpg'),
(2, 2, '12:15:00', '15:00:00', '8d3bb93db30721ec0305c2fba5e5ae99.jpeg'),
(3, 3, '19:00:00', '21:30:00', '345643dc9d5a4c94ff5eaa1d50f58597.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `TiffinStartEndStatus`
--

CREATE TABLE `TiffinStartEndStatus` (
  `Id` int(10) NOT NULL,
  `UserId` int(10) NOT NULL,
  `WeekDay` int(1) NOT NULL,
  `BreakFast` int(1) NOT NULL COMMENT '1=on',
  `Lunch` int(1) NOT NULL COMMENT '1=on',
  `Dinner` int(1) NOT NULL COMMENT '1=on',
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TiffinStartEndStatus`
--

INSERT INTO `TiffinStartEndStatus` (`Id`, `UserId`, `WeekDay`, `BreakFast`, `Lunch`, `Dinner`, `CreateOn`) VALUES
(1, 2, 1, 0, 0, 0, '2018-05-25 13:38:27'),
(2, 2, 2, 0, 0, 0, '2018-05-25 13:41:45'),
(3, 2, 3, 0, 0, 0, '2018-05-25 13:43:22'),
(4, 3, 1, 1, 1, 1, '2018-06-02 19:22:30'),
(5, 3, 2, 1, 1, 1, '2018-06-02 18:25:20'),
(6, 3, 3, 1, 1, 1, '2018-05-28 19:16:05'),
(7, 3, 4, 1, 1, 1, '2018-05-28 19:16:06'),
(8, 3, 5, 1, 1, 1, '2018-05-25 19:20:50'),
(9, 3, 6, 1, 1, 1, '2018-06-01 13:25:09'),
(10, 3, 7, 1, 1, 1, '2018-05-28 18:48:54'),
(11, 2, 4, 0, 0, 0, '0000-00-00 00:00:00'),
(12, 2, 5, 0, 0, 0, '0000-00-00 00:00:00'),
(13, 2, 6, 0, 0, 0, '0000-00-00 00:00:00'),
(14, 2, 7, 0, 0, 0, '0000-00-00 00:00:00'),
(15, 4, 1, 0, 0, 0, '2018-05-28 17:30:46'),
(16, 4, 2, 0, 0, 0, '2018-05-28 17:30:46'),
(17, 4, 3, 0, 0, 0, '2018-05-28 17:30:46'),
(18, 4, 4, 0, 0, 0, '2018-05-28 17:30:46'),
(19, 4, 5, 0, 0, 0, '2018-05-28 17:30:46'),
(20, 4, 6, 0, 0, 0, '2018-05-28 17:30:46'),
(21, 4, 7, 0, 0, 0, '2018-05-28 17:30:46'),
(22, 20, 1, 0, 1, 1, '2018-05-30 17:05:05'),
(23, 20, 2, 0, 1, 1, '2018-05-30 17:05:04'),
(24, 20, 3, 0, 1, 1, '2018-05-30 17:05:03'),
(25, 20, 4, 0, 1, 1, '2018-05-30 17:05:02'),
(26, 20, 5, 0, 1, 1, '2018-05-30 17:05:00'),
(27, 20, 6, 1, 1, 1, '2018-05-30 17:01:17'),
(28, 20, 7, 1, 1, 1, '2018-05-30 17:01:17'),
(29, 27, 2, 1, 0, 0, '2018-05-31 11:30:03'),
(30, 27, 3, 1, 1, 0, '2018-05-31 11:30:09'),
(31, 27, 4, 1, 1, 1, '2018-05-31 11:30:13'),
(32, 27, 5, 1, 1, 0, '2018-05-31 11:30:07'),
(33, 27, 6, 1, 0, 0, '2018-05-31 11:30:05'),
(34, 27, 7, 1, 0, 0, '2018-05-31 11:30:06'),
(35, 19, 7, 0, 0, 0, '2018-05-31 12:46:42'),
(36, 19, 6, 0, 1, 0, '2018-05-31 12:46:52'),
(37, 19, 3, 1, 1, 1, '2018-05-31 12:46:51'),
(38, 19, 4, 1, 1, 1, '2018-05-31 12:46:52'),
(39, 19, 5, 1, 0, 0, '2018-05-31 12:46:47'),
(40, 19, 6, 0, 1, 0, '2018-05-31 12:46:52'),
(41, 19, 7, 0, 0, 0, '0000-00-00 00:00:00'),
(42, 19, 2, 1, 1, 1, '2018-05-31 12:46:52'),
(43, 19, 1, 1, 1, 1, '2018-05-31 12:46:51');

-- --------------------------------------------------------

--
-- Table structure for table `TiffinUserWeeklyMeal`
--

CREATE TABLE `TiffinUserWeeklyMeal` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `WeekDay` int(11) NOT NULL,
  `SubscriptionId` int(11) NOT NULL,
  `OrderDetail` text NOT NULL,
  `IsEnabled` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `mobileno` varchar(10) DEFAULT NULL,
  `emailid` varchar(256) NOT NULL,
  `dob` varchar(12) NOT NULL,
  `gender` int(1) NOT NULL COMMENT '1=men, 2=women',
  `isemailverified` int(1) NOT NULL,
  `ismobileverified` int(1) NOT NULL,
  `password` varchar(256) NOT NULL,
  `mobileverificationcode` varchar(100) NOT NULL,
  `emailverificationcode` varchar(100) NOT NULL,
  `pass_code` varchar(55) NOT NULL,
  `registrationOn` datetime NOT NULL,
  `updateOn` datetime NOT NULL,
  `token` varchar(100) NOT NULL,
  `devicetype` varchar(10) NOT NULL,
  `devicetoken` varchar(256) NOT NULL,
  `deviceid` varchar(50) NOT NULL,
  `social_id` varchar(55) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `colorFlag` varchar(20) NOT NULL DEFAULT 'Green',
  `ReferCode` varchar(10) NOT NULL,
  `ReferralCode` varchar(10) NOT NULL,
  `wallet` varchar(20) NOT NULL,
  `ReferWallet` varchar(20) NOT NULL,
  `ValidityCount` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `image`, `mobileno`, `emailid`, `dob`, `gender`, `isemailverified`, `ismobileverified`, `password`, `mobileverificationcode`, `emailverificationcode`, `pass_code`, `registrationOn`, `updateOn`, `token`, `devicetype`, `devicetoken`, `deviceid`, `social_id`, `status`, `latitude`, `longitude`, `colorFlag`, `ReferCode`, `ReferralCode`, `wallet`, `ReferWallet`, `ValidityCount`) VALUES
(1, 'Admin', '', '', NULL, 'admin@gmail.com', '', 0, 1, 1, '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', '', 1, '', '', 'Green', '', '', '', '', 0),
(2, 'Saurabh', '', '', '8251024168', 'advsaurabhnayak@gmail.com', '', 0, 0, 1, 'e28986ae058832d9bda78295760b17a1c465102e', '', '', '', '2018-05-14 23:06:10', '2018-05-14 23:06:47', '1f56f50565c78923fb04ad6ece7a630568ce86e71526319407220', 'android', 'fsAFNd3Oa0M:APA91bFl4XalBfo66Q8wPEDXs1EaVqdINe7COfcWkDhErRzxkwrdMJxGFAi35BVpSW7ynWXbjoHq436GHgoQn0N3HoEjbiMPgBihkUaGcSBSjldiwN0c5-eN33-lM1n6fHjHwsOP_C7s', 'ae15f9792808c9af', '', 1, '', '', 'Green', 'MKD221512', '', '1000', '', 18),
(3, 'Mukesh Patidar', '', '0f54ce37c9fdbcad4ee57b7a28b3570a.jpeg', '9926020798', 'mukesh.patidar23@gmail.com', '23-MAY-2018', 1, 0, 1, '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '', '', '2018-05-15 12:55:11', '2018-05-30 17:20:23', '2931964555f42999ce4420536366ace41527681023768', 'android', 'coKmUV4qb84:APA91bFdXD-R2ZM4IV9Fx4vbYI9szblPRekNnVheAeIKCMLgoaRobCqN8YEH5-AsEoI5Sdb-FwuOaBUDpRqNYrgzu91-wlfh2_biTIptJBSd81oh0LKWQE6qgy1KTTNNwWlO4Tn6tax8', '312e85104597bb94', '', 1, '', '', 'Green', 'MKD189521', '', '900', '', 85),
(4, 'Yash', '', '', '9754743271', 'yatindra.mohite@ebabu.co', '2018-04-30', 1, 0, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '7f549aa8061193a4fc84d1b26eb59fdc99316d3ba68d672ce6', '', '2018-05-17 18:56:03', '2018-05-31 15:17:08', '0cbfea3853181265a83256b95685f90f15270697796301111111', '', '', '11111111111111111', '', 1, '', '', 'Green', 'MKD177555', 'MKD189521', '', '', 0),
(5, 'Yatindra Mohite', '', '', '9754743274', 'yatindra.mohite1@ebabu.co', '', 0, 0, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-18 12:16:13', '2018-05-17 18:56:10', '', '', '', '', '', 1, '', '', 'Green', 'MKD977923', '', '', '', 0),
(6, 'Yatindra Mohite', '', '', '9754743275', 'yatindra.mohite1@ebabu.co', '', 0, 0, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-18 12:21:55', '2018-05-18 12:21:55', '', '', '', '', '', 1, '', '', 'Green', 'MKD2631556', '', '', '', 0),
(7, 'Yatindra Mohite', '', '', '810609062', 'yatindra.mohite1@ebabu.co', '', 0, 0, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-18 12:31:00', '2018-05-18 12:31:00', '', '', '', '', '', 1, '', '', 'Green', 'MKD1526626', '', '', '', 0),
(8, 'Jhalak', '', '', '1234567890', '123@gmail.com', '', 0, 0, 1, '307bfd23c8b34fc3709d515f5713a5ad15b2e35c', '', '', '', '2018-05-23 12:29:39', '2018-05-23 12:29:46', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD88154', '', '', '', 0),
(9, 'Bhy', '', '', '9926020791', 'jhalak@.com', '', 0, 0, 0, '20eabe5d64b0e216796e834f52d61fd0b70332fc', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-23 11:48:57', '2018-05-23 11:48:45', '', '', '', '', '', 1, '', '', 'Green', 'MKD13743', '', '', '', 0),
(10, 'Jhalak', '', '', '8959034121', '123@gmail.com', '', 0, 0, 0, '00d6d25fe1e933d9c533dcc3c5768c570f24100c', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-23 11:56:18', '2018-05-23 11:51:48', '', '', '', '', '', 1, '', '', 'Green', 'MKD42754', '', '', '', 0),
(11, 'Jhalak', '', '', '8888669839', '123@.com', '', 0, 0, 0, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-23 11:55:59', '2018-05-23 11:55:56', '', '', '', '', '', 1, '', '', 'Green', 'MKD83767', '', '', '', 0),
(12, 'Jhgf', '', '', '9926020799', 'mukesh@eb.co', '', 0, 0, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-23 11:59:16', '2018-05-23 11:59:49', '', '', '', '', '', 1, '', '', 'Green', 'MKD24649', '', '', '', 0),
(13, 'Jhalak Mathur', '', 'f56029b3999866f458f2334b5775cb86.jpeg', '8959034121', 'ramakant.more@ebabu.co', '23-MAY-2018', 2, 0, 1, '20d75fe135fc3abc15aee2f6e4657c3107899d6a', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '9a54de5ae2a46a31829b0791d36b62d096c6f9f06cd4d2a1ee', '', '2018-05-23 12:00:21', '2018-05-23 12:17:34', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD73099', '', '', '', 0),
(14, 'Jhalak Mathur', '', '', '8959034120', 'jhalak@ebabu.co', '', 0, 0, 1, '20d75fe135fc3abc15aee2f6e4657c3107899d6a', '', '', '', '2018-05-23 12:24:13', '2018-05-23 12:24:20', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD92420', '', '', '', 0),
(15, 'Mathur', '', '', '8888888888', 'mathur@.com', '', 0, 0, 1, 'ad45f77a87ec61891433b8c6b1b0d1d782c28405', '', '', '', '2018-05-23 12:32:17', '2018-05-23 12:32:22', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD67775', '', '', '', 0),
(16, 'Mathur', '', '', '8888800000', 'jhalak@gmail.com', '', 0, 0, 1, '0608d5a657defd3599c8080dd326bb1431143a8e', '', '', '', '2018-05-23 12:35:20', '2018-05-23 12:35:23', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD46019', '', '', '', 0),
(17, 'Harshita', '', '', '6785445045', '6255@gmail.yuy', '', 0, 0, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-30 16:17:40', '2018-05-30 16:17:40', '', '', '', '', '', 1, '', '', 'Green', 'MKD20545', '', '', '', 0),
(18, 'Harshita', '', '', '8108770110', '6255@gmail.yuy', '', 0, 0, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-30 16:18:01', '2018-05-30 16:18:01', '', '', '', '', '', 1, '', '', 'Green', 'MKD32954', '', '', '', 0),
(19, 'Harshita', '', '8864df129405ce11a2ad8eae0fc889a5.jpeg', '8770810110', 'harshita@ebabu.co', '30-MAY-2005', 2, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '037f2a57ab751ef1fdf42c56151e36ba6742be8c2f21d84796', '', '2018-05-30 17:49:18', '2018-05-31 13:44:28', '', 'android', '', 'e8327967ec53e729', '', 1, '', '', 'Green', 'MKD37930', 'MKD41104', '651', '20', 13),
(20, 'Harshita', '', 'ae52bb4601d92aff8c5546e33ea8a95b.jpeg', '8878725953', 'harshitag112@gmail.com', '02-MAY-2018', 2, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '322a7d23957a3e1b394b9441e57df1899de74574a690623c2eb9536353f1b9c1', '', '2018-05-30 16:19:35', '2018-05-31 15:33:25', '', 'android', '', 'e8327967ec53e729', '', 1, '', '', 'Green', 'MKD41104', '', '19107', '286.5', 243),
(21, 'Harshita', '', '', '8770810111', 'harshita08gupta@gmail.com', '', 0, 0, 1, '65e21ea0de8852abc2b0d821c1f9ac6f2cd5bd98', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '5a9cf59581e08b4a80cdd586b39c3b915b2faa9ebdbb768e72', '', '2018-05-30 18:04:03', '2018-05-31 10:22:55', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD96615', 'MKD41104', '7438', '10', 83),
(22, 'ASR', '', '91c77c7b0efc181b54a583b93a571e7c.jpeg', '8878800000', 'harshitag112@gmail.com', '30-MAY-2018', 2, 0, 1, '93ec71b22793a81569c94ca17e4d9c293d8e201f', '', '2f505528ce29f3f45e7cbbad5377ed26a4c951c7e407dc1b29', '', '2018-05-30 18:20:47', '2018-05-30 18:36:57', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD68260', 'MKD96615', '450', '10', 23),
(23, 'Aarohi', '', '', '8585858585', 'yuv@gmail.com', '', 0, 0, 1, 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '', '', '', '2018-05-30 18:58:30', '2018-05-30 18:58:34', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD39376', 'MKD68260', '2406', '', 38),
(24, 'BCD', '', '', '7878787878', 'gv@gmail.com', '', 0, 0, 1, 'd94674574abba19a9acb36bba19fed9a80b3d174', '', '', '', '2018-05-30 19:05:58', '2018-05-30 19:08:47', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD54434', '', '250', '', 3),
(25, 'Rto', '', '', '8989898989', 'rto@gmaip.com', '', 0, 0, 1, '9e421e13239d14b78a1df2febaddc86b6a333e42', '', '', '', '2018-05-30 19:10:38', '2018-05-30 19:10:42', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD33922', 'MKD54434', '250', '', 3),
(26, 'Abc', '', '', '8888888881', 'a@gmail.com', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 10:27:54', '2018-05-31 10:30:45', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD82748', 'MKD37930', '250', '', 3),
(27, 'Cdc', '', '', '7777777777', 'cdc_@gmail.com', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 10:48:53', '2018-05-31 11:22:13', '', 'android', '', 'e8327967ec53e729', '', 1, '', '', 'Green', 'MKD24732', 'MKD37930', '350', '10', 13),
(28, 'vp', '', '', '3333333333', 'vp1@gmail.com', '', 0, 0, 1, 'b6312143db81a47a18b1649e5afb73e271fd2c7f', '', '', '', '2018-05-31 11:10:21', '2018-05-31 11:13:08', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD58512', 'MKD24732', '100', '', 10),
(29, 'AAA', '', '', '4444444444', 'z@yahoo.co', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 11:19:46', '2018-05-31 11:19:52', '', 'android', '', 'e8327967ec53e729', '', 1, '', '', 'Green', 'MKD63659', 'MKD37930', '100', '', 10),
(30, 'Ebabu', '', 'f7ad21684fdc07d44f76bd33a2c77f3b.jpeg', '8888888800', 'harshita@ebabu.co', '01-MAY-2018', 2, 0, 1, '8abc8798cc81b54c4fb40e4da43158172214ed3f', '', '5449d949edd9fe53d4a209919b921402237ee6f56d23418da8', '', '2018-05-31 11:59:12', '2018-05-31 12:03:21', '', 'android', '', '95be419c317c00c5', '', 1, '', '', 'Green', 'MKD43219', 'MKD37930', '', '', 0),
(31, 'Raj', '', '', '9188787259', 'cdc_@gmail.com', '', 0, 0, 1, '65e21ea0de8852abc2b0d821c1f9ac6f2cd5bd98', '', '', '', '2018-05-31 12:43:24', '2018-05-31 12:43:29', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD52147', 'MKD24732', '200', '10', 10),
(32, 'Uday', '', '', '9999999999', '44__@gmail.co', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 12:55:37', '2018-05-31 12:56:49', '', 'android', '', 'e8327967ec53e729', '', 1, '', '', 'Green', 'MKD84258', 'MKD52147', '300', '', 30),
(33, 'H', '', '', '7575757575', 'f@g.cm', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 13:25:52', '2018-05-31 13:26:09', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD45012', 'MKD84258', '', '', 0),
(34, 'J', '', '', '7777777770', 'f@gmaol.com', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 13:28:12', '2018-05-31 13:28:18', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD24932', 'MKD45012', '', '', 0),
(35, 'H', '', '', '8878782595', 'h@gmail.co', '', 0, 0, 0, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '', '', '2018-05-31 13:42:11', '2018-05-31 13:42:11', '', '', '', '', '', 1, '', '', 'Green', 'MKD95564', 'MKD41104', '', '', 0),
(36, 'S', '', '', '8000000008', 't@gmail.com', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 13:45:41', '2018-05-31 13:45:46', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD74486', 'MKD41104', '', '', 0),
(37, 'R', '', '', '8800000088', 'u@5gmail.com', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 13:47:42', '2018-05-31 13:53:06', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD31256', 'MKD41104', '100', '', 10),
(38, 'Q', '', '', '8880000000', 'ft@gsn4.dh', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 13:48:57', '2018-05-31 13:51:11', '', 'android', '', '9ae99f901a195166', '', 1, '', '', 'Green', 'MKD71915', 'MKD41104', '100', '', 10),
(39, 'Neha', '', '', '7777777888', 'harshita@ebabu.co', '', 0, 0, 1, '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', '', '2018-05-31 15:36:56', '2018-05-31 15:37:07', 'd17f48c1b8e4eab1edf10fe7193dbd2c2fa3d1551527761227700', 'android', 'cOGssxGSYwA:APA91bEZezNeLrr9aNQ7fUi3e8w5YvyOW1LZSIxmUSApKSvSnA9NxeWy03XrVHZXCrurl1qGF-1rvsqjD4_u6hsdLvi4pecTf0o-K73J19zBfsLY1weCxRPLhtjYGftr1mGX2p3_MK5m', 'e8327967ec53e729', '', 1, '', '', 'Green', 'MKD77588', '', '1247', '', 15);

-- --------------------------------------------------------

--
-- Table structure for table `userAddress`
--

CREATE TABLE `userAddress` (
  `Id` int(11) NOT NULL,
  `Address` varchar(256) NOT NULL,
  `HouseNo` varchar(50) NOT NULL,
  `LandMark` varchar(100) NOT NULL,
  `AddressTitle` varchar(50) NOT NULL DEFAULT 'Home',
  `CityState` varchar(50) NOT NULL,
  `LocalityId` int(11) NOT NULL,
  `Latitude` varchar(30) NOT NULL,
  `Longitude` varchar(30) NOT NULL,
  `UserId` int(11) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  `StoreId` int(11) NOT NULL,
  `IsPrimaryAddress` bit(1) NOT NULL,
  `State` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userAddress`
--

INSERT INTO `userAddress` (`Id`, `Address`, `HouseNo`, `LandMark`, `AddressTitle`, `CityState`, `LocalityId`, `Latitude`, `Longitude`, `UserId`, `AddedOn`, `UpdatedOn`, `StoreId`, `IsPrimaryAddress`, `State`) VALUES
(1, '616, shekhar central old palasia', '', '', 'Home', '1', 1, '25.123566', '75.123456', 2, '2018-05-14 23:09:00', '2018-06-02 18:22:54', 1, b'0', 1),
(3, 'vijaynagar kanpurvh#b(bnnnnbmknbjjbnjkjmmmmmmmmm', '', '', 'Home', '4', 0, '26.493356199999997', '77.99099489999999', 13, '2018-05-23 12:20:51', '2018-05-23 12:21:21', 7, b'0', 2),
(5, 'vijaynagaar', '', '', 'Home', '1', 0, '22.7532848', '75.8936962', 13, '2018-05-23 12:21:55', '2018-05-23 12:21:55', 1, b'0', 1),
(7, 'ggvvh', '', '', 'Home', '1', 0, '22.717928800000003', '75.8847369', 13, '2018-05-23 12:22:23', '2018-05-23 12:22:23', 2, b'0', 1),
(8, 'hibzkns', '', '', 'Home', '3', 0, '25.830717399999997', '84.1857115', 13, '2018-05-23 12:23:05', '2018-05-23 12:23:05', 5, b'0', 2),
(9, 'ujjain', '', '', 'Home', '3', 0, '25.7584381', '84.1487319', 14, '2018-05-23 12:24:56', '2018-05-23 12:24:56', 5, b'0', 2),
(11, '??????', '', '', 'Home', '4', 0, '26.5123388', '80.2329', 20, '2018-05-30 16:43:03', '2018-05-30 16:43:03', 7, b'0', 2),
(13, '?????', '', '', 'Home', '1', 0, '22.7180683', '75.8823077', 20, '2018-05-30 16:54:15', '2018-05-30 16:54:15', 2, b'0', 1),
(15, 'hjgj', '', '', 'Home', '4', 0, '26.493356199999997', '77.99099489999999', 22, '2018-05-30 18:38:00', '2018-05-30 18:38:00', 7, b'0', 2),
(16, '555', '', '', 'Home', '1', 0, '22.756359', '75.8891944', 22, '2018-05-30 18:38:26', '2018-05-30 18:38:26', 1, b'0', 1),
(17, '555', '', '', 'Home', '1', 0, '22.756359', '75.8891944', 22, '2018-05-30 18:38:38', '2018-05-30 18:38:38', 1, b'0', 1),
(18, '66/7', '', '', 'Home', '1', 0, '22.757133', '75.88396', 19, '2018-05-31 11:38:19', '2018-05-31 11:38:19', 1, b'0', 1),
(23, 'hui', '', '', 'Home', '1', 1, '22.7522195', '75.89152419999999', 3, '2018-06-02 19:21:22', '2018-06-02 19:21:22', 1, b'0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `UserFavourite`
--

CREATE TABLE `UserFavourite` (
  `Id` int(11) NOT NULL,
  `StoreId` int(11) NOT NULL,
  `DieticianId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `AddedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `UserPaymentAccount`
--

CREATE TABLE `UserPaymentAccount` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `BankName` varchar(50) NOT NULL,
  `Status` int(11) NOT NULL,
  `CardNo` varchar(50) NOT NULL,
  `CCV` int(11) NOT NULL,
  `AuthCode` varchar(256) NOT NULL,
  `PinCode` varchar(30) NOT NULL,
  `LinkedOn` datetime NOT NULL,
  `Sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `UserReferral`
--

CREATE TABLE `UserReferral` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `ReferCode` varchar(20) NOT NULL,
  `CreatedOn` int(11) NOT NULL,
  `Message` varchar(300) DEFAULT NULL,
  `ExpireOn` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `UserReferralTransaction`
--

CREATE TABLE `UserReferralTransaction` (
  `RHistoryId` int(10) NOT NULL,
  `UserId` int(10) NOT NULL,
  `Type` varchar(8) NOT NULL,
  `Amount` varchar(20) NOT NULL,
  `Msg` varchar(255) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `UserValidityPlan`
--

CREATE TABLE `UserValidityPlan` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `ValidityPlanId` int(10) NOT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `ValidityDays` int(5) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `ValidityAmount` varchar(20) NOT NULL,
  `TransactionDetail` varchar(100) NOT NULL,
  `PlanType` int(1) NOT NULL COMMENT '1=regular,2=trial'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserValidityPlan`
--

INSERT INTO `UserValidityPlan` (`Id`, `UserId`, `ValidityPlanId`, `StartDate`, `EndDate`, `ValidityDays`, `CreateOn`, `ValidityAmount`, `TransactionDetail`, `PlanType`) VALUES
(1, 2, 0, '2018-05-28 00:00:00', '2018-06-15 00:00:00', 18, '2018-05-28 11:44:02', '1000', '6fdf65d4f65s4fs665fsdfsfsdfsffs6f4s6fs6', 1),
(2, 3, 0, '2018-05-28 00:00:00', '2018-06-02 00:00:00', 5, '2018-05-28 19:09:45', '100.00', 'MKD007', 1),
(3, 20, 0, '2018-05-30 00:00:00', '2018-06-02 00:00:00', 3, '2018-05-30 16:31:05', '250.00', 'MKD007', 2),
(4, 20, 0, '2018-05-30 00:00:00', '2018-06-09 00:00:00', 10, '2018-05-30 16:31:39', '100.00', 'MKD007', 1),
(5, 20, 0, '2018-05-30 00:00:00', '2018-07-14 00:00:00', 45, '2018-05-30 17:16:41', '3457.00', 'MKD007', 1),
(6, 20, 0, '2018-05-30 00:00:00', '2018-07-14 00:00:00', 45, '2018-05-30 17:16:49', '3971.00', 'MKD007', 1),
(7, 20, 0, '2018-05-30 00:00:00', '2018-06-09 00:00:00', 10, '2018-05-30 17:17:48', '547.00', 'MKD007', 1),
(8, 20, 0, '2018-05-30 00:00:00', '2018-06-19 00:00:00', 20, '2018-05-30 17:18:17', '1733.00', 'MKD007', 1),
(9, 20, 0, '2018-05-30 00:00:00', '2018-07-14 00:00:00', 45, '2018-05-30 17:18:21', '4016.00', 'MKD007', 1),
(10, 20, 0, '2018-05-30 00:00:00', '2018-07-14 00:00:00', 45, '2018-05-30 17:18:25', '4833.00', 'MKD007', 1),
(11, 21, 0, '2018-05-30 00:00:00', '2018-06-29 00:00:00', 30, '2018-05-30 18:09:14', '2865.00', 'MKD007', 1),
(12, 21, 0, '2018-05-30 00:00:00', '2018-06-02 00:00:00', 3, '2018-05-30 18:09:20', '250.00', 'MKD007', 2),
(13, 21, 0, '2018-05-30 00:00:00', '2018-06-09 00:00:00', 10, '2018-05-30 18:09:42', '614.00', 'MKD007', 1),
(14, 21, 0, '2018-05-30 00:00:00', '2018-06-24 00:00:00', 25, '2018-05-30 18:10:35', '2256.00', 'MKD007', 1),
(15, 21, 0, '2018-05-30 00:00:00', '2018-06-14 00:00:00', 15, '2018-05-30 18:11:15', '1453.00', 'MKD007', 1),
(16, 22, 0, '2018-05-30 00:00:00', '2018-06-02 00:00:00', 3, '2018-05-30 18:22:30', '250.00', 'MKD007', 2),
(17, 22, 0, '2018-05-30 00:00:00', '2018-06-09 00:00:00', 10, '2018-05-30 18:23:03', '100.00', 'MKD007', 1),
(18, 22, 0, '2018-05-30 00:00:00', '2018-06-09 00:00:00', 10, '2018-05-30 18:55:51', '100.00', 'MKD007', 1),
(19, 23, 0, '2018-05-30 00:00:00', '2018-06-09 00:00:00', 10, '2018-05-30 18:59:13', '100.00', 'MKD007', 1),
(20, 23, 0, '2018-05-30 00:00:00', '2018-06-02 00:00:00', 3, '2018-05-30 19:02:02', '250.00', 'MKD007', 2),
(21, 23, 0, '2018-05-30 00:00:00', '2018-06-24 00:00:00', 25, '2018-05-30 19:02:08', '2056.00', 'MKD007', 1),
(22, 24, 0, '2018-05-30 00:00:00', '2018-06-02 00:00:00', 3, '2018-05-30 19:06:11', '250.00', 'MKD007', 2),
(23, 25, 0, '2018-05-30 00:00:00', '2018-06-02 00:00:00', 3, '2018-05-30 19:10:51', '250.00', 'MKD007', 2),
(24, 19, 0, '2018-05-31 00:00:00', '2018-06-03 00:00:00', 3, '2018-05-31 10:21:29', '250.00', 'MKD007', 2),
(25, 26, 0, '2018-05-31 00:00:00', '2018-06-03 00:00:00', 3, '2018-05-31 10:28:54', '250.00', 'MKD007', 2),
(26, 27, 0, '2018-05-31 00:00:00', '2018-06-03 00:00:00', 3, '2018-05-31 10:49:11', '250.00', 'MKD007', 2),
(27, 19, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 10:52:31', '401.00', 'MKD007', 1),
(28, 27, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 10:53:30', '100.00', 'MKD007', 1),
(29, 28, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 11:13:18', '100.00', 'MKD007', 1),
(30, 29, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 11:19:59', '100.00', 'MKD007', 1),
(31, 31, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 12:54:16', '200.00', 'MKD007', 1),
(32, 32, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 12:57:03', '100.00', 'MKD007', 1),
(33, 32, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 12:57:59', '100.00', 'MKD007', 1),
(34, 32, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 12:58:17', '100.00', 'MKD007', 1),
(35, 38, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 13:51:57', '100.00', 'MKD007', 1),
(36, 37, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 13:53:13', '100.00', 'MKD007', 1),
(37, 20, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 13:58:23', '100.00', 'MKD007', 1),
(38, 20, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 13:58:35', '100.00', 'MKD007', 1),
(39, 3, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 17:43:28', '100.00', 'pay_AHh5glbO0J3Lxw', 1),
(40, 3, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 17:44:22', '100.00', 'pay_AHh6lIBMUXABwL', 1),
(41, 3, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 17:45:27', '100.00', 'pay_AHh7siJc7PQP2g', 1),
(42, 3, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 17:50:34', '100.00', 'pay_AHhDIJ6NQfGqPM', 1),
(43, 3, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 18:44:11', '100.00', 'pay_AHi7wBKz0EyiIk', 1),
(44, 3, 0, '2018-05-31 00:00:00', '2018-06-10 00:00:00', 10, '2018-05-31 18:45:09', '100.00', 'pay_AHi8Z2GCRTGWCz', 1),
(45, 39, 0, '2018-05-31 00:00:00', '2018-06-15 00:00:00', 15, '2018-05-31 20:31:55', '1247.00', 'MKD007', 1),
(46, 3, 0, '2018-06-02 00:00:00', '2018-06-12 00:00:00', 10, '2018-06-02 12:09:02', '100.00', 'pay_AIOSehJwHeP7TK', 1),
(47, 3, 0, '2018-06-02 00:00:00', '2018-06-12 00:00:00', 10, '2018-06-02 16:23:58', '100.00', 'pay_AISo3kDVeuBKFX', 1);

-- --------------------------------------------------------

--
-- Table structure for table `WalletTransaction`
--

CREATE TABLE `WalletTransaction` (
  `HistoryId` int(10) NOT NULL,
  `UserId` int(10) NOT NULL,
  `Type` varchar(8) NOT NULL,
  `Msg` varchar(250) NOT NULL,
  `Amount` varchar(20) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AdminOffer`
--
ALTER TABLE `AdminOffer`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ProductId` (`ProductId`),
  ADD KEY `StoreId` (`StoreId`),
  ADD KEY `OrgId` (`OrgId`),
  ADD KEY `ProductCategoryId` (`ProductCategoryId`),
  ADD KEY `ProductSubCategoryId` (`ProductSubCategoryId`);

--
-- Indexes for table `AdminSetting`
--
ALTER TABLE `AdminSetting`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `AppVersion`
--
ALTER TABLE `AppVersion`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Area`
--
ALTER TABLE `Area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `City`
--
ALTER TABLE `City`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ContactUs`
--
ALTER TABLE `ContactUs`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `FAQ`
--
ALTER TABLE `FAQ`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Feedback`
--
ALTER TABLE `Feedback`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `StoreId` (`StoreId`),
  ADD KEY `UserId` (`UserId`),
  ADD KEY `ProductId` (`ProductId`),
  ADD KEY `OrderId` (`OrderId`);

--
-- Indexes for table `LocalArea`
--
ALTER TABLE `LocalArea`
  ADD PRIMARY KEY (`LocalAreaId`);

--
-- Indexes for table `MkdValidityPlan`
--
ALTER TABLE `MkdValidityPlan`
  ADD PRIMARY KEY (`ValidityId`);

--
-- Indexes for table `OrderData`
--
ALTER TABLE `OrderData`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `UserId` (`UserId`),
  ADD KEY `StoreId` (`StoreId`);

--
-- Indexes for table `OrderDelivery`
--
ALTER TABLE `OrderDelivery`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_OrderDelivery_Employee_AssignTo` (`AssignTo`),
  ADD KEY `FK_OrderDelivery_OrderData_Id` (`OrderId`);

--
-- Indexes for table `OrderItem`
--
ALTER TABLE `OrderItem`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ProductId` (`ProductId`),
  ADD KEY `OrderId` (`OrderId`);

--
-- Indexes for table `OrderItemOptionRelation`
--
ALTER TABLE `OrderItemOptionRelation`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `OrderItemId` (`OrderItemId`),
  ADD KEY `ProductOptionItemId` (`ProductOptionItemId`);

--
-- Indexes for table `OrderPayment`
--
ALTER TABLE `OrderPayment`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_OrderPayment_` (`OrderId`);

--
-- Indexes for table `OrderTracking`
--
ALTER TABLE `OrderTracking`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `OrderId` (`OrderId`);

--
-- Indexes for table `Organization`
--
ALTER TABLE `Organization`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `OrgType` (`OrgType`);

--
-- Indexes for table `OrganizationType`
--
ALTER TABLE `OrganizationType`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Page_content`
--
ALTER TABLE `Page_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PauseResume`
--
ALTER TABLE `PauseResume`
  ADD PRIMARY KEY (`PRId`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Product_Store_StoreId` (`StoreId`),
  ADD KEY `FK_Product_User_UserID` (`UserId`),
  ADD KEY `CategoryId` (`CategoryId`);

--
-- Indexes for table `ProductCategory`
--
ALTER TABLE `ProductCategory`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ProductImages`
--
ALTER TABLE `ProductImages`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_ProductImages_Product_Id` (`ProductId`);

--
-- Indexes for table `ProductSubCategory`
--
ALTER TABLE `ProductSubCategory`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CategoryId` (`CategoryId`);

--
-- Indexes for table `Promotions`
--
ALTER TABLE `Promotions`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ReferralByDeviceId`
--
ALTER TABLE `ReferralByDeviceId`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `State`
--
ALTER TABLE `State`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Store`
--
ALTER TABLE `Store`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `OrgId` (`OrgId`),
  ADD KEY `ManagerId` (`ManagerId`);

--
-- Indexes for table `SubscriptionPlan`
--
ALTER TABLE `SubscriptionPlan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TiffenProductSchedule`
--
ALTER TABLE `TiffenProductSchedule`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `TiffenTiming`
--
ALTER TABLE `TiffenTiming`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `TiffinStartEndStatus`
--
ALTER TABLE `TiffinStartEndStatus`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `TiffinUserWeeklyMeal`
--
ALTER TABLE `TiffinUserWeeklyMeal`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userAddress`
--
ALTER TABLE `userAddress`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `UserFavourite`
--
ALTER TABLE `UserFavourite`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `UserId` (`UserId`),
  ADD KEY `StoreId` (`StoreId`),
  ADD KEY `DieticianId` (`DieticianId`);

--
-- Indexes for table `UserPaymentAccount`
--
ALTER TABLE `UserPaymentAccount`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `UserReferral`
--
ALTER TABLE `UserReferral`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `UserReferralTransaction`
--
ALTER TABLE `UserReferralTransaction`
  ADD PRIMARY KEY (`RHistoryId`);

--
-- Indexes for table `UserValidityPlan`
--
ALTER TABLE `UserValidityPlan`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `WalletTransaction`
--
ALTER TABLE `WalletTransaction`
  ADD PRIMARY KEY (`HistoryId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AdminOffer`
--
ALTER TABLE `AdminOffer`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AdminSetting`
--
ALTER TABLE `AdminSetting`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `AppVersion`
--
ALTER TABLE `AppVersion`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Area`
--
ALTER TABLE `Area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `City`
--
ALTER TABLE `City`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ContactUs`
--
ALTER TABLE `ContactUs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `FAQ`
--
ALTER TABLE `FAQ`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Feedback`
--
ALTER TABLE `Feedback`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `LocalArea`
--
ALTER TABLE `LocalArea`
  MODIFY `LocalAreaId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `MkdValidityPlan`
--
ALTER TABLE `MkdValidityPlan`
  MODIFY `ValidityId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `OrderData`
--
ALTER TABLE `OrderData`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `OrderDelivery`
--
ALTER TABLE `OrderDelivery`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `OrderItem`
--
ALTER TABLE `OrderItem`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `OrderItemOptionRelation`
--
ALTER TABLE `OrderItemOptionRelation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `OrderPayment`
--
ALTER TABLE `OrderPayment`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `OrderTracking`
--
ALTER TABLE `OrderTracking`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Organization`
--
ALTER TABLE `Organization`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `OrganizationType`
--
ALTER TABLE `OrganizationType`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `PauseResume`
--
ALTER TABLE `PauseResume`
  MODIFY `PRId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Product`
--
ALTER TABLE `Product`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `ProductCategory`
--
ALTER TABLE `ProductCategory`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ProductImages`
--
ALTER TABLE `ProductImages`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ProductSubCategory`
--
ALTER TABLE `ProductSubCategory`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Promotions`
--
ALTER TABLE `Promotions`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `ReferralByDeviceId`
--
ALTER TABLE `ReferralByDeviceId`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `State`
--
ALTER TABLE `State`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Store`
--
ALTER TABLE `Store`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `SubscriptionPlan`
--
ALTER TABLE `SubscriptionPlan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `TiffenProductSchedule`
--
ALTER TABLE `TiffenProductSchedule`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `TiffenTiming`
--
ALTER TABLE `TiffenTiming`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `TiffinStartEndStatus`
--
ALTER TABLE `TiffinStartEndStatus`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `TiffinUserWeeklyMeal`
--
ALTER TABLE `TiffinUserWeeklyMeal`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `userAddress`
--
ALTER TABLE `userAddress`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `UserFavourite`
--
ALTER TABLE `UserFavourite`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `UserPaymentAccount`
--
ALTER TABLE `UserPaymentAccount`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `UserReferral`
--
ALTER TABLE `UserReferral`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `UserReferralTransaction`
--
ALTER TABLE `UserReferralTransaction`
  MODIFY `RHistoryId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `UserValidityPlan`
--
ALTER TABLE `UserValidityPlan`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `WalletTransaction`
--
ALTER TABLE `WalletTransaction`
  MODIFY `HistoryId` int(10) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `AdminOffer`
--
ALTER TABLE `AdminOffer`
  ADD CONSTRAINT `AdminOffer_ibfk_1` FOREIGN KEY (`ProductId`) REFERENCES `Product` (`Id`),
  ADD CONSTRAINT `AdminOffer_ibfk_2` FOREIGN KEY (`StoreId`) REFERENCES `Store` (`Id`),
  ADD CONSTRAINT `AdminOffer_ibfk_3` FOREIGN KEY (`OrgId`) REFERENCES `Organization` (`Id`),
  ADD CONSTRAINT `AdminOffer_ibfk_4` FOREIGN KEY (`ProductCategoryId`) REFERENCES `ProductCategory` (`Id`),
  ADD CONSTRAINT `AdminOffer_ibfk_5` FOREIGN KEY (`ProductSubCategoryId`) REFERENCES `ProductSubCategory` (`Id`);

--
-- Constraints for table `Feedback`
--
ALTER TABLE `Feedback`
  ADD CONSTRAINT `Feedback_ibfk_1` FOREIGN KEY (`StoreId`) REFERENCES `Store` (`Id`),
  ADD CONSTRAINT `Feedback_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `Feedback_ibfk_3` FOREIGN KEY (`ProductId`) REFERENCES `Product` (`Id`),
  ADD CONSTRAINT `Feedback_ibfk_4` FOREIGN KEY (`OrderId`) REFERENCES `OrderData` (`Id`);

--
-- Constraints for table `OrderData`
--
ALTER TABLE `OrderData`
  ADD CONSTRAINT `OrderData_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `OrderData_ibfk_2` FOREIGN KEY (`StoreId`) REFERENCES `Store` (`Id`);

--
-- Constraints for table `OrderDelivery`
--
ALTER TABLE `OrderDelivery`
  ADD CONSTRAINT `FK_OrderDelivery_Employee_AssignTo` FOREIGN KEY (`AssignTo`) REFERENCES `Employee` (`Id`),
  ADD CONSTRAINT `FK_OrderDelivery_OrderData_Id` FOREIGN KEY (`OrderId`) REFERENCES `OrderData` (`Id`);

--
-- Constraints for table `OrderItem`
--
ALTER TABLE `OrderItem`
  ADD CONSTRAINT `OrderItem_ibfk_1` FOREIGN KEY (`ProductId`) REFERENCES `Product` (`Id`),
  ADD CONSTRAINT `OrderItem_ibfk_2` FOREIGN KEY (`OrderId`) REFERENCES `OrderData` (`Id`);

--
-- Constraints for table `OrderItemOptionRelation`
--
ALTER TABLE `OrderItemOptionRelation`
  ADD CONSTRAINT `OrderItemOptionRelation_ibfk_1` FOREIGN KEY (`OrderItemId`) REFERENCES `OrderItem` (`Id`),
  ADD CONSTRAINT `OrderItemOptionRelation_ibfk_2` FOREIGN KEY (`ProductOptionItemId`) REFERENCES `ProductOptionItem` (`Id`);

--
-- Constraints for table `OrderPayment`
--
ALTER TABLE `OrderPayment`
  ADD CONSTRAINT `FK_OrderPayment_` FOREIGN KEY (`OrderId`) REFERENCES `OrderData` (`Id`);

--
-- Constraints for table `OrderTracking`
--
ALTER TABLE `OrderTracking`
  ADD CONSTRAINT `OrderTracking_ibfk_1` FOREIGN KEY (`OrderId`) REFERENCES `OrderData` (`Id`);

--
-- Constraints for table `Organization`
--
ALTER TABLE `Organization`
  ADD CONSTRAINT `Organization_ibfk_1` FOREIGN KEY (`OrgType`) REFERENCES `OrganizationType` (`Id`);

--
-- Constraints for table `ProductImages`
--
ALTER TABLE `ProductImages`
  ADD CONSTRAINT `FK_ProductImages_Product_Id` FOREIGN KEY (`ProductId`) REFERENCES `Product` (`Id`);

--
-- Constraints for table `ProductSubCategory`
--
ALTER TABLE `ProductSubCategory`
  ADD CONSTRAINT `ProductSubCategory_ibfk_1` FOREIGN KEY (`CategoryId`) REFERENCES `ProductCategory` (`Id`);

--
-- Constraints for table `Store`
--
ALTER TABLE `Store`
  ADD CONSTRAINT `Store_ibfk_1` FOREIGN KEY (`OrgId`) REFERENCES `Organization` (`Id`),
  ADD CONSTRAINT `Store_ibfk_2` FOREIGN KEY (`ManagerId`) REFERENCES `Employee` (`Id`);

--
-- Constraints for table `userAddress`
--
ALTER TABLE `userAddress`
  ADD CONSTRAINT `userAddress_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`);

--
-- Constraints for table `UserFavourite`
--
ALTER TABLE `UserFavourite`
  ADD CONSTRAINT `UserFavourite_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `UserFavourite_ibfk_2` FOREIGN KEY (`StoreId`) REFERENCES `Store` (`Id`);

--
-- Constraints for table `UserPaymentAccount`
--
ALTER TABLE `UserPaymentAccount`
  ADD CONSTRAINT `UserPaymentAccount_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
