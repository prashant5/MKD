<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends MY_Controller {
function __construct() {
		parent::__construct();
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
	}
	public function login() 
	{
		$headers = apache_request_headers();
		$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    if(!empty($json_array))
	    {
	     	if(strlen($json_array->mobile)==10 && ctype_digit($json_array->mobile))
			{
		     	//$otp = '1234';
		     	$otp = substr($this->common_model->randomuniqueOtp(),0,4);
				$otp1 ="Your OTP for ACH is:".$otp;
		     	$final_output = array();
		     	$check_phone = $this->common_model->common_getRow('user',array('mobile'=>$json_array->mobile));
		     	if($check_phone)
		     	{  
					$uid = $check_phone->user_id;
					if($check_phone->status != 2)
					{
						$update_phone = $this->common_model->updateData('user',array('mobile_otp'=>$otp,'status'=>0,'update_at'=>militime),array('user_id'=>$uid));
			     		if($update_phone)
						{  
							$this->common_model->sms_send($json_array->mobile,$otp1);
							$final_output['status'] = 'success';
							$final_output['message'] = 'Login successfully';	
							$final_output['user_id'] = $uid;		
						}
					}else
					{
		     			$final_output['status'] = 'failed';	
						$final_output['message'] = 'Your ACH account and has been temporarily suspended as a security precaution.';
					}
		     	}
		     	else
		     	{
		     		$final_output['status'] = 'failed';	
					$final_output['message'] = 'Please register first';	
		     	}
	     	}else
			{
				$final_output['message'] ="Request parameter not valid";
			}
	    }
	    else{
	     	$final_output['status'] = 'No Request parameter';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function register() 
	{
		$headers = apache_request_headers();
		$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    if(!empty($json_array))
	    {
		    if(strlen($json_array->mobile)==10 && ctype_digit($json_array->mobile))
			{
		     	$final_output = array();
		     	$check_row = $this->db->query("SELECT * FROM user WHERE mobile ='$json_array->mobile'");
		     	$check_row1=$check_row->row();
		     	if($check_row1)
		     	{  
	     			$final_output['status'] = 'failed';	
					$final_output['message'] = 'Mobile No Already Exist Please Login!!';	
	    	 	}
		     	else
		     	{
		     		$otp = substr($this->common_model->randomuniqueOtp(),0,4);
		     		//$otp = '1234';
					$otp1 ="Your OTP for ACH is:".$otp;
					$phone = $json_array->mobile;

		     		$data_array = array(
					                  'name'=>$json_array->name,
					                  'mobile'=>$json_array->mobile,
					                  'email'=>$json_array->email,
					                  'create_at'=>militime,
					                  'update_at'=>militime,
					                  'mobile_otp'=>$otp
						               );
					$insertId = $this->common_model->common_insert('user', $data_array);
					if($insertId)
					{  $this->db->cache_delete('admin', 'user');
						$this->common_model->sms_send($phone,$otp1);
						//$message= array('user_id'=>$insertId,'title'=>'Woodbazarr','status'=>'register','message'=>'welcome to Woodbazarr app!!','create_at'=>militime);
						//$this->AndroidNotification($json_array->device_token,$message);
						$final_output['status'] = 'success';
						$final_output['user_id'] = $insertId;		
						$final_output['message'] = 'Register successfully';	
					}
					else
					{ 
						$final_output['status'] = 'failed';	
						$final_output['message'] = 'Something went wrong!! Please try later';	
					}
		     	}
	     	}else
	     	{
	     		$final_output['message'] ="Request parameter not valid";
			}
	    }
	    else{
	     	$final_output['status'] = 'No Request parameter';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function verify()
	{		
		$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    if(!empty($json_array))
	    {
		    if(strlen($json_array->mobile_otp)==4 && ctype_digit($json_array->mobile_otp) && ctype_digit($json_array->user_id))
			{
		     	$final_output = array(); $addresss = (object)array();
		     	$check_phone = $this->common_model->common_getRow('user',array('mobile_otp'=>$json_array->mobile_otp,'user_id'=>$json_array->user_id));
		     	if($check_phone)
		     	{  
	     			$auth_key1 = bin2hex(openssl_random_pseudo_bytes(16));
					$auth_key = $auth_key1.militime;
					
					$update = $this->common_model->updateData('user',array('status'=>1,'token'=>$auth_key,'device_token'=>$json_array->device_token,'device_id'=>$json_array->device_id),array('user_id'=>$json_array->user_id));
					if($update)
					{  
		     			$select_phone = $this->common_model->common_getRow('user',array('user_id'=>$json_array->user_id));
						$url = '';
						if(!empty($select_phone->profile_pic))
						{
							$url = base_url().'uploads/user_image/'.$select_phone->profile_pic;
						}
						$this->db->limit(1);
						$this->db->order_by('address_id','DESC');
						$sel_cart = $this->db->get_where('address',array('user_id'=>$select_phone->user_id,'type'=>0))->row();
				     	if($sel_cart)
						{
							$addresss = $sel_cart; 
						}
						$cartcount = $this->db->query("SELECT count(user_id) as ccount FROM user_cart WHERE user_id = '".$select_phone->user_id."'")->row();

						$aa= array(
		    					"user_id"=>$select_phone->user_id,
		    					"mobile"=>$select_phone->mobile,
		    					"name"=>$select_phone->name,
		    					"email"=>$select_phone->email,
		    					"location"=>$select_phone->location,
		    					"device_token"=>$select_phone->device_token,
		    					"device_id"=>$select_phone->device_id,
		    					"profile_pic"=>$url,
		    					'cart_count'=>$cartcount->ccount,
		    					'ship_address'=>$addresss
		    					);
						$final_output['status'] = 'success';
						$final_output['message'] = 'Your Mobile No. has been verified successfully';	
						$final_output['data'] = $aa;
						$final_output['secret_key'] = $auth_key;	
					}
					else
					{ 
						if($check_phone->status == 1)
						{
							$final_output['status']= 'failed';
							$final_output['message']= "please login.";
							unset($final_output['data']);
						}
					}
		     	}
		     	else
		     	{
					$final_output['status'] = 'failed';	
					$final_output['message'] = 'Otp not matched';	
				}
	     	}else
	     	{
	     		$final_output['message']= "Request parameter not valid";
		 	}
	    }else
	    {
	     	$final_output['status'] = 'No Request parameter';
	    }
	     header("content-type: application/json");
	     echo json_encode($final_output);
	}

	public function resend_otp()
	{
		$headers = apache_request_headers();
		$json1 = file_get_contents('php://input');
	    $json_array = json_decode($json1);
	    if(!empty($json_array))
	    {
	    	$phone = $json_array->mobile;
	     	if(strlen($phone)==10 && ctype_digit($phone))
			{
		     	//$otp = '1234';
		     	$otp = substr($this->common_model->randomuniqueOtp(),0,4);
				$otp1 ="Your OTP for ACH is:".$otp;
		     	$final_output = array();
		     	$check_phone = $this->common_model->common_getRow('user',array('mobile'=>$phone));
		     	if($check_phone)
		     	{  
					$uid = $check_phone->user_id;
					if($check_phone->status != 2)
					{
						$update_phone = $this->common_model->updateData('user',array('mobile_otp'=>$otp,'status'=>0,'update_at'=>militime),array('user_id'=>$uid));
			     		if($update_phone)
						{  
							$this->common_model->sms_send($phone,$otp1);
							$final_output['status'] = 'success';
							$final_output['message'] = 'OTP Successfully Sent';	
							$final_output['user_id'] = $uid;		
						}
					}else
					{
		     			$final_output['status'] = 'failed';	
						$final_output['message'] = 'Your ACH account and has been temporarily suspended as a security precaution.';
					}
		     	}
		     	else
		     	{
		     		$final_output['status'] = 'failed';	
					$final_output['message'] = 'Please register first';	
		     	}
	     	}else
			{
				$final_output['message'] ="Request parameter not valid";
			}
	    }
	    else{
	     	$final_output['status'] = 'No Request parameter';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);

	}
   
  	/*public function health_Question_Answer()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				global $db;
				$userid = $check['data']->user_id;
				$json_array = $this->input->get('basic_info');
				$json_array2 = $this->input->get('basic_ques');
				$json_array3 = $this->input->get('advance_ques');
				$medication_ans = $this->input->get('medication_ans');
				$allergies_ans = $this->input->get('allergies_ans');
				$comment = $this->input->get('comment');
				$value=json_decode($json_array);
				$value2=json_decode($json_array2);
				$value3=json_decode($json_array3);
    			
			    if(!empty($json_array))
			    {
					$levelkey = $value->basic_info->level_key;
					$final_output = array();
					$insertbasic = array();
					$insertdata = $this->common_model->common_insert('health_basic_info',array('user_id'=>$userid,'problem_for'=>$value->basic_info->problem_for,'gender'=>$value->basic_info->gender,'age'=>$value->basic_info->age,'patient_name'=>$value->basic_info->patient_name,'profession'=>$value->basic_info->profession,'weight'=>$value->basic_info->weight,'height'=>$value->basic_info->height,'create_at'=>militime));
					if($insertdata)
					{
						for ($i=0; $i < count($value2->basic_question); $i++)
                		{
					   		$insertbasic = $this->common_model->common_insert("basic_question_answer",array('user_id'=>$userid,'basic_info_id'=>$insertdata,'basic_question_id'=>$value2->basic_question[$i]->question,'basic_answer_id'=>$value2->basic_question[$i]->answer,'created_at'=>militime));
						}
						if($insertbasic)
						{
							if($levelkey == 1)
							{
								for ($i=0; $i < count($value3->advance_question); $i++)
		                		{
							   		$insertadvance = $this->common_model->common_insert("advance_question_answer",array('user_id'=>$userid,'basic_info_id'=>$insertdata,'category_id'=>$value3->advance_question[$i]->categoryid,'question_id'=>$value3->advance_question[$i]->question,'answer_id'=>$value3->advance_question[$i]->answer,'create_at'=>militime));
								}
								if($insertadvance)
								{
							   		$image = '';
							   		if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
									{  
										$date = date("ymdhis");
										$config['upload_path'] = './uploads/injury_image/';
										$config['allowed_types'] = 'gif|jpg|png|jpeg';
										
										for ($i=0; $i < count($_FILES['image']['name']); $i++) { 
											$subFileName = explode('.',$_FILES['image']['name'][$i]);
											$ExtFileName = end($subFileName);
											$config['file_name'] = md5($date.$_FILES['image']['name'][$i]).'.'.$ExtFileName;
								            $this->load->library('upload', $config);
											$this->upload->initialize($config);
											$aa[] = $config['file_name'];
											if($this->upload->do_upload('image'))
											{ 
											    $upload_data[] = $this->upload->data();
												//	$image[] =  $upload_data['file_name'];
											}
										}
									}
									else
									{  
										$upload_data = ""; 
									}
									if(!empty($upload_data))
									{
										$i = 0;
										foreach ($upload_data[0] as $key) {
										if($i == 0)
										{
											$image .= $key['file_name'];
										}else
										{
											$image .= ','.$key['file_name'];
										}
										$i++;
										}
									}
									$insertadvance = $this->common_model->common_insert("other_advance_info",array('user_id'=>$userid,'basic_info_id'=>$insertdata,'answer_1'=>$medication_ans,'answer_2'=>$allergies_ans,'comment'=>$comment,'image'=>$image,'created_at'=>militime));
									
									$final_output['status'] ="success";
									$final_output['message'] ="successfull";
									$final_output['levelkey'] = 1;
									unset($final_output['data']);
								}else
								{
									$final_output['status'] ="failed";
									$final_output['message'] ="Someting went wrong!!";
									$final_output['levelkey'] =1;
									unset($final_output['data']);
								}
							}else
							{
								$final_output['status'] ="success";
								$final_output['message'] ="successfull";
								$final_output['levelkey'] = 0;
								unset($final_output['data']);		
							}
						}else
						{
							$final_output['status'] ="failed";
							$final_output['message'] ="Someting went wrong!!";
							$final_output['levelkey'] = 0;
							unset($final_output['data']);
						}
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="Something went wrong";
						$final_output['levelkey'] = 0;
						unset($final_output['data']);
					}
				}else
			    {
			     	$final_output['status'] = 'No Request parameter';
			    }
			}else
			{
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}*/
	public function Get_Category()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$final_output = array();
				$sele_cate = $this->common_model->getData("health_category",array('parent_id'=>0));
				if($sele_cate)
				{
					foreach ($sele_cate as $key) {
						if($key->image == '') $image = '';
						else $image = base_url().'uploads/category_image/'.$key->image; 
						$arr[] = array(
									'category_id'=>$key->category_id,
									'category_name'=>$key->category_name,
									'image'=>$image
									);
					}
					$final_output['status'] ="success";
					$final_output['message'] ="successfull";
					$final_output['data'] = $arr;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Somthing went wrong";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
   	
    public function Get_Sub_Category()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$json1 = file_get_contents('php://input');
			    $json_array = json_decode($json1);
			    if(!empty($json_array))
			    {
					$category_id = $json_array->category_id;
					$final_output = array();
					$sele_cate = $this->common_model->getData("health_category",array('parent_id'=>$category_id));
					if($sele_cate)
					{
						foreach ($sele_cate as $key) {
							if($key->image == '') $image = '';
							else $image = base_url().'uploads/category_image/'.$key->image; 
							$arr[] = array(
										'category_id'=>$key->category_id,
										'category_name'=>$key->category_name,
										'image'=>$image
										);
						}
						$final_output['status'] ="success";
						$final_output['message'] ="successfull";
						$final_output['data'] = $arr;
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="No Data Found";
						unset($final_output['data']);
					}
				}else
				{
					$final_output['status'] = 'No Request parameter';
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Get_Basic_Question()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
					$final_output = array();
					$sele_cate = $this->common_model->getData("quetions",array('question_type'=>'basic'),'question_id','ASC');
					if($sele_cate)
					{
						foreach ($sele_cate as $key) {
							$ans = '';
							$sele_que = $this->common_model->getData("answer",array('question_id'=>$key->question_id),'answer_id','ASC');
							if($sele_que)
							{
								foreach ($sele_que as $anskey) {
								$ans[] = array(
											'answer_id'=>$anskey->answer_id,
											'answer'=>$anskey->answer,
											);
								}
							}	
							$arr[] = array(
										'question_id'=>$key->question_id,
										'question'=>$key->question,
										'healthans'=>$ans
										);
						}
						$final_output['status'] ="success";
						$final_output['message'] ="successfull";
						$final_output['data'] = $arr;
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="No Data Found";
						unset($final_output['data']);
					}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Get_Primary_Advance_Question()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$json1 = file_get_contents('php://input');
			    $json_array = json_decode($json1);
			    if(!empty($json_array))
			    {
					$category_id = $json_array->category_id;
					$sub_category_id = $json_array->sub_category_id;
					$final_output = array();
					$arr = array();
					$sele_cate = $this->common_model->getData("quetions",array('category_id'=>$sub_category_id,'parent_category_id'=>$category_id,'answer_id'=>0,'parent_ques_id'=>0),'sort_id','ASC');
					if($sele_cate)
					{
						foreach ($sele_cate as $key) {
							$ans = array();
							$sele_que = $this->common_model->getData("answer",array('question_id'=>$key->question_id),'answer_id','ASC');
							if($sele_que)
							{
								foreach ($sele_que as $anskey) {
								$ans[] = array(
											'answer_id'=>$anskey->answer_id,
											'answer'=>$anskey->answer,
											);
								}
							}	
							$arr[] = array(
										'question_id'=>$key->question_id,
										'question'=>$key->question,
										'healthans'=>$ans
										);
						}
						$final_output['status'] ="success";
						$final_output['message'] ="successfull";
						$final_output['data'] = $arr;
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="No Data Found";
						unset($final_output['data']);
					}
				}else
				{
					$final_output['status'] = 'No Request parameter';
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Get_Secondary_Advance_Question()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$json1 = file_get_contents('php://input');
			    $json_array = json_decode($json1);
			    $arr = array();
			    if(!empty($json_array))
			    {
					$countarray = count($json_array->question_array);
					if($countarray != 0 && $countarray != '')
					{
						for ($i=0; $i < $countarray; $i++) { 
							$sele_cate = $this->common_model->getData("quetions",array('answer_id'=>$json_array->question_array[$i]->answer,'parent_ques_id'=>$json_array->question_array[$i]->question,'status'=>0),'sort_id','ASC');
							if($sele_cate)
							{
								foreach ($sele_cate as $key) {
									$ans = array();
									$sele_que = $this->common_model->getData("answer",array('question_id'=>$key->question_id),'answer_id','ASC');
									if($sele_que)
									{
										foreach ($sele_que as $anskey) {
										$ans[] = array(
													'answer_id'=>$anskey->answer_id,
													'answer'=>$anskey->answer,
													);
										}
									}	
								$arr[] = array(
											'question_id'=>$key->question_id,
											'question'=>$key->question,
											'healthans'=>$ans
											);
								}
							}
						}
						if($arr)
						{
							$final_output['status'] ="success";
							$final_output['message'] ="successfull";
							$final_output['data'] = $arr;
						}else
						{
							$final_output['status'] ="failed";
							$final_output['message'] ="No Data Found";
						}
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="No Data Found";
						unset($final_output['data']);
					}
				}else
				{
					$final_output['status'] = 'No Request parameter';
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function All_Diagnosis_Answer()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				global $db;
				$userid = $check['data']->user_id;
			
				$json1 = file_get_contents('php://input');
				$json_dec_arr = json_decode($json1);

				$value = $json_dec_arr->basic_info;
				$value2 = $json_dec_arr->basic_question;
				$value3 = $json_dec_arr->advance_question;
				$comment = $json_dec_arr->comment;
				$doctor_id = $json_dec_arr->doctor_id;
				$categoryid = $json_dec_arr->category_id;
				$subcategoryid = $json_dec_arr->sub_category_id;
    		    if(!empty($value))
			    {
					$paymentkey = $value->pay_key;
					$final_output = array();
					$insertbasic = array(); $insertadvance = '';
					$insertdata = $this->common_model->common_insert('health_basic_info',array("user_id"=>$userid,"problem_for"=>$value->problem_for,"relation"=>$value->relation, "gender"=>$value->gender,
						"age"=>$value->age, "patient_name"=>$value->patient_name,"weight" => $value->weight,"height" => $value->height, "marital_status" => $value->marital_status,
						"any_children" => $value->any_children,	"how_many_child"=>$value->how_many_child,"family_medical_history"=>$value->family_medical_history,"past_health_history"=>$value->past_health_history,
						"diagnosis_payment_status"=>$value->pay_key,'doctor_id'=>$doctor_id));
					if($insertdata)
					{
						for ($i=0; $i < count($value2); $i++)
                		{
					   		$insertbasic = $this->common_model->common_insert("basic_diagnosis_answer",array('user_id'=>$userid,'basic_info_id'=>$insertdata,'question_id'=>$value2[$i]->question,'answer'=>$value2[$i]->answer,'create_at'=>militime));
						}
						if($insertbasic)
						{
							if($paymentkey == 1)
							{
								for ($i=0; $i < count($value3); $i++)
		                		{
							   		$insertadvance = $this->common_model->common_insert("advance_diagnosis_answer",array('user_id'=>$userid,'basic_info_id'=>$insertdata,'category_id'=>$categoryid,'sub_category_id'=>$subcategoryid,'question_id'=>$value3[$i]->question,'answer_id'=>$value3[$i]->answer,'create_at'=>militime));
								}
								if($insertadvance)
								{
							   		$image = '';
							   		if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
									{  
										$date = date("ymdhis");
										$config['upload_path'] = './uploads/injury_image/';
										$config['allowed_types'] = 'gif|jpg|png|jpeg';
										
										for ($i=0; $i < count($_FILES['image']['name']); $i++) { 
											$subFileName = explode('.',$_FILES['image']['name'][$i]);
											$ExtFileName = end($subFileName);
											$config['file_name'] = md5($date.$_FILES['image']['name'][$i]).'.'.$ExtFileName;
								            $this->load->library('upload', $config);
											$this->upload->initialize($config);
											$aa[] = $config['file_name'];
											if($this->upload->do_upload('image'))
											{ 
											    $upload_data[] = $this->upload->data();
												//	$image[] =  $upload_data['file_name'];
											}
										}
									}
									else
									{  
										$upload_data = ""; 
									}
									if(!empty($upload_data))
									{
										$i = 0;
										foreach ($upload_data[0] as $key) {
										if($i == 0)
										{
											$image .= $key['file_name'];
										}else
										{
											$image .= ','.$key['file_name'];
										}
										$i++;
										}
									}
									$insertadvance = $this->common_model->common_insert("other_diagnosis_info",array('user_id'=>$userid,'basic_info_id'=>$insertdata,'image'=>$image,'comment'=>$comment,'doctor_id'=>$doctor_id));
									
									$final_output['status'] ="success";
									$final_output['message'] ="successfull";
									$final_output['pay_key'] = 1;
									unset($final_output['data']);
								}else
								{
									$final_output['status'] ="failed";
									$final_output['message'] ="Someting went wrong!!";
									$final_output['pay_key'] = 1;
									unset($final_output['data']);
								}
							}else
							{
								$final_output['status'] ="success";
								$final_output['message'] ="successfull";
								$final_output['pay_key'] = 0;
								unset($final_output['data']);		
							}
						}else
						{
							$final_output['status'] ="failed";
							$final_output['message'] ="Someting went wrong!!";
							$final_output['pay_key'] = 0;
							unset($final_output['data']);
						}
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="Something went wrong";
						$final_output['pay_key'] = 0;
						unset($final_output['data']);
					}
				}else
			    {
			     	$final_output['status'] = 'No Request parameter';
			    }
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

   	public function Prescription_List()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$final_output = array();
				$sele_cate = $this->common_model->getData("prescription_tb",array('user_id'=>$userid),'create_at','DESC');
				$finalarr = array();
				if($sele_cate)
				{
					foreach ($sele_cate as $key) {
						
						$provider_img = '';
						$providerinfo = $this->common_model->common_getRow("service_provider_tb",array('provider_id'=>$key->service_provider_id));
						if($providerinfo)
						{	
							$provider_id = $providerinfo->provider_id;
							$provider_name = $providerinfo->name;
							$provider_quali = $providerinfo->qualification;
							if(!empty($providerinfo->image))
							{
								$provider_img = base_url().'uploads/provider_image/'.$providerinfo->image;
							}
						}else
						{
							$provider_id = 0;
							$provider_name = 'admin';
							$provider_quali = '';
						}
						$basicinfo = $this->common_model->common_getRow("health_basic_info",array('info_id'=>$key->info_id));
						if($basicinfo->problem_for=="Myself"){  $problemfor = $basicinfo->problem_for; }else { $problemfor = $basicinfo->relation; }
						$patientname = $basicinfo->patient_name;
						
						$categoryname = $this->db->query("SELECT A.category_id,C1.category_name,C2.category_name as subcategory_name FROM `advance_diagnosis_answer` as A LEFT JOIN health_category AS C1 ON A.category_id = C1.category_id LEFT JOIN health_category AS C2 ON A.sub_category_id = C2.category_id WHERE A.basic_info_id = ".$key->info_id." ORDER BY A.question_answer_id ASC LIMIT 1")->row();
						//echo "SELECT A.category_id,C1.category_name,C2.category_name as subcategory_name FROM `advance_diagnosis_answer` as A LEFT JOIN health_category AS C1 ON A.category_id = C1.category_id LEFT JOIN health_category AS C2 ON A.sub_category_id = C2.category_id WHERE A.basic_info_id = ".$key->info_id." ORDER BY A.question_answer_id ASC LIMIT 1";exit;
						if($categoryname)
						{
							$category = $categoryname->category_name;
							$subcategory = $categoryname->subcategory_name;
						}else
						{
							$category = '';
							$subcategory = '';
						}
						$provider_comnt = $key->provider_comment;
						$prescription_id = $key->prescription_id;
						$arr =array();
						$sele_cate = $this->common_model->getData("prescription_medicine",array('prescription_id'=>$key->prescription_id),'id','DESC');
						if(!empty($sele_cate))
						{
							foreach ($sele_cate as $value) {
								//$value->clinical_note = array();
								$arr[] = $value;
							}
						}
						$finalarr[] = array(
								'service_provider_id'=>$provider_id,
								'service_provider_name'=>$provider_name,
								'service_provider_pic'=>$provider_img,
								'qualification'=>$provider_quali,
								'problem_for'=>$problemfor,
								'patient_name'=>$patientname,
								'problem_area_category'=>$category,
								'problem_area_subcategory'=>$subcategory,
								'provider_comment'=>$provider_comnt,
								'prescription_id'=>$prescription_id,
								'mobile_no'=>'',
								'prescription_date'=>'',
								'type'=>'1',
								'clinical_note'=>array(),
								'medicine_array'=>$arr
								);
					}
					$final_output['status'] ="success";
					$final_output['message'] ="successfull";
					$final_output['data'] = $finalarr;
				}
				
				$practo_prescrip = $this->common_model->getData("Practo_prescription_tb",array('user_id'=>$userid),'create_at','DESC');
				if(!empty($practo_prescrip))
				{
					foreach ($practo_prescrip as $key) {

						//clinical note
						$clinic_data = array(); $provider_id = 0; $provider_name = 'admin'; $provider_img = ''; $provider_quali = '';
						$clinic_note = $this->common_model->getData('clinical_notes',array('patient_no'=>$key->patient_no,'user_id'=>$key->user_id,'clinical_date'=>$key->prescription_date));
						if(!empty($clinic_note))
						{
							foreach ($clinic_note as $value) {
								$clinic_data[] = array(
													'name'=>$value->type,
													'description'=>$value->description
													);
							}
							//provider data
							$providerinfo = $this->common_model->common_getRow("service_provider_tb",array('provider_id'=>$clinic_note[0]->provider_id));
							if($providerinfo)
							{	
								$provider_id = $providerinfo->provider_id;
								$provider_name = $providerinfo->name;
								$provider_quali = $providerinfo->qualification;
								if(!empty($providerinfo->image))
								{
									$provider_img = base_url().'uploads/provider_image'.$providerinfo->image;
								}
							}
						}
						//tretment data
						$clinic_note = $this->common_model->getData('treatment_tb',array('patient_no'=>$key->patient_no,'user_id'=>$key->user_id,'treatment_date'=>$key->prescription_date));
						if(!empty($clinic_note))
						{

						}

						$dawai_arr = array();
						$patientname = $this->db->select('name')->get_where("user",array('user_id'=>$key->user_id))->row();
						$patientnm = $patientname->name;
						$dawai_arr[] = array('id'=>$key->id,'prescription_id'=>'','drug_name'=>$key->drugname,'drug_type'=>$key->drug_type,'dose'=>$key->dose,'dose_unit'=>$key->dose_unit);
						$finalarr[] = array(
								'service_provider_id'=>$provider_id,
								'service_provider_name'=>$provider_name,
								'service_provider_pic'=>$provider_img,
								'qualification'=>$provider_quali,
								'problem_for'=>'self',
								'patient_name'=>$patientnm,
								'problem_area_category'=>'',
								'problem_area_subcategory'=>'',
								'provider_comment'=>$provider_comnt,
								'prescription_id'=>'',
								'mobile_no'=>'',
								'prescription_date'=>'',
								'type'=>'2',
								'clinical_note'=>$clinic_data,
								'medicine_array'=>$dawai_arr
								);
					}
				}
				if(!empty($finalarr))
				{
					$final_output['status'] ="success";
					$final_output['message'] ="successfull";
					$final_output['data'] = $finalarr;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="No Data Found";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	
	public function ForumComment()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$json1 = file_get_contents('php://input');
			    $json_array = json_decode($json1);
			  	if(!empty($json_array))
			    {
					$final_output = array();
					//$sele_cate = $this->common_model->getData("health_basic_info",array('user_id'=>0));
					$json_array->user_id = $userid;
					$json_array->create_at = militime;
					$insertdata = $this->common_model->common_insert('forum_comment', $json_array);
					if($insertdata)
					{
						$json_array->forumcommnentid = $insertdata;
						$final_output['status'] ="success";
						$final_output['message'] ="Comment successfully added";
						unset($final_output['data']);
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="Something went wrong";
						unset($final_output['data']);
					}
				}else
			    {
			     	$final_output['status'] = 'No Request parameter';
			    }
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function GetForumList()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$created = $this->input->get('created');
				$final_output = array();
				$wheree = '';
				if($created != 0)
				{
					$wheree = "WHERE create_at < '$created'";
				}
				$check_row = $this->db->query("SELECT * FROM forum ".$wheree." ORDER BY forum_id DESC LIMIT 10");
		     	$getdata=$check_row->result();
				if($getdata)
				{
					$final_output['status'] ="success";
					$final_output['message'] ="Successfull";
					$final_output['data'] = $getdata;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Forum List not found";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function ReplyForumComment()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$json1 = file_get_contents('php://input');
			    $json_array = json_decode($json1);
			  	if(!empty($json_array))
			    {
					$final_output = array();
					//$sele_cate = $this->common_model->getData("health_basic_info",array('user_id'=>0));
					$json_array->user_id = $userid;
					$commentid = $this->db->select('comment_id')->get_where("forum_comment",array('comment_id'=>$json_array->comment_id))->row();
					if(!empty($commentid))
					{
						$insertdata = $this->common_model->common_insert('forum_comment_reply', array('comment_id'=>$json_array->comment_id,'user_id'=>$userid,'comment'=>$json_array->comment,'type'=>'user','create_at'=>militime));
						if($insertdata)
						{
							$reply_id = $insertdata;
							$comment_id = $json_array->comment_id;
							$final_output['status'] ="success";
							$final_output['message'] ="Reply successfully added";
							$final_output['data'] = array('comment_id'=>$comment_id,'reply_id'=>$reply_id);
						}else
						{
							$final_output['status'] ="failed";
							$final_output['message'] ="Something went wrong";
							unset($final_output['data']);
						}
					}else
					{
						$final_output['status'] = "failed";
						$final_output['message'] ="Something went wrong! please try again later.";
						unset($final_output['data']);
					}
				}else
			    {
			     	$final_output['status'] = 'No Request parameter';
			    }
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function GetCommentList()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$created = $this->input->get('created');
				$forum_id = $this->input->get('forum_id');
				$final_output = array();
				$arr = array();
				$wheree = '';
				if($created != 0)
				{
					$wheree = "AND create_at < '$created'";
				}
				$check_row = $this->db->query("SELECT * FROM forum_comment WHERE forum_id = '$forum_id' ".$wheree." ORDER BY comment_id DESC LIMIT 10");
		     	$getdata=$check_row->result();
				if($getdata)
				{
					foreach ($getdata as $key) {
						$replydatacount = $this->db->query("SELECT count(reply_id) as rcount FROM forum_comment_reply WHERE comment_id = '".$key->comment_id."'")->row();
						$image = $name = '';
						$username = $this->db->select('name,profile_pic')->get_where("user",array('user_id'=>$key->user_id))->row();
						if(!empty($username->profile_pic))
						{
							$image = base_url().'uploads/user_image/'.$username->profile_pic;
						}
						$key->name = $username->name;
						$key->profile_pic = $image;
						$key->reply_count = $replydatacount->rcount;
						$arr[] = $key;
					}
					$final_output['status'] ="success";
					$final_output['message'] ="Successfull";
					$final_output['data'] = $arr;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Forum Comment not found";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Getreplylist()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				///$created = $this->input->get('created');
				$comment_id = $this->input->get('comment_id');
				$final_output = array();
				$arr = array();
				/*$wheree = '';
				if($created != 0)
				{
					$wheree = "AND create_at < '$created'";
				}*/
				$check_row = $this->db->query("SELECT * FROM forum_comment_reply WHERE comment_id = '$comment_id' ORDER BY reply_id ASC");
		     	$getdata=$check_row->result();
				if($getdata)
				{
					foreach ($getdata as $key) {
						$image = $name = '';
						if($key->type == 'user'){
							$username = $this->db->select('name,profile_pic')->get_where("user",array('user_id'=>$key->user_id))->row();
							if(!empty($username->profile_pic))
							{
								$image = base_url().'uploads/user_image/'.$username->profile_pic;
							}
							$name = $username->name;
						}elseif($key->type == 'provider')
							{
								$username = $this->db->select('name,image')->get_where("service_provider_tb",array('provider_id'=>$key->user_id))->row();
								if(!empty($username->profile_pic))
								{
									$image = base_url().'uploads/user_image/'.$username->image;
								}
								$name = $username->name;
							}else
							{
								$name= 'Admin';
								$image = '';
							}
						$key->name = $name;
						$key->profile_pic = $image;
						$arr[] = $key;
					}
					$final_output['status'] ="success";
					$final_output['message'] ="Successfull";
					$final_output['data'] = $arr;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="No reply found on this comment";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Chat_with_provider()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$json1 = file_get_contents('php://input');
			    $json_array = json_decode($json1);
			  	if(!empty($json_array))
			    {
					$final_output = array();
					//$sele_cate = $this->common_model->getData("health_basic_info",array('user_id'=>0));
					$provider_id = $json_array->provider_id;
					$prescription_id = $json_array->prescription_id;
					$message = $json_array->message;
					$selprovider = $this->db->select("provider_id")->get_where("service_provider_tb",array('provider_id'=>$provider_id,'del_status'=>0))->row();
					if(!empty($selprovider))
					{	
						$insertdata = $this->common_model->common_insert('Chat_with_provider',array('user_id'=>$userid,'type'=>'user','provider_id'=>$provider_id,'prescription_id'=>$prescription_id,'message'=>$message,'create_at'=>date('Y-m-d H:i:s'),'update_at'=>date('Y-m-d H:i:s')),array());
						if($insertdata)
						{
							$final_output['status'] ="success";
							$final_output['message'] ="Message Successfully sent";
							unset($final_output['data']);
						}else
						{
							$final_output['status'] ="failed";
							$final_output['message'] ="Something went wrong";
							unset($final_output['data']);
						}
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="Provider not found";
						unset($final_output['data']);
					}
				}else
			    {
			     	$final_output['status'] = 'No Request parameter';
			    }
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}


	public function ServiceProviderList()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$created = $this->input->get('created');
				$speciality_id = $this->input->get('speciality_id');
				$final_output = array();
				$wheree = '';
				if($created != 0)
				{
					$wheree = "AND create_at < '$created'";
				}
				$check_row = $this->db->query("SELECT provider_id,name,create_at FROM service_provider_tb WHERE speciality_id = '$speciality_id' AND del_status = 0 ".$wheree." ORDER BY provider_id DESC LIMIT 10");
		     	$getdata=$check_row->result();
				if($getdata)
				{
					$final_output['status'] ="success";
					$final_output['message'] ="Successfull";
					$final_output['data'] = $getdata;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Service Provider List Not Found";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Update_profile()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$username = $this->input->post('username');
				if($username !='')
				{
					$final_output = array();
					if(isset($_FILES['profile_pic']['name']) && $_FILES['profile_pic']['name'] != '')
		    		{
		    			$image = md5(militime.$_FILES['profile_pic']['name']).".jpg";
		    			move_uploaded_file($_FILES['profile_pic']['tmp_name'],'uploads/user_image/'.$image);
		    			$url = base_url().'uploads/user_image/'.$image;
		    		}else
		    		{
		    			$image = '';
		    			$url = '';
		    			if($check['data']->profile_pic)
		    			{
		    				$image = $check['data']->profile_pic;
		    				$url = base_url().'uploads/user_image/'.$check['data']->profile_pic;
		    			}
		    		}

					$updatepro = $this->common_model->updateData('user',array('name'=>$username,'profile_pic'=>$image,'update_at'=>militime),array('user_id'=>$userid));
					if($updatepro)
					{
						$final_output['status'] ="success";
						$final_output['message'] ="Profile Successfully Updated";
						$final_output['data'] = array('name'=>$username,'profile_pic'=>$url);
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="Something went wrong! please try again later";
						unset($final_output['data']);
					}
				}else
				{
					$final_output['status'] = "failed";
					$final_output['message'] = "Empty Request Parameter";
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Search_Generic_medicine()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$create_at = $this->input->post('create_at');
				$search_key = $this->input->post('search_key');
				$category_id = $this->input->post('category_id');
				$final_output = array();
				$wheree = '';
				if($create_at != 0)
				{
					$wheree = "AND create_at < '$create_at'";
				}
				$searchkey = '';
				if(!empty($search_key))
				{
					$searchkey = "AND medicine_name like '%$search_key%'";
				}
				$arr =array();
				$check_row = $this->db->query("SELECT * FROM medicine WHERE type = 1 AND category= '$category_id' ".$searchkey." ".$wheree." ORDER BY medicine_id DESC LIMIT 4")->result();
		     	if($check_row)
				{
					$image = '';
					foreach ($check_row as $key) {
						if($key->medicine_image!='')
						{
							$image = base_url().'uploads/medicine_image/'.$key->medicine_image;
						}
						$key->medicine_image = $image;
						$arr[] = $key;
					}
					$final_output['status'] ="success";
					$final_output['message'] ="Successfull";
					$final_output['data'] = $arr;
					$final_output['search_key'] = $search_key;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Medicine Not Found";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Add_to_Cart()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				//$doctor_id = $check['data']->doctor_id;
				$json1 = file_get_contents('php://input');
			    $json_array = json_decode($json1);
			  	if(!empty($json_array))
			    {
			    	$json_array->user_id = $userid;
					$quantity = $json_array->quantity;
					$medicine_id = $json_array->medicine_id;
					$type = $json_array->type;
					$final_output = array();
					if($quantity > 0)
					{
						$selquty = $this->common_model->common_getRow("medicine",array('medicine_id'=>$medicine_id));
						if($selquty->quantity >= $quantity)
						{
							$sel_cart = $this->common_model->common_getRow("user_cart",array('user_id'=>$userid,'medicine_id'=>$medicine_id));
					     	if($sel_cart)
							{
								if($type==1)
								{
									$newqty = $sel_cart->medicine_quantity+$quantity;
									if($newqty > $selquty->quantity)
									{
										$final_output['status'] ="failed";
										$final_output['message'] ="Only ".$selquty->quantity." quantity is available in stock.";
										unset($final_output['data']);
									}else
									{
										$updatecart = $this->common_model->updateData("user_cart",array('medicine_quantity'=>$newqty,'update_at'=>militime),array('user_id'=>$userid,'medicine_id'=>$medicine_id));
									}
								}else
								{
									$updatecart = $this->common_model->updateData("user_cart",array('medicine_quantity'=>$quantity,'update_at'=>militime),array('user_id'=>$userid,'medicine_id'=>$medicine_id));
								}
								if($updatecart)
								{
									$countt = $this->db->query("SELECT count(user_cart_id) as cartcount FROM user_cart WHERE user_id = '$userid'")->row();
									$final_output['status'] ="success";
									$final_output['message'] ="Cart Successfully Updated";
									$final_output['data'] = array('cart_id'=>$sel_cart->user_cart_id,'cart_count'=>$countt->cartcount);	
								}else
								{
									$final_output['status'] ="failed";
									$final_output['message'] ="Something went wrong! please try again later.";
									unset($final_output['data']);
								}
								
							}else
							{
								$insercart = $this->common_model->common_insert("user_cart",array('user_id'=>$userid,'medicine_id'=>$medicine_id,'medicine_quantity'=>$quantity,'create_at'=>militime,'update_at'=>militime),array());
								if($insercart)
								{
									$countt = $this->db->query("SELECT count(user_cart_id) as cartcount FROM user_cart WHERE user_id = '$userid'")->row();
									$final_output['status'] ="success";
									$final_output['message'] ="Cart Successfully Added";
									$final_output['data'] =  array('cart_id'=>$insercart,'cart_count'=>$countt->cartcount);
								}else
								{
									$final_output['status'] ="failed";
									$final_output['message'] ="Something went wrong! please try again later.";
									unset($final_output['data']);
								}
							}
						}else
						{
							$final_output['status'] ="failed";
							$final_output['message'] ="Only ".$selquty->quantity." quantity is available in stock.";
							unset($final_output['data']);
						}
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="You have to select atleast one quantity";
						unset($final_output['data']);
					}
				}else
			    {
			     	$final_output['status'] = 'No Request parameter';
					unset($final_output['data']);
			    }
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
				unset($final_output['data']);
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
			unset($final_output['data']);
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Add_shipping_address()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$name = $this->input->post('name');
				$address = $this->input->post('address');
				$zip_code = $this->input->post('zip_code');
				$country = $this->input->post('country');
				$state = $this->input->post('state');
				$city = $this->input->post('city');
				$final_output = array();
				$sel_cart = $this->db->query("SELECT count(user_id) as addcount FROM address WHERE user_id = '$userid' AND type != 1")->row();
		     	if($sel_cart)
				{
					if($sel_cart->addcount >3)
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="You can add maximum 4 address.";
						unset($final_output['data']);
					}else
					{
						$insertaddress = $this->common_model->common_insert("address",array('user_id'=>$userid,'name'=>$name,'address'=>$address,'zip'=>$zip_code,'country'=>$country,'state'=>$state,'city'=>$city,'create_at'=>militime,'update_at'=>militime),array());
						if($insertaddress)
						{
							$final_output['status'] ="success";
							$final_output['message'] ="Shipping Address Successfully Added";
							$final_output['data'] = array('address_id'=>$insertaddress);	
						}else
						{
							$final_output['status'] ="failed";
							$final_output['message'] ="Something went wrong! please try again later.";
							unset($final_output['data']);
						}
					}
				}else
				{
					$insertaddress = $this->common_model->common_insert("address",array('user_id'=>$userid,'name'=>$name,'address'=>$address,'zip'=>$zip_code,'country'=>$country,'state'=>$state,'city'=>$city,'create_at'=>militime,'update_at'=>militime),array());
					if($insertaddress)
					{
						$final_output['status'] ="success";
						$final_output['message'] ="Shipping Address Successfully Added";
						$final_output['data'] = array('address_id'=>$insertaddress);	
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="Something went wrong! please try again later.";
						unset($final_output['data']);
					}
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Shipping_address_list()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$final_output = array();
				$sel_cart = $this->common_model->getData('address',array('user_id'=>$userid,'type'=>0),'address_id','DESC');
		     	if($sel_cart)
				{
					$final_output['status'] ="success";
					$final_output['message'] ="Successfull";
					$final_output['data'] = $sel_cart; 
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Shipping address not found.";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Delete_Shipping_address()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$address_id = $this->input->post('address_id');
				$final_output = array();
				$sel_cart = $this->common_model->common_getRow('address',array('user_id'=>$userid,'address_id'=>$address_id));
		     	if(!empty($sel_cart))
				{
					$deleteadd = $this->common_model->deleteData("address",array('address_id'=>$address_id));
					if($deleteadd)
					{
						$final_output['status'] ="success";
						$final_output['message'] ="Successfully Deleted";
						unset($final_output['data']);
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="Something went wrong! please try again later.";
						unset($final_output['data']);
					}
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Shipping address not found.";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Cart_detail()
	{
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$user_id = $check['data']->user_id;
				$final_output = array();
				$arr = (object)array();
				$shipcharge = 0;
				$sel_cart = $this->common_model->getData('user_cart',array('user_id'=>$user_id),'user_cart_id','DESC');
				if($sel_cart)
				{
					$shipcharg = $this->common_model->common_getRow('shipping_charge',array('id'=>1));
					if($shipcharg)
					{
						$shipcharge = $shipcharg->shipping_charge;
					}
					$countt = $this->db->query("SELECT count(user_cart_id) as cartcount FROM user_cart WHERE user_id = '$user_id'")->row();
					
					foreach ($sel_cart as $key) {
						$check_row = $this->common_model->common_getRow('medicine',array('medicine_id'=>$key->medicine_id));
						if($check_row)
						{
							$arr = $check_row;
						}
						

						$final_arr[] = array(
								'cart_id'=>$key->user_cart_id,
								'medicine_quantity'=>$key->medicine_quantity,
								'medicine_detail'=>$arr,
								'cart_count'=>$countt->cartcount,
								'shipping_charge'=>$shipcharge
								);
					}
				}
				if($arr == (object)array() || $arr == null)
				{
					$final_output['status']="failed";
					$final_output['message']="empty cart";
					unset($final_output['data']);
				}else
				{
					$final_output['status']="success";
					$final_output['message']="Successfull";
					$final_output['shipping_charge']=$shipcharge;
					$final_output['data']=$final_arr;
				}
		}else
		{
				$final_output['status'] ="false";
			$final_output['message'] = "Invalid Token";
		}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
		 header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//End Cart List
	
	public function Item_remove_from_cart()
	{
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$user_id = $check['data']->user_id;
				$medicine_id = $this->input->post('medicine_id');
				$final_output = array();
				$sel_cart = $this->common_model->common_getRow('user_cart',array('user_id'=>$user_id,'medicine_id'=>$medicine_id));
				if($sel_cart)
				{
					$countt = $this->db->query("SELECT count(user_cart_id) as cartcount FROM user_cart WHERE user_id = '$user_id'")->row();
					$delete = $this->common_model->deleteData("user_cart",array('user_id'=>$user_id,'medicine_id'=>$medicine_id));	
					if($delete)
					{
						$final_output['status']="success";
						$final_output['message']="Medicine Successfull Removed From Cart.";
						$final_output['cart_count'] = $countt->cartcount;
					}
				}else
				{
					$final_output['status']="failed";
					$final_output['message']="Item not found in cart";
					unset($final_output['data']);
				}
		}else
		{
				$final_output['status'] ="false";
			$final_output['message'] = "Invalid Token";
		}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
		 header("content-type: application/json");
	    echo json_encode($final_output);
	}
	//End Remove from Cart
	public function Cart_checkout()
	{
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$address_id = $this->input->post('address_id');
				$total_amount = $this->input->post('total_amount');
				$coupon_code = $this->input->post('coupon_code');
				$coupon_id = $this->input->post('coupon_id');
				$discount_amount = $this->input->post('discount_amount');
				$pay_amount = $this->input->post('pay_amount');
				$shipping_amount = $this->input->post('shipping_amount');
				$payment_method = $this->input->post('payment_method');
				$transaction_id = $this->input->post('transaction_id');
				$data = array();
				$curnt_date = date('Y-m-d 00:00:00');
				$offerdetail = 'no';
				$shipcharge =0;
				$cart = $this->common_model->getData("user_cart",array('user_id'=>$userid));
				if($cart)
				{
						$address = $this->db->query("SELECT * FROM address WHERE user_id = '$userid' AND type != 1")->row();
						if($address)
						{
							if(!empty($coupon_code))
							{
								$sel_coupon = $this->db->get_where("coupan_tb",array('coupon_code'=>$coupon_code,'user_id'=>$userid,'type'=>1,'admin_status'=>0))->row();
								if(!empty($sel_coupon))
								{
										if($sel_coupon->status == 0)
										{
											if($sel_coupon->start_date > $curnt_date)
											{ 
												$final_output['status'] ="failed";
												$final_output['message'] ="Coupon code not activate";
											}elseif($sel_coupon->end_date < $curnt_date)
											{
												$final_output['status'] ="failed";
												$final_output['message'] ="Coupon code has been expired";
											}else
											{
												$disamt = $total_amount * $sel_coupon->discount / 100;
												if($disamt != $discount_amount && $discount_amount != $sel_coupon->max_discount)
												{
													$final_output['status'] ="failed";
													$final_output['message'] ="Coupon code has been expired";
												}else
												{
													$offerdetail = json_encode(array('coupon_code'=>$coupon_code,'discount'=>$discount_amount));
												}
											}
										}else
										{
											$final_output['status'] ="failed";
											$final_output['message'] ="You have already used this coupon code.";
										}
										if(isset($final_output['status']) && $final_output['status']=='failed')
										{
											header("content-type: application/json");
	    									echo json_encode($final_output);
	    									exit;
										}
								}else
								{
									$final_output['status'] ="failed";
									$final_output['message'] ="Either coupon code has been expired or you have entered invalid code";
									header("content-type: application/json");
									echo json_encode($final_output);
									exit;
								}
							}
							$add_detail = array(
											'name'=>$address->name,
											'address'=>$address->address,
											'zip'=>$address->zip,
											'country'=>$address->country,
											'state'=>$address->state,
											'city'=>$address->city,
											);	
							$shipcharg = $this->common_model->common_getRow('shipping_charge',array('id'=>1));
							if($shipcharg)
							{
								$shipcharge = $shipcharg->shipping_charge;
							}
							$total_price = $sale_price = $shipp_price = $groce_amt = 0;
							foreach ($cart as $key) {
								$medicine = $this->common_model->common_getRow("medicine",array("medicine_id"=>$key->medicine_id));
								if($medicine)
								{
									if($medicine->quantity >0)
									{
										if($medicine->quantity >= $key->medicine_quantity)
										{
											$cart_id[] = $key->user_cart_id;
											$m_qty[] = array('m_id'=>$key->medicine_id,'m_qty'=>$key->medicine_quantity);
											$total_price = $total_price+$medicine->sale_price*$key->medicine_quantity; 
											$sale_price = $sale_price+$medicine->sale_price*$key->medicine_quantity; 
											//$shipp_price = $shipp_price+$medicine->shipping; 
											$payamt = $sale_price-$discount_amount; 
											$groce_amt = $groce_amt+$medicine->sale_price*$key->medicine_quantity;
											$gross = $medicine->sale_price*$key->medicine_quantity;
												
						                   		if(!empty($medicine->medicine_image))
												{
													$mimage = $medicine->medicine_image;
												}else
												{
													$mimage = '';
												}
												$medicine->medicine_image = $mimage;
												$order_detail[] = array(
																'id'=>$medicine->medicine_id,
																'name'=>$medicine->medicine_name,
																'image'=>$mimage,
																'price'=>$medicine->price,
																'sale_price'=>$medicine->sale_price,
																//'shipping'=>$medicine->shipping,
																'quantity'=>$key->medicine_quantity,
																'gross_amount'=>$gross
																);
												//$soldby[] = $medicine->sold_by;  sold by 
										}else
										{
											$final_output['status'] = "failed";
											$final_output['message'] = "Insufficent stock available.";
											header("content-type: application/json");
			    							echo json_encode($final_output);exit;	
										}
									}else
									{
										$final_output['status'] = "failed";
										$final_output['message'] = "Out of stock";
										header("content-type: application/json");
		    							echo json_encode($final_output);exit;		
		    							//Out of stock
									}
								}else
								{
									$final_output['status'] = "failed";
									$final_output['message'] = "Medicine not available.";
									header("content-type: application/json");
	    							echo json_encode($final_output);exit;
								}
								$delivery_status = array();  $payment_status = array();
		                        if($payment_method == 'cash_on_delivery')
		                        {
		                            $delivery_status = 'pending';
	                                $payment_status = 'due';
	                                $payment_details = 'cash';
		                        }elseif($payment_method == 'online')
		                        {   
		                           	$delivery_status = 'pending';
	                                $payment_status = 'paid';
	                                $payment_details = $transaction_id;
		                        }
		                  		$data = array('buyer_id'=>$userid,'product_detail'=>json_encode($order_detail),'shipping_charge'=>$shipcharge,'shipping_address'=>json_encode($add_detail),
	                             'payment_type'=>$payment_method,'payment_status'=>$payment_status,'payment_details'=>$payment_details,'grand_total'=>$payamt,
	                              'sale_datetime'=>time(),'delivary_datetime'=>'','delivery_status'=>$delivery_status,'offer_detail'=>$offerdetail,'create_at'=>militime,'update_at'=>militime,'type'=>0
	                             );
							}
							if(!empty($data))
							{
								$payamt = $payamt+$shipcharge;
								/*	echo $payamt; echo "</pre>";
								echo $pay_amount; echo "</pre>";
								echo $total_price; echo "</pre>";
								echo $total_amount; echo "</pre>";
								echo $shipcharge; echo "</pre>";
								echo $shipping_amount; echo "</pre>";exit;*/
								
								if($payamt == $pay_amount && $total_price == $total_amount && $shipcharge == $shipping_amount)
								{
									$insertorder = $this->common_model->common_insert("order_table",$data,array());
									if($insertorder)
									{
										$impcartid = implode(',', $cart_id);
										$select = $this->common_model->common_getRow("order_table",array('order_id'=>$insertorder));
										//$saller = implode(',',json_decode($select->sold_by));
										$salecode = date('Ym', $select->sale_datetime) . $insertorder;
										$pro_detail = json_decode($select->product_detail);
		                            	$update = $this->common_model->updateData("order_table",array('invoice_no'=>$salecode),array('order_id'=>$insertorder),array());
		               					$message = "Thank you for your order! we will send a confirmation when your order(#".$insertorder.") dispatches. Total Payable Amount: Rs:".$select->grand_total;
										$msg = array('title'=>'Order Place Successfully','type'=>1,'message'=>$message,'image'=>base_url().'uploads/medicine_image/'.$pro_detail[0]->image,'order_id'=>$insertorder,'create_at'=>militime);
										$msgkey = array('title'=>'Order Place Successfully','type'=>1,'msg'=>$message,'image'=>base_url().'uploads/medicine_image/'.$pro_detail[0]->image,'order_id'=>$insertorder,'create_at'=>militime);
										$deletecart = $this->db->query("DELETE FROM user_cart WHERE user_cart_id IN ($impcartid)");
										for ($i=0; $i < count($m_qty); $i++) { 
										$this->db->where('medicine_id',$m_qty[$i]['m_id'])->set('quantity', 'quantity - '.$m_qty[$i]["m_qty"].'', FALSE)->update('medicine');
										}
										$this->common_model->sendPushNotification($check['data']->device_token,$msg);
										$insertorder = $this->common_model->common_insert("notification",array('receiver_id'=>$userid,'notification_type'=>1,'order_id'=>$insertorder,'user_type'=>1,'msg'=>json_encode($msgkey),'create_at'=>militime),array());
										if(!empty($coupon_code))
										{
											$updatecode = $this->common_model->updateData("coupan_tb",array('status'=>1,'update_at'=>militime),array('coupon_code'=>$coupon_code,'user_id'=>$userid));
										}
										$sel_temp = $this->common_model->common_getRow("email_template_descriptions",array('id'=>1));
							            if($sel_temp)
							            {
							                $content = $sel_temp->content;
							            }else
						              	{
						                    $content = ''; 
						              	}
						              	
						              	$message=stripcslashes($content);
						               
					              		//$message=str_replace("{selleraddress}",$result['data'][0]['address1'].",".$result['data'][0]['address2'].",".$result['data'][0]['city_name']."-".$result['data'][0]['pincode']." ".$result['data'][0]['region_name'].",".$result['data'][0]['country_name'],$message);
						              	//$message=str_replace("{vatno}",$result['data'][0]['vat_no'],$message);
						              	
						              	//$message=str_replace("{tinno}",$result['data'][0]['tin_no'],$message);
						              	
						              	/*$message=str_replace("{sno}",$i,$message);
						              	$message=str_replace("{productname}",$pro_detail[$i]->name,$message);
						              	$message=str_replace("{quantity}",$pro_detail[$i]->quantity,$message);
						              	$message=str_replace("{price}",$pro_detail[$i]->price,$message);
						              	$message=str_replace("{subtotal}",$pro_detail[$i]->gross_amount,$message);
						              	$message=str_replace("{taxrate}",0,$message);
						              	$message=str_replace("{tax}",0,$message);*/
						              	$msg ='';
						               	$subject = "Order#". $salecode." Confirmation Invoice";
						              	$message=str_replace("{salecode}",$salecode,$message);
						              	$message=str_replace("{datetime}",date('d M, Y',$select->sale_datetime),$message);
						             	$message=str_replace("{username}",$check['data']->name,$message);
						              	$message=str_replace("{useraddress}",$address->address,$message);
						              	$message=str_replace("{city}",$address->city,$message);
						              	$message=str_replace("{zip}",$address->zip,$message);
						              	$message=str_replace("{contact}",$check['data']->mobile,$message);
						              	$message=str_replace("{email}",$check['data']->email,$message);
						              	$message=str_replace("{paymentstatus}",$select->payment_status,$message);
						              	$message=str_replace("{paymentmethod}",$select->payment_type,$message);
						              	$message=str_replace("{orderid}",$insertorder,$message);
						             	$message=str_replace("{logo}",base_url().'template/assets/layouts/layout4/img/login_logo.png',$message);
					              	  	//$message=str_replace("{sellername}",$saller,$message);
						              	$message=str_replace("{subtotal}",$groce_amt,$message);
						              	$message=str_replace("{grandtotal}",$select->grand_total,$message);
						              	$message=str_replace("{payable}",$select->grand_total,$message);
					              		$message=str_replace("{shipping}",$shipcharge,$message);
					              		$message=str_replace("{discount}",$discount_amount,$message);
					              		$message=str_replace("{tax}",0.00,$message);
						              	for ($i=0; $i < count($pro_detail); $i++) { 
						              		$no=$i+1;
						            		$msg .= '<tr>
					                            <td style="padding: 5px;text-align:center;background:rgba(128, 128, 128, 0.18)">'.$no.'</td>
					                            <td style="padding: 5px;text-align:center;background:rgba(128, 128, 128, 0.18)">'. $pro_detail[$i]->name.'</td>
					                            <td style="padding: 5px;text-align:center;background:rgba(128, 128, 128, 0.18)">'. $pro_detail[$i]->quantity.'</td>
					                            <td style="padding: 5px;text-align:center;background:rgba(128, 128, 128, 0.18)">Rs.'. $pro_detail[$i]->sale_price.'</td>
					                            <td style="padding: 5px;text-align:center;background:rgba(128, 128, 128, 0.18)">0</td>
					                            <td style="padding: 5px;text-align:center;background:rgba(128, 128, 128, 0.18)">Rs.0.00</td>
					                            <td style="padding: 5px;text-align:center;background:rgba(128, 128, 128, 0.18)">Rs.'.$pro_detail[$i]->gross_amount.'</td>
					                        </tr>';
							     	    } 
						            	$message=str_replace("{product_detail}",$msg,$message);
						            	$email_from = 'support@ach.com';
						              	$email = $check['data']->email;
						              	$headers  = 'MIME-Version: 1.0' . "\r\n";
						                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						                $headers .= 'From: '.$email_from. '\r\n';            // Mail it
						           		@mail($email, $subject,$message,$headers);			
										$final_output['status'] = "success";
										$final_output['message'] = "Order successfully placed";
										unset($final_output['data']);
									}else
									{
										$final_output['status'] = "failed";
										$final_output['message'] = "Something went wrong! please try again later.";
									}
								}else
								{
									$final_output['status'] = "failed";
									$final_output['message'] = "Please pay valid amount";
									//please pay valid amt
								} 
							}else
							{
								$final_output['status'] = "failed";
								$final_output['message'] = "Something went wrong! please try again later.";
							}		
						}else
						{
							$final_output['status'] = "failed";
							$final_output['message'] = "Invalid Address";
							//address not found
						}
					
				}else
				{
					$final_output['status'] = "failed";
					$final_output['message'] = "Empty cart";
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
		header("content-type: application/json");
		echo json_encode($final_output);
	}

	public function Get_all_diagnosis_que_ans()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$arr = array();
				$basicinfo = $this->db->get_where("health_basic_info",array('user_id'=>$userid))->result();
				if(!empty($basicinfo))
				{
					foreach ($basicinfo as $key) {
					
						$basquearr =  array();
						$basicques = $this->db->query("SELECT basic_diagnosis_answer.*,quetions.question,answer.answer FROM basic_diagnosis_answer INNER JOIN quetions ON basic_diagnosis_answer.question_id = quetions.question_id INNER JOIN answer ON basic_diagnosis_answer.answer = answer.answer_id WHERE basic_diagnosis_answer.basic_info_id = ".$key->info_id." ")->result();
						if(!empty($basicques))
						{
							foreach ($basicques as $value) {
								
								$basquearr[] = array(
									'question'=>$value->question,
									'answer'=>$value->answer
									);
							}
						}
						 $arrimg = $advquearr = array();

						 $providername = $coment = $proimag = $proname = '';

						if($key->diagnosis_payment_status==1)
						{
							$advanceque = $this->db->query("SELECT advance_diagnosis_answer.*,quetions.question,answer.answer FROM advance_diagnosis_answer INNER JOIN quetions ON advance_diagnosis_answer.question_id = quetions.question_id INNER JOIN answer ON advance_diagnosis_answer.answer_id = answer.answer_id WHERE advance_diagnosis_answer.basic_info_id = ".$key->info_id." ")->result();
							if(!empty($advanceque))
							{
								foreach ($advanceque as $keyvalue) {
									$advquearr[] = array(
										'question'=>$keyvalue->question,
										'answer'=>$keyvalue->answer,
									);
								}
							}
							$otherinfo = $this->db->get_where("other_diagnosis_info",array('user_id'=>$userid,'basic_info_id'=>$key->info_id))->row();
							if(!empty($otherinfo))
							{
								if(!empty($otherinfo->image))
								{
									$expimg = explode(',', $otherinfo->image);
									for ($i=0; $i < count($expimg); $i++) { 
										$arrimg[] = base_url().'/uploads/injury_image/'.$expimg[$i];
									}
									$coment = $otherinfo->comment;
								}
								if(!empty($otherinfo->doctor_id))
								{
									$selectdoc = $this->db->select('name,image')->get_where('service_provider_tb',array('provider_id'=>$otherinfo->doctor_id))->row();
									if(!empty($selectdoc->image))
									{
										$proimag= base_url().'uploads/provider_image/'.$selectdoc->image;
									}
									$proname = $selectdoc->name;
								}
							}
						}
						$key->provider_name = $providername;
						$key->comment = $coment;
						$key->injury_image = $arrimg;
						$key->provider_image = $proimag;
						$key->provider_name = $proname;
						$key->basic_question = $basquearr;
						$key->advan_question = $advquearr;
						$arr[] = $key;
					}
				}
				$final_output['status'] = "success";
				$final_output['message'] = "Diagnosis History";
				$final_output['data'] = $arr;
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	
	public function Chat_list()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$prescription_id = $this->input->get('prescription_id');
				//$created = $this->input->get('created');
				$final_output = array();
				$wheree = '';
				/*if($created != 0)
				{
					$wheree = "WHERE create_at < '$created'";
				}*/
				$check_row = $this->db->query("SELECT * FROM Chat_with_provider WHERE user_id = '$userid' AND prescription_id = '$prescription_id' ORDER BY chat_id ASC");
		     	$getdata=$check_row->result();
				if($getdata)
				{
					foreach ($getdata as $key) {
						$ar[] = array(
								'user_id'=>$key->user_id,
								'provider_id'=>$key->provider_id,
								'type'=>$key->type,
								'message'=>$key->message,
								'chat_date'=>date('d M Y H:i:s',strtotime($key->create_at))
								);			
					}
					$final_output['status'] ="success";
					$final_output['message'] ="Successfull";
					$final_output['data'] = $ar;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Forum List not found";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}
	
	public function Order_history()
	{
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$final_output = array();
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$user_id = $check['data']->user_id;
				$create_at = $this->input->post('create_at');
				$create = '';
				if($create_at != 0 && $create_at != '')
				{
					$create = "AND create_at < '$create_at'";
				}
				$order = $this->db->query("SELECT * FROM order_table WHERE buyer_id ='$user_id'  AND type=0 ".$create." ORDER BY order_id DESC LIMIT 10")->result();
				if($order)
				{
					foreach ($order as $key) {
						
						$prodetail =array(); $total_ship = 0;
						$productdetail = json_decode($key->product_detail);
						for ($i=0; $i <count($productdetail) ; $i++) { 
							if($productdetail[$i]->image){ $img = base_url().'uploads/medicine_image/'.$productdetail[$i]->image; }else{ $img='';}
							$productdetail[$i]->image = $img;
							$prodetail[] = $productdetail[$i];
							//$total_ship += $productdetail[$i]->shipping;
						}
						$key->product_detail = $prodetail; 
						$address = json_decode($key->shipping_address);
						$code = ''; $c_amt = 0;
						if(!empty($key->offer_detail) && $key->offer_detail!='no')
						{
							$offerdetail = json_decode($key->offer_detail);
							$code = $offerdetail->coupon_code;
							$c_amt = $offerdetail->discount;
						}
						$key->coupon_code = $code;
						$key->discount_amount = $c_amt;
						$key->shipping_address = $address;
						$key->total_shipping = $key->shipping_charge;
						$arr[] = $key;
						
					}
					$final_output['status']= 'success';
					$final_output['message']= 'Order List';
					$final_output['data']= $arr;
				}else
				{
					$final_output['status']= 'failed';
					$final_output['message']= 'No order found';
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
		header("content-type: application/json");
		echo json_encode($final_output);
	}

	public function cancel_order()
	{
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$final_output = array();
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$user_id = $check['data']->user_id;
				$order_id = $this->input->post('order_id');
				$order = $this->common_model->common_getRow("order_table",array('order_id'=>$order_id,'buyer_id'=>$user_id));
				if($order)
				{
					$update = $this->common_model->updateData('order_table',array('delivery_status'=>'cancelled','delivary_datetime'=>date('Y-m-d H:i:s'),'update_at'=>militime),array('order_id'=>$order_id));
					if($update)
					{
						$final_output['status']= 'success';
						$final_output['message']= 'Order successfully cancelled';
					}else
					{
						$final_output['status']= 'failed';
						$final_output['message']= 'Something went wrong! please try again later';
					}
				}else
				{
					$final_output['status']= 'failed';
					$final_output['message']= 'Order not found';
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
		header("content-type: application/json");
		echo json_encode($final_output);
	}

	public function Get_speciality_list()
	{		
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$final_output = array();
				$sele_cate = $this->common_model->getData("speciality",array('status'=>1));
				if($sele_cate)
				{
					foreach ($sele_cate as $key) {
						$arr[] = array(
									'speciality_id'=>$key->speciality_id,
									'speciality_name'=>$key->speciality_name,
									);
					}
					$final_output['status'] ="success";
					$final_output['message'] ="successfull";
					$final_output['data'] = $arr;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Empty speciality List";
					unset($final_output['data']);
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function coupon_code()
	{	
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$coupon_code = $this->input->post('coupon_code');
				$curnt_date = date('Y-m-d 00:00:00');
				if($coupon_code!= '')
				{
					$sel_coupon = $this->db->get_where("coupan_tb",array('coupon_code'=>$coupon_code,'user_id'=>$userid,'type'=>1,'admin_status'=>0))->row();
					if(!empty($sel_coupon))
					{
						if($sel_coupon->status == 0)
						{
							if($sel_coupon->start_date > $curnt_date)
							{ 
								$final_output['status'] ="failed";
								$final_output['message'] ="Coupon code not activate";
							}elseif($sel_coupon->end_date < $curnt_date)
							{
								$final_output['status'] ="failed";
								$final_output['message'] ="Coupon code has been expired";
							}else
							{
								$final_output['status'] ="success";
								$final_output['message'] ="Coupon code successfully applied";
								$final_output['data'] = array('coupon_id'=>$sel_coupon->coupon_id,'discount'=>$sel_coupon->discount,'max_discount'=>$sel_coupon->max_discount);
							}
						}else
						{
							$final_output['status'] ="failed";
							$final_output['message'] ="You have already used this coupon code.";
						}
					}else
					{
						$final_output['status'] ="failed";
						$final_output['message'] ="Either coupon code has been expired or you have entered invalid code";
					}
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="No Request Parameter";
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function get_offer_list()
	{	
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$curnt_date = date('Y-m-d 00:00:00');
				$arr = array();
				$this->db->order_by('coupon_id DESC');
				$this->db->limit('10');
				$sel_coupon = $this->db->get_where("coupan_tb",array('user_id'=>$userid,'type'=>1,'admin_status'=>0))->result();
				if(!empty($sel_coupon))
				{
					foreach ($sel_coupon as $key) {
						if($key->start_date <= $curnt_date && $key->end_date >= $curnt_date)
						{ 
							$status = 1;
						}else
						{
							$status = 0;
						}								
						$arr[] = array('coupon_id'=>$key->coupon_id,'title'=>$key->title,'message'=>$key->message,'coupon_code'=>$key->coupon_code,'discount'=>$key->discount,'max_discount'=>$key->max_discount,'start_date'=>$key->start_date,'end_date'=>$key->end_date,'coupon_status'=>$status);
					}
					$final_output['status'] ="success";
					$final_output['message'] ="Offer list";
					$final_output['data'] = $arr;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="Currently no offer available.";
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function offer_detail_by_id()
	{	
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$userid = $check['data']->user_id;
				$curnt_date = date('Y-m-d 00:00:00');
				$arr = array();
				$coupon_id = $this->input->post('coupon_id');

				$key = $this->db->get_where("coupan_tb",array('coupon_id'=>$coupon_id))->row();
				if(!empty($key))
				{
						if($key->start_date <= $curnt_date && $key->end_date >= $curnt_date)
						{ 
							$status = 1;
						}else
						{
							$status = 0;
						}								
						$arr = array('coupon_id'=>$key->coupon_id,'title'=>$key->title,'message'=>$key->message,'coupon_code'=>$key->coupon_code,'discount'=>$key->discount,'max_discount'=>$key->max_discount,'start_date'=>$key->start_date,'end_date'=>$key->end_date,'coupon_status'=>$status);
					$final_output['status'] ="success";
					$final_output['message'] ="Offer list";
					$final_output['data'] = $arr;
				}else
				{
					$final_output['status'] ="failed";
					$final_output['message'] ="offer detail not found";
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);
	}

	public function Notification()
	{
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$final_output = array();
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$user_id = $check['data']->user_id;
				$create_at = $this->input->post('create_at');
				$create = '';
				if($create_at !=0)
				{
					$create = "AND create_at < '$create_at'";
				}
				$noti_list = $this->db->query("SELECT * FROM notification WHERE receiver_id = '$user_id' AND user_type=1 ".$create." ORDER BY notification_id DESC LIMIT 10")->result();
				if(!empty($noti_list))
				{
					foreach ($noti_list as $key) {
						$msg = json_decode($key->msg);
						if(isset($msg->title))
						{
							$arrqay[] =array(
								'notify_id'=>$key->notification_id,
								'title'=>$msg->title,
								'order_id'=>$key->order_id,
								'msg'=>$msg->msg,
								'type'=>$msg->type,
								'image'=>$msg->image,
								'create_at'=>$key->create_at
								);
						}
					} 
				}
				if(!empty($arrqay))
				{
					$final_output['status']='success';
					$final_output['message']='notification list';
					$final_output['data']= $arrqay;
				}else
				{
					$final_output['status']='failed';
					$final_output['message']='notification not found';
				}
			}else
			{
				$final_output['status'] = 'false';
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
		header("content-type: application/json");
		echo json_encode($final_output);
	}

	public function Get_order_detail_by_id()
	{
		$headers = apache_request_headers();
		if(!empty($headers['secret_key']))
		{
			$check = $this->ChechAuth($headers['secret_key']);
			if($check['status']=="true")
			{
				$order_id = $this->input->post('order_id');
				$user_id = $check['data']->user_id;
				$key = $this->db->query("SELECT * FROM order_table WHERE order_id = '$order_id' AND buyer_id = '$user_id'")->row();
				if($key)
				{
						$prodetail =array(); $total_ship = 0;
						$productdetail = json_decode($key->product_detail);
						for ($i=0; $i <count($productdetail) ; $i++) { 
							if($productdetail[$i]->image){ $img = base_url().'uploads/medicine_image/'.$productdetail[$i]->image; }else{ $img='';}
							$productdetail[$i]->image = $img;
							$prodetail[] = $productdetail[$i];
							//$total_ship += $productdetail[$i]->shipping;
						}
						$key->product_detail = $prodetail; 
						$address = json_decode($key->shipping_address);
						$code = ''; $c_amt = 0;
						if(!empty($key->offer_detail) && $key->offer_detail!='no')
						{
							$offerdetail = json_decode($key->offer_detail);
							$code = $offerdetail->coupon_code;
							$c_amt = $offerdetail->discount;
						}
						$key->coupon_code = $code;
						$key->discount_amount = $c_amt;
						$key->shipping_address = $address;
						$key->total_shipping = $key->shipping_charge;
						$arr = $key;
						
				
					$final_output['status']= 'success';
					$final_output['message']= 'Order detail';
					$final_output['data']= $arr;
				}else
				{
					$final_output['status']= 'failed';
					$final_output['message']= 'No order found';
				}
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['message'] = "Unauthorised access";
		}
	    header("content-type: application/json");
	    echo json_encode($final_output);		
	}
	public function test_otp()
	{
		$this->common_model->sms_send('9754743271',"This is ACH OTP Testing");
	}
	public function ChechAuth($token)
	{
		$auth = $this->common_model->common_getRow('user',array('token'=>$token));
		if(!empty($auth))
		{
			$abc['status'] = "true";
			$abc['data'] =$auth;
			return $abc;
		}else
		{
			$abc['status'] = "false";
			return $abc;
		}
	}

	
}
