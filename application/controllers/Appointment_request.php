<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment_request extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		if(!$userid = $this->session->userdata('admin_id')){
		redirect(base_url('login'));

		$response = $this->common_model->check_auth($this->session->userdata('admin_id'));
	    if($response == 1001)
	    {
	      redirect(base_url().'Logout');
	    }    

		$militime = round(microtime(true) * 1000);
		define('militime', $militime);
	  }
	}
	
	public function index()
	{ 
       $data['Appointment_request'] = $this->common_model->getData('barber_web_appointment',array(),'booking_id','DESC');

	   $this->load->view('admin/web_appointment_request/request_detail',$data);
	}

	public function app_request()
	{
       $data['Appointment_request'] = $this->common_model->getData('barber_customer_booking',array(),'booking_id','DESC');

	   $this->load->view('admin/web_appointment_request/app_request',$data);
	} 

	public function available_barber($booking_id = false,$type = false)
	{
		if($type == 'w')
		{
			$getdata = $this->common_model->common_getRow("barber_web_appointment",array('booking_id'=>$booking_id));
		}
		else if($type == 'd')
		{
			$getdata = $this->common_model->common_getRow("barber_customer_booking",array('booking_id'=>$booking_id));
		}	

		$booking_date = $getdata->booking_date;

		$time = $getdata->booking_time;

		$booking_time = date("H:i", strtotime($time));
       
        $find_day = strtotime($booking_date);

		$day = date('D', $find_day);

		$day = strtolower($day);
		
		$avaibility = $this->db->query("SELECT barber_user.user_id,barber_availability.always,barber_availability.".$day." FROM barber_user LEFT JOIN barber_availability ON barber_user.user_id = barber_availability.barber_id WHERE barber_user.mobile_status = 1 AND barber_user.admin_status = 1 AND barber_user.email_status = 1 AND barber_user.user_type = 2")->result();

		$available_barber = array();

		foreach($avaibility as $key)
		{
			if($key->always == 1 OR $key->always == null)
			{
				$available_barber[] = array('barber_id'=>$key->user_id);
			}
			else if($key->always == 0)
			{
                $var = json_decode($key->$day);

	            for($i=0;$i<count($var);$i++)
			    { 
			    	$from = date("H:i", strtotime($var[$i]->from));
					$to = date("H:i", strtotime($var[$i]->to));

					if(($booking_time >= $from) && ($booking_time <= $to))
					{	
						$available_barber[] = array('barber_id'=>$key->user_id);
					}
			    }
			}

		} 


		$booking_datetime = $booking_date.' '.$booking_time;

		$barbers_available = array();
		if(!empty($available_barber))
		{
			for($i=0;$i<count($available_barber);$i++)
			{
				$check_avaibility =  $this->db->query("SELECT barber_id FROM barber_customer_booking WHERE barber_id = ".$available_barber[$i]['barber_id']." AND '".$booking_datetime."' BETWEEN start_booking_time AND end_booking_time AND booking_date = '".$booking_date."' AND booking_status IN(0,1)")->row();

				if(empty($check_avaibility))
				{
					$barber_avaibility =  $this->db->query("SELECT dummy_barber_id FROM barber_web_appointment
 					WHERE dummy_barber_id = ".$available_barber[$i]['barber_id']." AND '".$time."' BETWEEN start_booking_time AND end_booking_time AND booking_date = '".$booking_date."' AND admin_status = 1")->row();
 					
 					if(empty($barber_avaibility))
 					{
 						$barbers_available[] =  array('barber_id'=>$available_barber[$i]['barber_id'],'type'=>$type);
 					}
 					else
 					{ 
 						continue;
 					}	
				}
				else
				{
					continue;
				}	
			}	
		}
		else
		{
			$barbers_available[] =  array();
		}

		$data['available_barber'] = $barbers_available;

		$this->load->view('admin/web_appointment_request/available_barber',$data);
	}

	public function assign_barber($booking_id = false,$barber_id = false,$type = false)
	{
		if($type == 'w')
		{
			$getdata = $this->common_model->common_getRow("barber_web_appointment",array('booking_id'=>$booking_id));
		}
		else if($type == 'd')
		{
			$getdata = $this->common_model->common_getRow("barber_customer_booking",array('booking_id'=>$booking_id));

		}	

		$booking_date = $getdata->booking_date;

		$time = $getdata->booking_time;

		$booking_time = date("H:i", strtotime($time));
       
        $find_day = strtotime($booking_date);

		$day = date('D', $find_day);

		$day = strtolower($day);

		$booking_datetime = $booking_date.' '.$booking_time;
		
		/*$avaibility = $this->db->query("SELECT barber_id,always,".$day." FROM barber_availability WHERE barber_id = ".$barber_id."")->row();*/

		$avaibility = $this->db->query("SELECT barber_user.user_id,barber_availability.always,barber_availability.".$day." FROM barber_user LEFT JOIN barber_availability ON barber_user.user_id = barber_availability.barber_id WHERE barber_user.mobile_status = 1 AND barber_user.admin_status = 1 AND barber_user.email_status = 1 AND barber_user.user_type = 2 AND barber_user.user_id = ".$barber_id."")->row();


		if(!empty($avaibility))
		{	$available = false;
            if($avaibility->always == 0)
            {
            	$var = json_decode($avaibility->$day);
            	
	            for($i=0;$i<count($var);$i++)
			    { 
			    	$from = date("H:i", strtotime($var[$i]->from));
					$to = date("H:i", strtotime($var[$i]->to));

					if(($booking_time >= $from) && ($booking_time <= $to))
					{	
						$available = true;
						break;
					}
			    }
			}
            else if($avaibility->always == 1 OR $avaibility->always == null)
            {
            	$available = true;
            }

            	if($available = true)
            	{
            		 $check_avaibility =  $this->db->query("SELECT barber_id,service_id FROM barber_customer_booking WHERE barber_id = ".$barber_id." AND booking_date = '".$booking_date."' AND booking_status IN(0,1) AND '".$booking_datetime."' BETWEEN start_booking_time AND end_booking_time")->row();
            		
		            	if(empty($check_avaibility))
		            	{
		            		$barber_avaibility =  $this->db->query("SELECT dummy_barber_id FROM barber_web_appointment
		 					WHERE dummy_barber_id = ".$barber_id." AND booking_date = '".$booking_date."' AND '".$booking_datetime."' BETWEEN start_booking_time AND end_booking_time  AND admin_status = 1")->row();

		            		if(empty($barber_avaibility))
		            		{

		            			if($type == 'w')
		            			{ 
		            				$startTime = strtotime("-30 minutes", strtotime($booking_datetime));
						    		$starttime1 = date('Y-m-d H:i', $startTime);
							    		
							    	$endTime = strtotime("+60 minutes", strtotime($booking_datetime));
							    	$endTime1 = date('Y-m-d H:i', $endTime);	

		            				$update = $this->common_model->updateData("barber_web_appointment",array('dummy_barber_id'=>$barber_id,'admin_status'=>1,'start_booking_time'=>$starttime1,'end_booking_time'=>$endTime1),array('booking_id'=>$booking_id));
		            			}
		            			else if($type == 'd')	
		            			{ 
		            				$service = json_decode($check_avaibility->service_id);

		            				for($i=0;$i<count($service);$i++)
		    						{
		    		  			  	  $no_of_person[] = $service_id[$i]->no_of_per;
		    						}	

				    				$total_person = array_sum($no_of_person);

						    		$taking_time_by_barber = 30*$total_person + 30;

						    		
						    		$startTime = strtotime("-30 minutes", strtotime($booking_datetime));
						    		$starttime1 = date('Y-m-d H:i', $startTime);
							    		
							    	$endTime = strtotime("+".$taking_time_by_barber." minutes", strtotime($booking_datetime));
							    	$endTime1 = date('Y-m-d H:i', $endTime);


		            				$update = $this->common_model->updateData("barber_customer_booking",array('barber_id'=>$barber_id,'start_booking_time'=>$starttime1,'end_booking_time'=>$endTime1),array('booking_id'=>$booking_id));
		            			}	

		            			if($update != false)
		            			{
		            				$this->session->set_flashdata('success',"Barber Assign Successfully.");
		        					redirect(base_url().'Appointment_request/available_barber/'.$booking_id.'/'.$type);
		            			}	
		            		}
		            		else
		            		{
		            			$this->session->set_flashdata('failed',"Barber Already Occupied, please assign someone else");
		        				redirect(base_url().'Appointment_request/available_barber/'.$booking_id.'/'.$type);exit;
		            		}	
		            	}
		            	else
		            	{
		            		$this->session->set_flashdata('failed',"Barber Already Occupied, please assign someone else");
		        			redirect(base_url().'Appointment_request/available_barber/'.$booking_id.'/'.$type);exit;
		            	}	
            	}
            	else
            	{
            		$this->session->set_flashdata('failed',"Barber not Available yet");
		            redirect(base_url().'Appointment_request/available_barber/'.$booking_id.'/'.$type);exit;
            	}	
		}	
	}

	public function request_to_dummy()
	{
		$data['Appointment_request'] = $this->common_model->getData('barber_customer_booking',array('dummy_barber_id !='=>0),'booking_id','DESC');

		$this->load->view('admin/web_appointment_request/request_to_dummy_barber',$data);
	}

	public function change_status()
	{  
	    $booking_id = $this->input->post('booking_id');
	    $status = $this->input->post('status');
	    $update = $this->common_model->updateData('barber_web_appointment',array('admin_status'=>$status),array('booking_id'=>$booking_id));

	    if($update)
	    {
	       echo "1000"; 
	    }
	}

	public function additional_booking()
	{
		$data['additional_booking'] = $this->common_model->getData('barber_customer_booking',array('booking_type'=>2),'booking_id','DESC');

	    $this->load->view('admin/web_appointment_request/additional_booking',$data);
	}

}
