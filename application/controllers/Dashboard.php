<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}

		$response = $this->common_model->check_auth($this->session->userdata('admin_id'));
	    if($response == 1001)
	    {
	      redirect(base_url().'Logout');
	    }    
		
	}
	
	public function index()
    {		
		$this->load->view('admin/dashboard1');
	}
}
