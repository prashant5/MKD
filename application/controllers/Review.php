<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Review extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}

		$response = $this->common_model->check_auth($this->session->userdata('admin_id'));
	    if($response == 1001)
	    {
	       redirect(base_url().'Logout');
	    }   
    }

    public function rating_review()
    {
    	$data['rating_data'] = $this->common_model->getData('barber_rating_review',array(),'rating_id','DESC');
		
		$this->load->view('admin/review_rating/showreview_rating',$data);
    }

    public function delete_rating()
    {
    	$rating_id = $this->input->post('rating_id');

    	$delete = $this->db->query("DELETE FROM `barber_rating_review` WHERE `rating_id` IN($rating_id)");	
		
		if($delete)
		{
			echo $rating_id; exit;
		}    	
    }

 }   