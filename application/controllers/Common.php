<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Common extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
  }

	public function add_banner()
	{
		if(!empty($this->input->post('fileSubmit')))
		{
			if(isset($_FILES['bannerFiles']['name']) && $_FILES['bannerFiles']['name'] != '')
		    {  
				$date = date("Y-m-d");
				$dataBannerUrl =array();
				$config['upload_path'] = 'uploads/banner/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				
				$count = count($_FILES['bannerFiles']['name']);
				if($count > 0)
				{
					$imagearr = $_FILES['bannerFiles']['name'];
					for ($i=0; $i < $count; $i++) { 
						
						$_FILES['userFile']['name'] = $_FILES['bannerFiles']['name'][$i];
		                $_FILES['userFile']['type'] = $_FILES['bannerFiles']['type'][$i];
		                $_FILES['userFile']['tmp_name'] = $_FILES['bannerFiles']['tmp_name'][$i];
		                $_FILES['userFile']['error'] = $_FILES['bannerFiles']['error'][$i];
		                $_FILES['userFile']['size'] = $_FILES['bannerFiles']['size'][$i];	
						$subFileName = explode('.',$imagearr[$i]);
						$ExtFileName = end($subFileName);
						$config['file_name'] = md5($imagearr[$i].$date).'.'.$ExtFileName;
						$this->upload->initialize($config);
						if($this->upload->do_upload('userFile'))
			            { 
			              	$upload_data = $this->upload->data();
			              	$image = $upload_data['file_name'];
					       	$dataBannerUrl[] = "('','','$date',1,'$image')";
					       	   //ini_set("memory_limit", "-1");
				             	// $config['image_library']  = 'gd2';
				              ////$config['source_image']   = 'uploads/admin_image/'.$image;
				              //$config['create_thumb']   = TRUE;
				              //$config['maintain_ratio'] = TRUE;
				              //$config['width']          = "100";
				              //$config['height']         = "100";
							  //$config['new_image'] = 'uploads/admin_image1/'.$image;
							  
							  //$this->load->library('image_lib');
				              //$this->image_lib->initialize($config);
				              //$newimage =  $this->image_lib->resize();
				              //$this->image_lib->clear();
				              //$x12 = explode('.', $image);//for extension
				              //$thumb_img =  $x12[0].'.'.$x12[1];
			            }
			            else
						{  
							$this->data['err']= $this->upload->display_errors();
						}	
					}
				} 
				if(count($dataBannerUrl)!=$count)
				{
					$this->session->set_flashdata('error_pic', $this->data['err']);	
				}else
				{
					$implod = implode(',',$dataBannerUrl);
					$insert = $this->db->query("INSERT INTO Promotions VALUES ".$implod."");
					$this->session->set_flashdata('success', "Banner successfully uploaded");	
				}
	        }   
		}
		$data1 = array();
		$banner = $this->db->select("BannerUrl,Id")->get_where("Promotions",array('Status'=>1))->result();
		if(!empty($banner))
		{
			$data1['banerimage'] = $banner;
		}
		$this->load->view('admin/common/add_banner',$data1);
	}


	function add_validity_plan()
	{
		if(!empty($this->input->post('formsubmit')))
		{
			$date = date("Y-m-d");
			$minamount = $this->input->post('minamount');
			$maxamount = $this->input->post('maxamount');
			$dayss = $this->input->post('days');
			if(!empty($minamount))
			{
				$ccount = count($minamount);
				if($ccount>0)
				{
					for ($i=0; $i < $ccount; $i++) { 
							
							$dataarr[] = "('','".$minamount[$i]."','".$maxamount[$i]."','".$dayss[$i]."','$date','$date',0)";
						}	
					$implod = implode(',',$dataarr);
					$insert = $this->db->query("INSERT INTO MkdValidityPlan VALUES ".$implod."");
					$this->session->set_flashdata('success', "Validity plan successfully added.");	
					redirect('common/add_validity_plan');
				}
			}
		}
		$this->load->view('admin/common/add_validity_plan');
	}
	
	//Show validity plan list//
	function validity_plan()
	{
		$data['validity'] = $this->common_model->getData("MkdValidityPlan",array('Status'=>1,'ValidityId !='=>3));
		$this->load->view('admin/common/validity_plan',$data);
	}
	function edit_validity_plan()
	{
		$id = $this->input->post('id');
		$data['MinAmount'] = $this->input->post('minval');
		$data['MaxAmount'] = $this->input->post('maxval');
		$data['ValidityDays'] = $this->input->post('day');
		$data['UpdateOn'] = date('Y-m-d');
		$update = $this->common_model->updateData("MkdValidityPlan",$data,array('ValidityId'=>$id));
		if($update!=false)
		{
			echo "1000"; exit;
		}

	}
}