<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		//if(!$userid = $this->session->userdata('admin_id')){
			//redirect(base_url('login'));
		//}

     // $response = $this->common_model->check_auth($this->session->userdata('admin_id'));
     // if($response == 1001)
      //{
       // redirect(base_url().'Logout');
    //  }    
  }

  // public function index()
  // {
  //   $data['products'] = $this->common_model->getData('Product',array(),'id','DESC');

  //   $this->load->view('admin/product/product',$data);
  // }

  public function index()
  { 
  	
     $data['products'] = $this->common_model->getData('Product',array(),'id','DESC');   

     $this->load->view('admin/product/product',$data);
  }


  public function category()
  {
     
      $data['category'] = $this->common_model->getData('ProductCategory',array(),'id','DESC');

      $this->load->view('admin/product/category',$data);
  }

  public function add_category()
  { 
    if($this->input->server('REQUEST_METHOD') === 'POST')
    { 
         $category = array(
          'CategoryName' =>$this->input->post('category_name'),
          'AddedOn' =>date('Y-m-d'),
          'Sequence'=>$this->input->post('sequence')
          );

        $insert_id = $this->common_model->common_insert('ProductCategory',$category);

        if($insert_id)
        {
           $this->session->set_flashdata('success', 'Category successfully Added.');
           redirect(base_url().'product/add_category');
        }
    }

    $this->load->view('admin/product/add_category');
  }

  public function edit_category($category_id = false)
  {
		if($this->input->server('REQUEST_METHOD') === 'POST')
    	{ 
	         $category = array(
	          'CategoryName' =>$this->input->post('category_name'),
	          'AddedOn' =>date('Y-m-d'),
	          'Sequence'=>$this->input->post('sequence')
	          );

	        $update = $this->common_model->updateData("ProductCategory",$category,array('id'=>$category_id));

	        if($update != false)
	        {
	           $this->session->set_flashdata('success', 'Category Updated successfully.');
	           redirect(base_url().'product/category');
	        }
    	}

      $data['edit_category'] = $this->common_model->common_getRow("ProductCategory",array('id'=>$category_id));	

      $this->load->view('admin/product/edit_category',$data);
  }  

  public function edit_subcategory($subcategory_id = false)
  {
		if($this->input->server('REQUEST_METHOD') === 'POST')
    	{ 
	          $subcategory = array(
        		'SubCategoryName' =>$this->input->post('SubCategoryName'),
        		'AddedOn' =>date('Y-m-d H:i:s'),
        		'CategoryId'=>$this->input->post('CategoryId')
            );

	        $update = $this->common_model->updateData("ProductSubCategory",$subcategory,array('id'=>$subcategory_id));

	        if($update != false)
	        {
	        	$category_relation  = array('ProductId'=>$this->input->post('CategoryId'),
       							  	        'AddedOn'=>date('Y-m-d H:i:s')
       								   );

	            $update = $this->common_model->updateData("ProductCategoryRelation",$subcategory,array('SubCategoryId'=>$subcategory_id));

	           $this->session->set_flashdata('success', 'Category edited successfully.');
	           redirect(base_url().'product/subcategory');
	        }
    	}

      $data['edit_category'] = $this->common_model->common_getRow("ProductSubCategory",array('id'=>$subcategory_id));	

      $this->load->view('admin/product/edit_category',$data);
  }  

  public function add_subcategory()
  {
  	if($this->input->server('REQUEST_METHOD') === 'POST')
    {  
        $subcategory = array(
        'SubCategoryName' =>$this->input->post('SubCategoryName'),
        'AddedOn' =>date('Y-m-d'),
        'CategoryId'=>$this->input->post('CategoryId')
        );

        $insert_id = $this->common_model->common_insert('ProductSubCategory',$subcategory);

        if($insert_id)
        {
           $category_relation  = array('ProductId'=>$this->input->post('CategoryId'),
       							  	 'SubCategoryId'=>$insert_id,
       							  	 'AddedOn'=>date('Y-m-d')
       								);

            $insert_id = $this->common_model->common_insert('ProductCategoryRelation',$category_relation);

            $this->session->set_flashdata('success', 'Product successfully Added.');
            redirect(base_url().'product/subcategory');
        }
    }
      $this->load->view('admin/product/addsubcategory');
  }

  public function subcategory()
  {
    $data['category'] = $this->common_model->getData('ProductSubCategory',array(),'id','DESC');

    $this->load->view('admin/product/subcategory',$data);
  }

  public function delete_category($category_id = false,$type = false)
  {
  	if($type == 1)
  	{
  		$delete = $this->common_model->deleteData('ProductCategory',array('id'=>$category_id));

  		if($delete)
  		{
  			$delete = $this->common_model->deleteData('ProductOptionRelation',array('id'=>$category_id));

  			echo "1000"; exit;
  		}	
  	}	
    else if($type == 2)
    {
    	$delete = $this->common_model->deleteData('ProductSubCategory',array('id'=>$category_id));

    	if($delete)
    	{
  			$delete = $this->common_model->deleteData('ProductOptionRelation',array('id'=>$category_id));

    		echo "1000"; exit;
    	}	
    }	
  }

  public function add_product()
  {
    if($this->input->server('REQUEST_METHOD') === 'POST')
    {
    	 // if(isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '')
      //   { 
      //       $date = date("ymdhis");
      //       $config['upload_path'] = 'uploads/product_image/';
      //       $config['allowed_types'] = 'jpg|png|jpeg';
      //       $subFileName = explode('.',$_FILES['logo']['name']);
      //       $ExtFileName = end($subFileName);
      //       $config['file_name'] = md5($date.$_FILES['logo']['name']).'.'.$ExtFileName;
                      
      //       $this->load->library('upload', $config);
      //       $this->upload->initialize($config);
          
      //       if($this->upload->do_upload('logo'))
      //       { 
      //         $upload_data = $this->upload->data();
      //         $image = $upload_data['file_name'];

      //         ini_set("memory_limit", "-1");
                
      //         $config['image_library']  = 'gd2';
      //         $config['source_image']   = 'uploads/product_image/'.$image;
      //         $config['create_thumb']   = TRUE;
      //         $config['maintain_ratio'] = TRUE;
      //         $config['max_width']      = "80";
      //         $config['max_height']     = "80";
      //         $config['new_image'] = 'uploads/product_image/'.$image;

      //         $this->load->library('image_lib', $config);

      //         $this->image_lib->initialize($config);

      //         $newimage =  $this->image_lib->resize();
      //         $this->image_lib->clear();
      //         $x12 = explode('.', $image);
      //         $logo =  $x12[0].'_thumb.'.$x12[1];
      //       }
      //       else
      //       {   
      //          $this->data['err']= $this->upload->display_errors();
      //          $this->session->set_flashdata('error_pic', 'Please Select png,jpg,jpeg File Type.');
      //          redirect('admin_user/add_admin');
      //       }
      //   }
      //   else
      //   { 
      //       $logo = '';
      //   }

      //   if(isset($_FILES['banner']['name']) && $_FILES['banner']['name'] != '')
      //   { 
      //       $date = date("ymdhis");
      //       $config['upload_path'] = 'uploads/banner_image/';
      //       $config['allowed_types'] = 'jpg|png|jpeg';
      //       $subFileName = explode('.',$_FILES['banner']['name']);
      //       $ExtFileName = end($subFileName);
      //       $config['file_name'] = md5($date.$_FILES['banner']['name']).'.'.$ExtFileName;
                      
      //       $this->load->library('upload', $config);
      //       $this->upload->initialize($config);
          
      //       if($this->upload->do_upload('banner'))
      //       { 
      //         $upload_data = $this->upload->data();
      //         $image = $upload_data['file_name'];

      //         ini_set("memory_limit", "-1");
                
      //         $config['image_library']  = 'gd2';
      //         $config['source_image']   = 'uploads/banner_image/'.$image;
      //         $config['create_thumb']   = TRUE;
      //         $config['maintain_ratio'] = TRUE;
      //         $config['max_width']      = "400";
      //         $config['max_height']     = "400";
      //         $config['new_image'] = 'uploads/banner_image/'.$image;

      //         $this->load->library('image_lib', $config);

      //         $this->image_lib->initialize($config);

      //         $newimage =  $this->image_lib->resize();
      //         $this->image_lib->clear();
      //         $x12 = explode('.', $image);
      //         $banner_image =  $x12[0].'_thumb.'.$x12[1];
      //       }
      //       else
      //       {   
      //          $this->data['err']= $this->upload->display_errors();
      //          $this->session->set_flashdata('banner_error', 'Please Select png,jpg,jpeg File Type.');
      //          redirect(base_url().'product/add_product');
      //       }
      //   }
      //   else
      //   { 
      //       $banner_image = '';
      //   }
        $date = date('Y-m-d H:i:s');
        $proname = $this->input->post('product_name');
        $desc =$this->input->post('description');
        $price = $this->input->post('price');
        $category = $this->input->post('category');
        for ($i=0; $i < count($proname); $i++) { 
          $product[] = "('',$category,'".$proname[$i]."','','',0,'','','','',1,'$date','','".$price[$i]."','','',1,'".$desc[$i]."')";
        }
        $empprod = implode(',',$product);
        $insert_id = $this->db->query("INSERT INTO Product VALUES $empprod");
        if($insert_id)
        {
         // echo $this->input->post('category');
         // echo $insert_id;exit;
           $this->session->set_flashdata('success', 'Product added successfully.');
           redirect(base_url().'product/add_product');
        }
    }
    $selcate = $this->common_model->getData("ProductCategory",array(),'CategoryName','ASC');  
    $data['catedata'] = $selcate;
    $this->load->view('admin/product/add_product',$data);
  }

  // function get_detail()
  // {
  //  	$this->session->unset_userdata('cateid');
  // 	$id = $this->input->post('cateid'); 
  //  	$this->session->set_flashdata('cateid',$id);
  // }
  public function edit()
  {
     if($this->input->server('REQUEST_METHOD') === 'POST')
     {
     	  // if(isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '')
        // { 
        //     $date = date("ymdhis");
        //     $config['upload_path'] = 'uploads/product_image/';
        //     $config['allowed_types'] = 'jpg|png|jpeg';
        //     $subFileName = explode('.',$_FILES['logo']['name']);
        //     $ExtFileName = end($subFileName);
        //     $config['file_name'] = md5($date.$_FILES['logo']['name']).'.'.$ExtFileName;
                      
        //     $this->load->library('upload', $config);
        //     $this->upload->initialize($config);
          
        //     if($this->upload->do_upload('logo'))
        //     { 
        //       $upload_data = $this->upload->data();
        //       $image = $upload_data['file_name'];

        //       ini_set("memory_limit", "-1");
                
        //       $config['image_library']  = 'gd2';
        //       $config['source_image']   = 'uploads/product_image/'.$image;
        //       $config['create_thumb']   = TRUE;
        //       $config['maintain_ratio'] = TRUE;
        //       $config['max_width']      = "80";
        //       $config['max_height']     = "80";
        //       $config['new_image'] = 'uploads/product_image/'.$image;

        //       $this->load->library('image_lib', $config);

        //       $this->image_lib->initialize($config);

        //       $newimage =  $this->image_lib->resize();
        //       $this->image_lib->clear();
        //       $x12 = explode('.', $image);
        //       $logo =  $x12[0].'_thumb.'.$x12[1];
        //     }
        //     else
        //     {   
        //        $this->data['err']= $this->upload->display_errors();
        //        $this->session->set_flashdata('error_pic', 'Please Select png,jpg,jpeg File Type.');
        //        redirect(base_url().'product/edit/'.$product_id);
        //     }
        // }
        // else
        // { 
        //     $logo = '';
        // }
        $name = $this->input->post('name');
        $price = $this->input->post('price');
        $cate = $this->input->post('cate');
        $desc = $this->input->post('desc');
        $proid = $this->input->post('proid');
        $product = array(
          'Name' =>$name,
          'CategoryId' =>$cate,
          'Amount'=>$price,
          'Description'=>$desc,
          'AddedOn' =>date('Y-m-d H:i:s')
        );
          $update = $this->common_model->updateData("Product",$product,array('Id'=>$proid));
          //echo $this->db->last_query();
          if($update)
          {
            $this->session->userdata('cid',$cate);
            
            redirect('product/'.$cate);
            //$this->session->set_flashdata('success', 'Product Updated Successfully.');
          }else
          {
            echo 400; exit;
          }
          
     } 

     $data['edit_product'] = $this->common_model->common_getRow("Product",array('id'=>$product_id));

     $this->load->view('admin/product/edit_product',$data);   
  }

  public function add_product_image()
  {
     if($this->input->server('REQUEST_METHOD') === 'POST')
     {
     	$product_id = $this->input->post('product_id');
     	if(isset($_FILES['product_image']['name']) && $_FILES['logo']['name'] != '')
        { 
            $date = date("ymdhis");
            $config['upload_path'] = 'uploads/product_image/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $subFileName = explode('.',$_FILES['product_image']['name']);
            $ExtFileName = end($subFileName);
            $config['file_name'] = md5($date.$_FILES['product_image']['name']).'.'.$ExtFileName;
                      
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
          
            if($this->upload->do_upload('product_image'))
            { 
              $upload_data = $this->upload->data();
              $image = $upload_data['file_name'];

              ini_set("memory_limit", "-1");
                
              $config['image_library']  = 'gd2';
              $config['source_image']   = 'uploads/product_image/'.$image;
              $config['create_thumb']   = TRUE;
              $config['maintain_ratio'] = TRUE;
              $config['max_width']      = "80";
              $config['max_height']     = "80";
              $config['new_image'] = 'uploads/product_image/'.$image;

              $this->load->library('image_lib', $config);

              $this->image_lib->initialize($config);

              $newimage =  $this->image_lib->resize();
              $this->image_lib->clear();
              $x12 = explode('.', $image);
              $product_image =  $x12[0].'_thumb.'.$x12[1];
            }
            else
            {   
               $this->data['err']= $this->upload->display_errors();
               $this->session->set_flashdata('error_pic', 'Please Select png,jpg,jpeg File Type.');
               redirect(base_url().'product/add_product_image');
            }
        }
        else
        { 
            $product_image = '';
        }	


        $product = array('ProductId'=>$product_id,'AddedOn'=>datatime,'ContentType'=>$product_image);

        $insert_id = $this->common_model->common_insert('ProductImages',$product);

        if($insert)
        {
        	$this->session->set_flashdata('success', 'Add Product image Successfully.');
            redirect(base_url().'product/productlist');
        }	

     }		   		

  }

  function get_product_by_category()
  {
      $id = $this->input->post('id');
      $where = '';
      if($id!='')
      {
        $where = array('CategoryId'=>$id);
      }
      $data = $this->common_model->getData('Product',$where, 'id','DESC'); 
      if(!empty($data))
      {
         foreach ($data as $key) {
           $arr[] = array(
                          'Name'=>$key->Name,
                          'Amount'=>$key->Amount,
                          'Description'=>$key->Description,
                          'Id'=>$key->Id,
                          'CategoryId'=>$id
                        );
         }
         print_r(json_encode($arr));
      }
  }


  public function remove_product_image($image_id =false)
  {
  	  $delete = $this->common_model->deleteData('ProductImages',array('id'=>$image_id));

  	  if($delete)
  	  {
  	  	 echo '1000';exit;
  	  }	
  }

}