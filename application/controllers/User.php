<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
		
		$response = $this->common_model->check_auth($this->session->userdata('admin_id'));
		
		if($response == '1001')
	    {
	    	redirect(base_url().'Logout');
	    } 		
		
	}
	
	public function verified()
	{   
		$data['user_data'] = $this->common_model->getData('user',array('ismobileverified'=>'1','status'=>'1'),'id','DESC');
		
		$this->load->view('admin/user/activeuser',$data);
	}

	public function unverified()
	{
		$data['user_data'] = $this->common_model->getData('user',array('ismobileverified'=>'0','status'=>'1'),'id','DESC');
       
		$this->load->view('admin/user/deactivate_user',$data);
	}

	public function block_user()
	{
		$data['user_data'] = $this->common_model->getData('user',array('status'=>'0'),'id','DESC');
       
		$this->load->view('admin/user/block_user',$data);
	}
    //Deactivate action for Active user 
	public function block()
	{
		$user_id = $this->input->post('user_id');
        $delete = $this->db->query("UPDATE `user` SET `status` = 0 WHERE `id` IN ($user_id)");
        if($delete)
        {
        	echo $user_id;exit;
        }	
	}
    //Unblock action for block user
	public function unblock()
	{
        $user_id = $this->input->post('user_id');
        $delete = $this->db->query("UPDATE `user` SET `status` = 1 WHERE `id` IN($user_id)");

        if($delete)
        {
        	echo $user_id;exit;
        }
	}

	public function delete()
	{
	   $user_id = $this->input->post('user_id');

	   $delete = $this->db->query("DELETE FROM `qalame_user` WHERE `user_id` IN($user_id)");
	   $delete = $this->db->query("DELETE FROM `qalame_follow_user` WHERE `following_id` IN($user_id)");

	   $delete = $this->db->query("DELETE FROM `qalame_follow_user` WHERE `follower_id` IN($user_id)");

	   $delete = $this->db->query("DELETE FROM `qalame_user_post` WHERE `user_id` IN($user_id)");

	   echo $user_id;exit;

	}
}
