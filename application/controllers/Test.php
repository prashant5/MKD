<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Test extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		$this->load->library('curl');	
	}
	
	public function sendmail()
	{
		$config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://smtp.gmail.com',
	    'smtp_port' => 465,
	    'smtp_user' => '***',
	    'smtp_pass' => '***',
	    'mailtype'  => 'html', 
	    'charset'   => 'iso-8859-1'
	 );

	$this->load->library('email', $config);
	$this->email->set_newline("\r\n");

	$this->email->from('jaipal.solanki@ebabu.co', 'jaipal');
    $this->email->to('ankitthakur.nmims@gmail.com'); 

    $this->email->subject('Email to sumit');
    $this->email->message('Testing the email class.');  

    if($this->email->send())
    {
       echo 'success';
    }
    else
    {
       echo $this->email->print_debugger();
    }	

	}

	public function captcha()
	{   
		if(isset($_POST['submit']))
		{
			$recaptchaResponse = $this->input->post('g-recaptcha-response');

			$secret = '6LekRz4UAAAAAEFxtjzixGtquA9bgi6MfTiqpguJ';

			$post_data = "secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$_SERVER['REMOTE_ADDR'] ;

			$ch = curl_init();  
			curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=utf-8', 'Content-Length: ' . strlen($post_data)));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); 
			$googresp = curl_exec($ch);       
			$decgoogresp = json_decode($googresp);
			
			curl_close($ch);

			if($decgoogresp->success == true)
			{
			  die('success');
			}
			else
			{
			  die('Please verify that you are a Human');
			}

		}		
		
		$this->load->view('admin/captcha');
	}
	
}
