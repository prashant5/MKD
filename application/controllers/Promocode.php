 <?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Promocode extends CI_Controller {
public function __construct()
{
	parent::__construct();
	if(!$userid = $this->session->userdata('admin_id')){
		redirect(base_url('login'));
	}

  $response = $this->common_model->check_auth($this->session->userdata('admin_id'));
  
  if($response == 1001)
  {
      redirect(base_url().'Logout');
  }    

	date_default_timezone_set('Asia/Kolkata');
	$militime =round(microtime(true) * 1000);
	$datetime =date('Y-m-d h:i:s');
	define('militime', $militime);
	define('datetime', $datetime);

}

public function index()
{
  $data['promocode'] = $this->common_model->getData('barber_promocode',array(),'promocode_id','DESC');

  $this->load->view('admin/promocode/show_promocode',$data);
}

public function check_promocode()
{
    $promocode = $this->input->post('promo_code'); 
    $query=$this->db->query("SELECT `promocode` FROM `barber_promocode` WHERE `promocode` = '".$promocode."'");
    if ($query->num_rows() > 0){
      echo "10000";
    }
}

public function add()
{
  if($this->input->server('REQUEST_METHOD') === 'POST')
  {
      $data = array(
          'title' =>$this->input->post('title'),
          'promocode' =>$this->input->post('promocode'),
          'start_date' =>$this->input->post('start_date'),
          'end_date' =>$this->input->post('end_date'),
          'description' =>$this->input->post('description'),
          'discount' =>$this->input->post('discount'),
          'max_discount' =>$this->input->post('max_discount'),
          'admin_status'=>0,
          'create_at'=>datetime,
          'update_at'=>datetime
          );

     $insert_data = $this->common_model->common_insert('barber_promocode',$data);

     if(!empty($insert_data))
     {
        $this->session->set_flashdata('success',"Coupon Generated Successfully.");
        redirect(base_url().'promocode');
     }
     else
     {
        $this->session->set_flashdata('failed',"Something Went Wrong please try again later.");
        redirect(base_url().'promocode/add');
     }  
  }

  $this->load->view('admin/promocode/add_promocode');
}

public function change_status()
{  
    $promocode_id = $this->input->post('id');
    $status = $this->input->post('status');
    $update = $this->common_model->updateData('barber_promocode',array('admin_status'=>$status),array('promocode_id'=>$promocode_id));

    if($update)
    {
       echo "1000"; 
    }
}

public function version()
{
	 $data['androide_version'] = $this->common_model->common_getRow('app_version',array('version_id'=>1));

	 $data['ios_version'] = $this->common_model->common_getRow('app_version',array('version_id'=>2));

     $this->load->view('admin/content/version',$data);
}


public function version_update()
{
    if(isset($_POST['submit1']))
    {
            $min_android_version = $this->input->post('min_android_version');
            $current_android_version = $this->input->post('current_android_version');
            
           $version_update = $this->common_model->updateData('app_version',array('min_version'=> $min_android_version,'current_version'=>$current_android_version),array('version_id'=>'1'));

        if($version_update)
        {
             
            $this->session->set_flashdata('success1',"Version updated Successfully");

            redirect('setting/version');
        } 
        else
        {
          $this->session->set_flashdata('failed',"Version is Not Updated");

          redirect('setting/version');
        } 
          
    }
    else if(isset($_POST['submit2']))
    {
        $min_ios_version = $this->input->post('min_ios_version');
        $current_ios_version = $this->input->post('current_ios_version');
            
        $version_update = $this->common_model->updateData('app_version',array('min_version'=> $min_ios_version,'current_version'=>$current_ios_version),array('version_id'=>'2'));

        if($version_update)
        {
          $this->session->set_flashdata('success2',"Version updated Successfully");

          redirect('setting/version');
        } 
        else
        {
          $this->session->set_flashdata('failed',"Version is Not Updated");

          redirect('setting/version');
        }   
      
    }  
      redirect('setting/version');
  }
}
