 <?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller {
public function __construct()
{
	parent::__construct();
	if(!$userid = $this->session->userdata('admin_id')){
		redirect(base_url('login'));
	}

	$response = $this->common_model->check_auth($this->session->userdata('admin_id'));
    if($response == 1001)
    {
      redirect(base_url().'Logout');
    }
	
	date_default_timezone_set('Asia/Kolkata');
	$militime =round(microtime(true) * 1000);
	$datetime =date('Y-m-d h:i:s');
	define('militime', $militime);
	define('datetime', $datetime);

}

public function index()
{
  $data['contactus'] = $this->common_model->getData('barber_contactus',array(),'id','DESC');

  $this->load->view('admin/contactus/detail',$data);
}

public function delete($id = false)
{
  $delete = $this->common_model->deleteData('barber_contactus',array('id'=>$id));
  
  if($delete)
  {
    echo "1000"; exit;
  }   
}

}
