<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Barber_api extends MY_Controller {
function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
	}

	function registration()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	if($json_array->user_email!='' && $json_array->user_password!='' && $json_array->user_mobile_num!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('barber_user',array('user_email'=>$json_array->user_email,'user_type'=>2));
	    		if(!empty($seleuser))
	    		{
    				if($seleuser->mobile_status ==1)
	    			{	
						if($seleuser->email_status ==1)
		    			{	
							if($seleuser->admin_status==1)
    						{
								$final_output['status'] = 'failed';
		    					$final_output['message'] = 'Email address and mobile number already registered.';
			    			}else
				    		{
				    			$final_output['status'] = '1000';
			    				$final_output['message'] = admin_status;
				    		}
		    			}else
		    			{
		    				$final_output['status'] = 'failed';
	    					$final_output['message'] = "Email address already registered.";
		    			}
	    			}else
	    			{
	    				$selectmob = $this->common_model->common_getRow("barber_user",array('user_mobile_num'=>$json_array->user_mobile_num,'mobile_status'=>1,'user_id !='=>$seleuser->user_id));
	    				if(!empty($selectmob))
	    				{
							$final_output['status'] = 'failed';
	    					$final_output['message'] = 'Mobile number has been already registered! please try another mobile number.';
	                    }else
	                    {
							//$otp = $this->common_model->randomuniqueCode();
	                    	$otp = '1234';
	                    	$update = $this->common_model->updateData("barber_user",array('user_mobile_num'=>$json_array->user_mobile_num,'mobile_code'=>hash('sha256', $otp),'update_date'=>datetime),array('user_id'=>$seleuser->user_id));
	                   		$final_output['status'] = 'success';
	    					$final_output['message'] = 'Successfully registered.';
	    					$final_output['user_id'] = $seleuser->user_id; 
	                    }
	    			}
		    	}else
	    		{
    				$v_code = $this->common_model->randomuniqueCode();
    				$v_Code = $this->common_model->encryptor_ym('encrypt',$v_code);
	    			//$otp = $this->common_model->randomuniqueCode();
		            $otp = '1234';
	    			$selectmob = $this->common_model->common_getRow("barber_user",array('user_mobile_num'=>$json_array->user_mobile_num,'mobile_status'=>1));
    				if(!empty($selectmob))
    				{
						$final_output['status'] = 'failed';
    					$final_output['message'] = 'Mobile number has been already registered! please try another mobile number.';
                    }else
                    {
						$insert = $this->common_model->common_insert("barber_user",array('user_name'=>$json_array->user_name,'user_email'=>$json_array->user_email,'email_status'=>0,'user_mobile_num'=>$json_array->user_mobile_num,'user_type'=>2,'user_location'=>$json_array->user_location,'user_lat'=>$json_array->user_lat,'user_lng'=>$json_array->user_lng,'email_code'=>$v_Code,'mobile_code'=>hash('sha256', $otp),'user_password'=>sha1($json_array->user_password)
						,'create_date'=>datetime,'user_device_id'=>$json_array->user_device_id,'user_device_type'=>$json_array->user_device_type,'user_device_token'=>$json_array->user_device_token));
		    			if(!empty($insert) && $insert != false)
		    			{
							//Send verification mail
		    				$url = "<a href=".base_url()."barber_api/email_verification/".$this->common_model->encryptor_ym('encrypt',$insert)."/".$v_Code." target='_blank' title='' style='background: #d3a117; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";
		    				$data = array('email'=>$json_array->user_email,
				                          'name'=>$json_array->user_name,
				                           'url'=>$url
				                          );
	                        $message = $this->load->view('email_template/template_verify.php',$data,TRUE);


	                        $config = Array(
				                  'protocol' => 'smtp',
				                  'smtp_host' => 'mail.zohomail.com',
				                  'smtp_port' => 465,
				                  'smtp_user' => 'Barbers@Kwikuts.co.uk',
				                  'smtp_pass' => 'Kwikuts2017',
				                  'mailtype'  => 'html', 
				                  'charset'   => 'utf-8',
				                  'wordwrap'  => TRUE
             					 );

					            $this->load->library('email', $config);

					            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
					            $this->email->set_header('Content-type', 'text/html');

					            $this->email->set_newline("\r\n");
		          
								$this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
								$this->email->to($json_array->user_email); 
								$this->email->subject('Forget Password..!');

								$this->email->message($message);

								$this->email->send();
							
							// $subject = 'KwiKuts App: Verification Link';
					  		//$email_from = 'no-reply@kwikuts.com';
					 		//$email = $json_array->user_email;
					  		//$headers  = 'MIME-Version: 1.0' . "\r\n";
					  		//$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					  		//$headers .= 'From: '.$email_from. '\r\n';            // Mail it
					  		//@mail($email, $subject,$message,$headers);	
	                       
	                       	$final_output['status'] = 'success';
		    				$final_output['message'] = 'Successfully registered.';
		    				$final_output['user_id'] = $insert; 
	        			}else
		    			{
		    				$final_output['status'] = 'failed'; 
						 	$final_output['message'] = some_error;
		    			}
		    		}
		    	}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = request_params;
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end signup (Y)
	function email_verification()
	{
		$id = $this->uri->segment(3);
		$code = $this->uri->segment(4);
		$id =$this->common_model->encryptor_ym('decrypt',$id);
		
		$select = $this->db->get_where("barber_user ",array('user_id'=>$id,'user_type'=>2))->row();
		if(!empty($select))
		{
				$data = array('email'=>$select->user_email
                             );
          	if($select->email_code == $code && !empty($select->email_code))
			{
				//$this->db->cache_delete('user', 'verified'); //delete cache file
				$update = $this->db->update("barber_user",array('email_code'=>'','email_status'=>1),array('user_id'=>$id));	
				
				$message = $this->load->view('email_template/successfully_verify.php',$data,TRUE);
				print_r($message);exit;
			}else
			{
				$message = $this->load->view('email_template/verification_failed.php',$data,TRUE);
				print_r($message);exit;
			}
		}else
		{
			$message = $this->load->view('email_template/verification_failed.php','',TRUE);
			print_r($message);exit;
		}
	}
	//end email verification (Y) 30-10-17

	function otp_verification()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array->mobile_otp) && !empty($json_array->user_id))
	    {
	    	$checkotp = $this->common_model->common_getRow("barber_user",array('user_id'=>$json_array->user_id,'user_type'=>2));
	    	if(!empty($checkotp))
	    	{	
				if(hash('sha256', $json_array->mobile_otp) == $checkotp->mobile_code)
				{
					$updateotp = $this->common_model->updateData("barber_user",array('mobile_code'=>'','mobile_status'=>1,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$json_array->user_id));
					if($updateotp!=false)
					{
						// $object = array(
						// 	'user_id'=>$checkotp->user_id,
						// );
						$final_output['status'] = 'success';
						$final_output['message'] = 'OTP has been verified successfully.';
						$final_output['user_id'] = $checkotp->user_id;
					}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = some_error;
					}
				}else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = "Otp does not match.";
				}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = 'Account does not exists.';		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = some_error;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	// end otp verification (Y) 30-10-17

	function Resend_otp()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array->user_mobile_num))
	    {
	    	$checkotp = $this->common_model->common_getRow("barber_user",array('user_mobile_num'=>$json_array->user_mobile_num,'user_type'=>2));
	    	if(!empty($checkotp))
	    	{	
					//$otp = $this->common_model->randomuniqueCode();
		            $otp = '1234';
					$updateotp = $this->common_model->updateData("barber_user",array('mobile_code'=>hash('sha256', $otp),'mobile_status'=>0,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$checkotp->user_id));
					if($updateotp!=false)
					{
						$final_output['status'] = 'success';
						$final_output['message'] = 'OTP sent successfully.';
						$final_output['user_id'] = $checkotp->user_id;
					}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = some_error;
					}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = 'Account does not exists';		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	// end otp verification (Y) 30-10-17

	function login()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    { 
	    	if($json_array->user_email!='' && $json_array->user_password!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('barber_user',array('user_email'=>$json_array->user_email,'user_password'=>sha1($json_array->user_password),'user_type'=>2));
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
		    			if($seleuser->mobile_status==1)
		    			{	
							if($seleuser->email_status==1)
							{
								$token = bin2hex(openssl_random_pseudo_bytes(16));
        						$token = $token.militime;
	    						$update = $this->common_model->updateData('barber_user',array('user_device_type'=>$json_array->user_device_type,'user_device_id'=>$json_array->user_device_id,'user_device_token'=>$json_array->user_device_token,'user_token'=>$token,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$seleuser->user_id));
	    						
	    						$updatedevicetoken = $this->common_model->updateData('barber_user',array('user_token'=>'','user_device_token'=>''),array('user_id !='=>$seleuser->user_id,'user_device_id'=>$json_array->user_device_id,'user_type'=>$seleuser->user_type));
	    						
	    						$image = '';
								if(!empty($seleuser->user_image))
								{
									if (filter_var($seleuser->user_image, FILTER_VALIDATE_URL)) {
										$image = $seleuser->user_image;
									}else
									{
										$image = base_url().'uploads/barber_image/'.$seleuser->user_image;
									}
								}
								$Availability = $avail = (object)array(); 
								$selepas = $this->common_model->common_getRow('barber_availability',array('barber_id'=>$seleuser->user_id));
								if(!empty($selepas))
								{
									if(!empty($selepas->availability))
									{
										$avail = json_decode($selepas->availability);
									}
									$Availability = $avail;

									/*$timingss[] = array(
														'day'=>0,
														'timearr'=>json_decode($selepas->mon),
													);
									$timingss[] = array(
														'day'=>1,
														'timearr'=>json_decode($selepas->tue),
													);
									$timingss[] = array(
														'day'=>2,
														'timearr'=>json_decode($selepas->wed),
													);
									$timingss[] = array(
														'day'=>3,
														'timearr'=>json_decode($selepas->thu),
													);
									$timingss[] = array(
														'day'=>4,
														'timearr'=>json_decode($selepas->fri),
													);
									$timingss[] = array(
														'day'=>5,
														'timearr'=>json_decode($selepas->sat),
													);
									$timingss[] = array(
														'day'=>6,
														'timearr'=>json_decode($selepas->sun),
													);
									$Availability = array(
													'always'=>$selepas->always,
													'timings'=>$timingss
														);*/
								}

								$galimag = array(); 
								$gallarypic = $this->common_model->getDataField('gallary_pic','barber_gallary',array('user_id'=>$seleuser->user_id),'gallary_id','ASC');

								if(!empty($gallarypic))
								{
									foreach ($gallarypic as $gallarypicval) {
										
										$galimag[] = base_url().'uploads/gallary_image/'.$gallarypicval->gallary_pic;
									}
								}

								$object = array(
									'user_id'=>$seleuser->user_id,
									'user_name'=>$seleuser->user_name,
									'user_image'=>$image,
									'user_email'=>$seleuser->user_email,
									'user_mobile_num'=>$seleuser->user_mobile_num,
									'user_location'=>$seleuser->user_location,
									'about_me'=>$seleuser->about_me,
									'user_lat'=>$seleuser->user_lat,
									'user_lng'=>$seleuser->user_lng,
									'user_device_type'=>$json_array->user_device_type,
									'user_device_id'=>$json_array->user_device_id,
									'user_device_token'=>$json_array->user_device_token,
									'user_token'=>$token,
									'availability'=>$Availability,
									'gallary_pic'=>$galimag
									);
								$final_output['status'] = 'success';
	    						$final_output['message'] = 'Successfully Login';
	    						$final_output['data'] = $object;
							}else
							{
								$final_output['status'] = 'failed';
								$final_output['message'] = 'Please check your email if you have already verification email or click on resend button for email verification link.';
							}
						}else
		    			{
							$otp = '1234';
							$update = $this->common_model->updateData('barber_user',array('mobile_code'=>hash('sha256', $otp),'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$seleuser->user_id));

							$final_output['status'] = 'failed';
	    					$final_output['message'] = "Please verify your mobile";
		    			}
	    			}else
	    			{
						$final_output['status'] = '1000';
	    				$final_output['message'] = admin_status;
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = 'Invalid login credentials';
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = request_params;
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end login (Y)
	
	function resend_verification_link()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
    		$seleuser = $this->common_model->common_getRow('barber_user',array('user_email'=>$json_array->user_email,'user_type'=>2));


    		if(!empty($seleuser))
    		{
    				$v_code = $this->common_model->randomuniqueCode();
    				$v_Code = $this->common_model->encryptor_ym('encrypt',$v_code);
    				$update = $this->common_model->updateData("barber_user",array('email_code'=>$v_Code),array('user_email'=>$json_array->user_email));
    				$subject = 'KwiKuts App: Verification Link';
		    		$url = "<a href=".base_url()."barber_api/email_verification/".$this->common_model->encryptor_ym('encrypt',$seleuser->user_id)."/".$v_Code." target='_blank' title='' style='background: #d3a117; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";
                	$data = array('email'=>$json_array->user_email,
		                          'name'=>$seleuser->user_name,
		                           'url'=>$url
		                          );

                    $message = $this->load->view('email_template/template_verify.php',$data,TRUE);
    				// $email_from = 'no-reply@kwikuts.com';
	       //        	$email = $json_array->user_email;
	       //        	$headers  = 'MIME-Version: 1.0' . "\r\n";
	       //          $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
	       //          $headers .= 'From: '.$email_from. '\r\n';            // Mail it
	       //     		@mail($email, $subject,$message,$headers);	

	                    $config = Array(
				                  'protocol' => 'smtp',
				                  'smtp_host' => 'mail.zohomail.com',
				                  'smtp_port' => 465,
				                  'smtp_user' => 'Barbers@Kwikuts.co.uk',
				                  'smtp_pass' => 'Kwikuts2017',
				                  'mailtype'  => 'html', 
				                  'charset'   => 'utf-8',
				                  'wordwrap'  => TRUE
             					 );

					            $this->load->library('email', $config);

					            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
					            $this->email->set_header('Content-type', 'text/html');

					            $this->email->set_newline("\r\n");
		          
								$this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
								$this->email->to($json_array->user_email); 
								$this->email->subject('Resend Mail Verification..!');

								$this->email->message($message);

								$this->email->send();
								
								$final_output['status'] = 'success';
    							$final_output['message'] = "Verification link has been sent to your registered email address.";
								

 			}else
    		{
    			$final_output['status'] = 'failed';
    			$final_output['message'] = 'Account does not exists';
	  		}
		}else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }		
		header("content-type: application/json");
	    echo json_encode($final_output);	exit;
	}
	//end resend verificatin mail (Y)

	function update_profile()
	{ 
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			if($aa['data']->admin_status==1)
			{
				$data['about_me'] = $this->input->post('about_me');
				$data['user_location'] = $this->input->post('user_location');
				$data['user_name'] = $this->input->post('user_name');
				$data['user_lat'] = $this->input->post('user_lat');
				$data['user_lng'] = $this->input->post('user_lng');
				$image = ''; $user_image = $galryimage = array();


				
				if(!empty($_FILES['user_image']['name']) && isset($_FILES['user_image']))
				{
					$config = array();
					$config['upload_path']   = './uploads/barber_image/';
					$config['allowed_types'] = 'jpg|jpeg|png';

					$subFileName = explode('.',$_FILES['user_image']['name']);
					$ExtFileName = end($subFileName);
				    $images = militime.$_FILES['user_image']['name'];
					//$images = md5(militime.$images).'.png';
					move_uploaded_file($_FILES["user_image"]["tmp_name"],"uploads/barber_image/".$images);
					$data['user_image'] = $images;
					$image = base_url().'uploads/barber_image/'.$images;
				}else
				{
					$getdata = $this->common_model->common_getRow("barber_user",array('user_id'=>$aa['data']->user_id));	
					if($getdata->user_image!=''){
						if (filter_var($getdata->user_image, FILTER_VALIDATE_URL)) {
							$image = $getdata->user_image;
						}else
						{
							$image = base_url().'uploads/barber_image/'.$getdata->user_image;
						}
					}else{
					 	$image = '';
					}
				}


				if(isset($_FILES['gallery_pic']['name']) && $_FILES['gallery_pic']['name'] != '')
				{ 
					$file_count=count($_FILES['gallery_pic']['name']);

					for ($i=0; $i < $file_count; $i++) 
					{ 	
						$_FILES['file_url']['name']= $_FILES['gallery_pic']['name'][$i];
						$_FILES['file_url']['type']= $_FILES['gallery_pic']['type'][$i];
						$_FILES['file_url']['tmp_name']= $_FILES['gallery_pic']['tmp_name'][$i];
						$_FILES['file_url']['error']= $_FILES['gallery_pic']['error'][$i];
						$_FILES['file_url']['size']= $_FILES['gallery_pic']['size'][$i];  
	
						$config = array();
						$config['upload_path']   = 'uploads/gallary_image/';
						$config['allowed_types'] = 'jpg|jpeg|png';
	
						$subFileName = explode('.',$_FILES['file_url']['name']);
						$ExtFileName = end($subFileName);

	                    //$config['file_name'] = md5($date.$_FILES['document']['name']).'.'.$ExtFileName;
						$images = md5(militime.$_FILES['file_url']['name']).'.jpeg';

						move_uploaded_file($_FILES["file_url"]["tmp_name"],"uploads/gallary_image/".$images);
						$user_images[] = '("","'.$aa['data']->user_id.'","'.$images.'","'.datetime.'")';
						$galryimage[] = base_url().'uploads/gallary_image/'.$images;
					}
				}

				//print_r($user_images);exit;

				$data['update_date'] = date('Y-m-d H:i:s');
				$insertt = $this->common_model->updateData("barber_user",$data,array('user_id'=>$aa['data']->user_id));
				if($this->db->affected_rows())
				{
					if(!empty($user_images))
					{
						$impgalry = implode(',',$user_images);
						
						$delete = $this->common_model->deleteData("barber_gallary",array('user_id'=>$aa['data']->user_id));
						$insertt = $this->db->query("INSERT INTO barber_gallary VALUES $impgalry");
					}
					if(!empty($insertt))
					{
						$newdata = $this->db->select('about_me,user_location,user_lat,user_lng,user_name')->get_where("barber_user",array('user_id'=>$aa['data']->user_id))->row();	
						$final_output['status'] = 'success'; 
						$final_output['message'] = 'Profile successfully updated';
						$final_output['data'] = array('user_image'=>$image,'gallery_pic'=>$galryimage,'about_me'=>$newdata->about_me,'user_location'=>$newdata->user_location,'user_lat'=>$newdata->user_lat,'user_lng'=>$newdata->user_lng,'user_name'=>$newdata->user_name); 
					}else
					{
						$final_output['status'] = 'failed'; 
						$final_output['message'] = some_error;
						//$final_output['data'] = $err;
					}
				}else
				{
					$final_output['status'] = 'failed'; 
					$final_output['message'] = some_error;
				}
			}else
			{
				$final_output['status'] = '1000';
	    		$final_output['message'] = admin_error;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end update profile (Y)

	function update_profile_ios()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{ 
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    //print_r($json_array);exit;
			if($aa['data']->admin_status==1)
			{
                $user_id = $aa['data']->user_id;
				$data['about_me'] = $json_array->about_me;
				$data['user_location'] = $json_array->user_location;
				$data['user_name'] = $json_array->user_name;
				$data['user_lat'] =  $json_array->user_lat;
				$data['user_lng'] =  $json_array->user_lng;
				$u_image =  $json_array->user_image;
				$gallery_image = $json_array->gallary_pic;
				$user_image = $galryimage = array();


				if(!empty($u_image))
				{
				    $image_name1 = 'img_'.militime.$aa['data']->user_id.'.jpeg';
				     
				    $path = 'uploads/barber_image/'.$image_name1;
				   
				    $base64img = str_replace('data:image/jpeg;base64,', '', $u_image);
				   
				    $data1 = base64_decode($base64img);
				   
				    $aa = file_put_contents($path, $data1);
				    $data['user_image'] = $image_name1;
					$image = base_url().'uploads/barber_image/'.$image_name1;
				}
				else
				{
					$getdata = $this->common_model->common_getRow("barber_user",array('user_id'=>$user_id));	
					if($getdata->user_image!=''){
						if (filter_var($getdata->user_image, FILTER_VALIDATE_URL)) {
							$image = $getdata->user_image;
						}else
						{
							//$user_image  =  $getdata->user_image
							$image = base_url().'uploads/barber_image/'.$getdata->user_image;
						}
					}else{
					 	$image = '';
					}
				}
				if(!empty($gallery_image))
				{
				   for($i=0; $i<count($gallery_image); $i++)
				   { 

				    $image_name = 'img'.$i.'_'.militime.$user_id.'.jpeg';

				    $path = 'uploads/gallary_image/'.$image_name;
				   
				    $base64img = str_replace('data:image/jpeg;base64,', '', $gallery_image[$i]);
				   
				    $data1 = base64_decode($base64img);
				   
				    $aa = file_put_contents($path, $data1);

				    $user_images[] = '("","'.$user_id.'","'.$image_name.'","'.datetime.'")';
					$galryimage[] = base_url().'uploads/gallary_image/'.$image_name;
				   
				   }
				}


				$data['update_date'] = date('Y-m-d H:i:s');
				$insertt = $this->common_model->updateData("barber_user",$data,array('user_id'=>$user_id));
				if($this->db->affected_rows())
				{
					if(!empty($user_images))
					{
						$impgalry = implode(',',$user_images);
						
						$delete = $this->common_model->deleteData("barber_gallary",array('user_id'=>$user_id));
						$insertt = $this->db->query("INSERT INTO barber_gallary VALUES $impgalry");
					}
					if(!empty($insertt))
					{
						$newdata = $this->db->select('about_me,user_location,user_lat,user_lng,user_name')->get_where("barber_user",array('user_id'=>$user_id))->row();	
						$final_output['status'] = 'success'; 
						$final_output['message'] = 'Profile successfully updated';
						$final_output['data'] = array('user_image'=>$image,'gallary_pic'=>$galryimage,'about_me'=>$newdata->about_me,'user_location'=>$newdata->user_location,'user_lat'=>$newdata->user_lat,'user_lng'=>$newdata->user_lng,'user_name'=>$newdata->user_name); 
					}else
					{
						$final_output['status'] = 'failed'; 
						$final_output['message'] = some_error;
						//$final_output['data'] = $err;
					}
				}else
				{
					$final_output['status'] = 'failed'; 
					$final_output['message'] = some_error;
				}
			}else
			{
				$final_output['status'] = '1000';
	    		$final_output['message'] = admin_error;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	
	function forgot_password()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	if($json_array->user_email!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('barber_user',array('user_email'=>$json_array->user_email,'user_type'=>2));
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
		    			if($seleuser->email_status==1)
		    			{
		    				$code =  $this->common_model->randomuniqueCode();
							$encryted_code = $this->common_model->encryptor_ym('encrypt', $code);
							$type = $this->common_model->encryptor_ym('encrypt', 2);

							$url = "<a href=".base_url()."forget/changepassword?8f14e45fceea167a5a36dedd4bea2543=".base64_encode($seleuser->user_id)."&type=".$type."&code=".$encryted_code." target='_blank' style='background: #388e3c; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";

							/*$config = Array(        
		            	     'mailtype'  => 'html', 
		            	     'charset'   => 'utf-8'
	   			     	    );
							$this->email->initialize($config);
	           				$this->email->set_newline("\r\n");
							$this->email->from('Barber@base3.engineerbabu.com', 'Barber');
							$this->email->to($json_array->user_email); 
							$this->email->subject('KwiKuts App: Reset Password Link');
							$this->email->set_mailtype('html');*/

							$config = Array(
				                  'protocol' => 'smtp',
				                  'smtp_host' => 'mail.zohomail.com',
				                  'smtp_port' => 465,
				                  'smtp_user' => 'Barbers@Kwikuts.co.uk',
				                  'smtp_pass' => 'Kwikuts2017',
				                  'mailtype'  => 'html', 
				                  'charset'   => 'utf-8',
				                  'wordwrap'  => TRUE
             					 );

					            $this->load->library('email', $config);

					            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
					            $this->email->set_header('Content-type', 'text/html');

					            $this->email->set_newline("\r\n");
		          
								$this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
								$this->email->to($json_array->user_email); 
								$this->email->subject('KwiKuts App: Reset Password Link..!');


							    $data = array('email'=>$seleuser->user_email,
                         			'url'=>$url,
                         			'name'=>$seleuser->user_name		
                         				);

							$message = $this->load->view('email_template/template_forgot_pass.php',$data,TRUE);

							$this->email->message($message);

							$this->email->send();

			           		$update = $this->common_model->updateData("barber_user",array("pass_code"=>$code),array('user_id'=>$seleuser->user_id));
			           		
			           		$final_output['status'] = 'success';
    						$final_output['message'] = "Reset password link has been sent to your registered email address.";
		    			}else
		    			{
		    				$final_output['status'] = 'failed';
	    					$final_output['message'] = 'Email address is not verified';
		    			}
	    			}else
	    			{
						$final_output['status'] = '1000';
	    				$final_output['message'] = admin_error;
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = 'Email address is not registered.';
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = request_params;
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end forgot password (Y)
	function change_password()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if($aa['data']->admin_status ==1){
				    $final_output = array();
				    if(!empty($json_array->old_password) && !empty($json_array->new_password))
				    {
				    	$selepas = $this->db->select('user_password')->get_where("barber_user",array('user_id'=>$aa['data']->user_id,'user_password'=>sha1($json_array->old_password)))->row();
				    	if(!empty($selepas))
				    	{
					    	$updatepass= $this->common_model->updateData("barber_user",array('user_password'=>sha1($json_array->new_password),'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$aa['data']->user_id));
					    	if($updatepass==true)
					    	{	
					    		$final_output['status'] = 'success';
		    					$final_output['message'] = "Password has been successfully changed";
					    	}else
					    	{
					    		$final_output['status'] = 'failed';
		    					$final_output['message'] = some_error;
					    	}
				    	}else
				    	{
				    		$final_output['status'] = 'failed';
							$final_output['message'] = "Current password does not match";
				    	}
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = "All parameters are required";
				    }
			}else{
				$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end Chagne Password (Y)

	function Manage_availability()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				    if(!empty($json_array))
				    {
				    	$user_id = $aa['data']->user_id;
				    	$data['always'] = $json_array->always;
				    	$data['create_date'] = date('Y-m-d H:i:s');
				    	if($data['always'] == 0)
				    	{
				    		$data['availability'] = json_encode($json_array);
				    		$countt = count($json_array->timings);
				    		for ($i=0; $i < $countt; $i++) { 
				    			if($json_array->timings[$i]->day==0){
				    				$data['mon'] = json_encode($json_array->timings[$i]->timearr);
				    			}elseif ($json_array->timings[$i]->day==1) {
				    				$data['tue'] = json_encode($json_array->timings[$i]->timearr);
				    			}elseif ($json_array->timings[$i]->day==2) {
				    				$data['wed'] = json_encode($json_array->timings[$i]->timearr);
				    			}elseif ($json_array->timings[$i]->day==3) {
				    				$data['thu'] = json_encode($json_array->timings[$i]->timearr);
				    			}elseif ($json_array->timings[$i]->day==4) {
				    				$data['fri'] = json_encode($json_array->timings[$i]->timearr);
				    			}elseif ($json_array->timings[$i]->day==5) {
				    				$data['sat'] = json_encode($json_array->timings[$i]->timearr);
				    			}elseif ($json_array->timings[$i]->day==6) {
				    				$data['sun'] = json_encode($json_array->timings[$i]->timearr);
				    			}
				    		}
				    	}

				    	$selepas = $this->common_model->common_getRow('barber_availability',array('barber_id'=>$user_id));
				    	if(!empty($selepas))
				    	{
				    		$Manage_availability= $this->common_model->updateData("barber_availability",$data,array('barber_id'=>$user_id));
					 	}else
				    	{
				    		$data['barber_id']= $user_id;
				    		$Manage_availability = $this->common_model->common_insert("barber_availability",$data);
				    	}
			    	 	if($Manage_availability==true)
				    	{	
				    		$final_output['status'] = 'success';
	    					$final_output['message'] = "Availability successfully added";
	    					$final_output['data'] = $json_array;
				    	}else
				    	{
				    		$final_output['status'] = 'failed';
	    					$final_output['message'] = some_error;
				    	}
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = "All parameters are required";
				    }
			}else{
				$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}

	function Rating_and_Review_list()
    {
    	$aa = $this->check_authentication();
		if($aa['status']=='true')
		{ 
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = $rating_arr = array();
				$barber_id = $aa['data']->user_id;
				$create_at = $json_array->create_at;

				if(!empty($barber_id))
				{ 
					if($create_at==0)
					{
						$create_at1= "WHERE barber_id = $barber_id";
					}else
					{
						$create_at1= "WHERE create_at < $create_at AND barber_id = $barber_id";
					}

					$query = $this->db->query('SELECT * FROM barber_rating_review '.$create_at1.' ORDER BY rating_id DESC LIMIT 10');
 
					$ratinglist = $query->result();

					if(!empty($ratinglist))
					{
                          foreach($ratinglist as $rating)
                          {
                          	$userdata = $this->db->query('SELECT user_name,user_image FROM barber_user WHERE user_id = '.$rating->user_id.'')->row();

                          	if(!empty($userdata))
                          	{
                          		if (filter_var($userdata->user_image, FILTER_VALIDATE_URL))
                          		{
								   $image = $userdata->user_image;
								}
								else
								{
									$image = base_url().'uploads/user_image/'.$userdata->user_image;
								}

                          	    $user_name = $userdata->user_name;
                          	}
                          	else
                          	{
                          	  $user_name = '';
                          	  $image = '';
                          	}	

                          	$rating_arr[] = array(
                          				'user_name'=>$user_name,
                          				'user_image'=>$image,
                          				'rating'=>$rating->rating,
                          				'review'=>$rating->review,
                          				'review_date'=>date('Y-m-d',strtotime($rating->update_date)),
                          				'create_at'=>$rating->create_at
                          		    );
                           }
                            $final_output['status'] = 'success';
							$final_output['message'] = "Rating & Review List";
							$final_output['data'] = $rating_arr;

					}
					else
					{
                            $final_output['status'] = 'failed';
							$final_output['message'] = "Rating & Review Not Found.";
							unset($final_output['data']);
					}	
				}
				else
				{  
					$final_output['status'] = 'failed';
	    	        $final_output['message'] = request_params;
				}	
		    }
		    else
		    {
		    	$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
		    }	
		}
		else
		{
			$final_output = $aa;
		}	
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }


    function reject_booking()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {  
		    	$final_output = array();

		    	if($json_array->booking_id != '')	
			    {
			    	$user_id = $aa['data']->user_id;

			    	$cancel_booking = $this->common_model->updateData("barber_customer_booking",array('booking_status'=>3,'booking_status_datetime'=>date('Y-m-d h:i:s'),'reason_of_cancel'=>$json_array->reason),array('booking_id'=>$json_array->booking_id));

			    	if($cancel_booking == true)
			    	{

			    	$get_barber_data = $this->db->query("SELECT user_id FROM barber_customer_booking WHERE booking_id  = '".$json_array->booking_id."'")->row();

			    	$user_devicetoken = $this->db->query("SELECT user_device_token,user_device_type FROM barber_user WHERE user_id  = '".$get_barber_data->user_id."'")->row();

				    	if(!empty($user_devicetoken->user_device_token) && ($user_devicetoken->user_device_type == 'android'))
	            		{
    					  $title = 'Reject Booking Request';	
    					  $msg = "booking request Rejected by ".$aa['data']->user_name."";
    		 			  $message = array("title"=>$title,"type"=>2,"message"=>$msg,"currenttime"=>militime);

             			  $this->common_model->sendPushNotification($user_devicetoken->user_device_token,$message);

             			  $insertnoti = $this->common_model->common_insert('barber_notification',array('sender_id'=>$user_id,'reciever_id'=>$get_barber_data->user_id,'type'=>'2','title'=>$title,'message'=>$msg,'create_at'=>militime));
	            		}
	            		else if(!empty($user_devicetoken->user_device_token) && ($user_devicetoken->user_device_type == 'iOS'))
		            	{
		            		//$pushStatus = $this->common_model->iOSPushNotification1($user_devicetoken->user_device_token,$msg,$title,'2',$get_barber_data->user_id,$user_id);
		            	}

		            /*		$config = Array(        
		            	'mailtype'  => 'html', 
		            	'charset'   => 'utf-8'
	   			     	);

			            $this->email->initialize($config);
			            $this->email->set_newline("\r\n");
						$this->email->from('Test@kartavya.me', 'Kartavya');
						$this->email->to($email); 
						$this->email->subject('Jai Bhim, Welcome to the Kartavya Family..!');
						$this->email->set_mailtype('html');

						$data = array('username'=>$this->input->post('name'),
									  'email'=>$email,
									  'password'=>$password
									);

						$message = $this->load->view('admin/email_template/kartavya.php',$data,TRUE);
           
         				$this->email->message($message);

         				$this->email->send()*/

			    		$final_output['status'] = 'success';
			    		$final_output['message'] = 'Booking Has Been Reject Successfully.';
			    	}
			    	else
			    	{
			    		$final_output['status'] = 'failed';
			    		$final_output['message'] = 'Something Went Wrong.';	
			    	}	
			    }
			    else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = request_params;	
			    }	
		    }
		    else
		    {
		       $final_output['status'] = 'failed';
			   $final_output['message'] = admin_status;	
		    }		
	  	}
	  	else
	  	{
	  		$final_output = $aa;
	  	}
	  	header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

    function accept_booking_action()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    { 
		    	$final_output = array();
		    	if($json_array->booking_id != '' && $json_array->user_id != '')	
			    {
			    	$barber_id = $aa['data']->user_id;
			    	$booking_id = $json_array->booking_id;
			    	$user_id = $json_array->user_id;
			    	$transport_mode = $json_array->transport_mode;

			    	$getdata = $this->db->query("SELECT service_id,booking_time,booking_date FROM barber_customer_booking WHERE booking_id  = '".$booking_id."'")->row();

			    	$booking_datetime = $getdata->booking_date.' '.$getdata->booking_time;


			    	$service_id = json_decode($getdata->service_id);


		    		for($i=0;$i<count($service_id);$i++)
		    		{
		    		   $no_of_person[] = $service_id[$i]->no_of_per;
		    		}	

		    		$total_person = array_sum($no_of_person);

		    		$taking_time_by_barber = 30*$total_person + 30;

		    		//$booking_time = $getdata->booking_time;
		    		$startTime = strtotime("-30 minutes", strtotime($booking_datetime));
		    		$starttime1 = date('Y-m-d H:i', $startTime);

		    		$endTime = strtotime("+".$taking_time_by_barber." minutes", strtotime($booking_datetime));

		    		$endTime1 = date('Y-m-d H:i', $endTime);

			    	$accept_booking = $this->common_model->updateData("barber_customer_booking",array('booking_status'=>1,'booking_status_datetime'=>date('Y-m-d h:i:s'),'transport_mode'=>$transport_mode,'start_booking_time'=>$starttime1,'end_booking_time'=>$endTime1),array('booking_id'=>$booking_id,'barber_id'=>$barber_id,'user_id'=>$user_id));

			    	if($accept_booking == true)
			    	{
			    		$user_devicetoken = $this->db->query("SELECT user_device_token,user_device_type FROM barber_user WHERE user_id  = '".$user_id."'")->row();

			    		 $title = 'Accept Booking Request';	
    					 $msg = "Booking request Accept by ".$aa['data']->user_name."";


				    	if(!empty($user_devicetoken->user_device_token) && ($user_devicetoken->user_device_type == 'android'))
	            		{
    					 
    		 			  $message = array("title"=>$title,"type"=>2,"message"=>$msg,"currenttime"=>militime);

             			  $this->common_model->sendPushNotification($user_devicetoken->user_device_token,$message);
             			
	            		}
	            		else if(!empty($user_devicetoken->user_device_token) && ($user_devicetoken->user_device_type == 'android'))
	            		{
	            			//$pushStatus = $this->common_model->iOSPushNotification1($user_devicetoken->user_device_token,$msg,$title,'2',$user_id,$barber_id);
	            		}	


	            		$insertnoti = $this->common_model->common_insert('barber_notification',array('sender_id'=>$barber_id,'reciever_id'=>$user_id,'type'=>'2','title'=>$title,'message'=>$msg,'create_at'=>militime));

	            		$final_output['status'] = 'success';
			    		$final_output['message'] = 'Booking Has Been Accepted Successfully.';

			    	}
			    	else
			    	{
			    		$final_output['status'] = 'failed';
			    		$final_output['message'] = 'Something Went Wrong.';	
			    	}	
			    }
			    else
				{
					$final_output['status'] = 'failed';
			    	$final_output['message'] = request_params;
				}			    	
		    }
		    else
		    {
		       $final_output['status'] = 'failed';
			   $final_output['message'] = admin_status;	
		    }	
	  	}
	  	else
		{
			$final_output = $aa;
		}	

		header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }


    function complete_booking_action()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();

		    	if($json_array->booking_id != '' && $json_array->booking_code != '')	
			    {
			    	$user_id = $aa['data']->user_id;
			    	$booking_id = $json_array->booking_id;
			    	$booking_code = $json_array->booking_code;

			    	$check_validcoupan = $this->common_model->common_getRow('barber_customer_booking',array('booking_id'=>$booking_id,'booking_code'=>$booking_code,'barber_id'=>$user_id));

			    	if(!empty($check_validcoupan))
			    	{
			    		$completed_booking = $this->common_model->updateData("barber_customer_booking",array('booking_status'=>2,'booking_status_datetime'=>date('Y-m-d h:i:s')),array('booking_id'=>$json_array->booking_id));

			    		if($completed_booking == true)
			    		{
			    			$final_output['status'] = 'success';
			    			$final_output['message'] = 'Booking Completed Successfully.';	
			    		}
			    		else
			    		{
			    			$final_output['status'] = 'failed';
			    			$final_output['message'] = 'Something Went Wrong.';
			    		}	
			    	}
			    	else
			    	{
			    		$final_output['status'] = 'failed';
			    		$final_output['message'] = 'invalid Booking Code.';	
			    	}		
			    }
			    else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = request_params;	
			    }	
		    }
		    else
		    {
		       $final_output['status'] = 'failed';
			   $final_output['message'] = admin_status;	
		    }	
	  	}
	  	else
		{
			$final_output = $aa;
		}	

		header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

    function barber_stats()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();

		    	$barber_id = $aa['data']->user_id;
		    	//$query = $this->db->query("SELECT * FROM barber_customer_booking WHERE booking_status_datetime BETWEEN date_sub(now(),INTERVAL 1 WEEK) and now()")->result();

		    	$thisweek = $total_booking = $completed_booking = $accepted_booking = $total_earn = 0;

		    	$query = $this->db->query("SELECT COUNT(*) AS thisweek  FROM barber_customer_booking WHERE YEARWEEK(`booking_status_datetime`, 1) = YEARWEEK(CURDATE(), 1) AND barber_id = '$barber_id'")->result();

		    	if(!empty($query)){ $thisweek = $query[0]->thisweek; }


			    $total_bookings =  $this->db->query("SELECT COUNT(*) AS total_booking FROM barber_customer_booking WHERE barber_id = '$barber_id'")->result();

			    if(!empty($total_bookings)) { $total_booking = $total_bookings[0]->total_booking;}


			    $completed_bookings =  $this->db->query("SELECT COUNT(*) AS completed_booking FROM barber_customer_booking WHERE barber_id = '$barber_id' AND booking_status = 2")->result();

			    if(!empty($completed_bookings)) { $completed_booking = $completed_bookings[0]->completed_booking;}


			    $accepted_bookings =  $this->db->query("SELECT COUNT(*) AS accepted_booking FROM barber_customer_booking WHERE barber_id = '$barber_id' AND booking_status = 1")->result();

			    if(!empty($accepted_bookings)) { $accepted_booking = $accepted_bookings[0]->accepted_booking;}


			    $total_earning =  $this->db->query("SELECT SUM(booking_amount) AS total_earn FROM barber_customer_booking WHERE barber_id = '$barber_id'")->result();

			    if(!empty($total_earning[0]->total_earn)) {  $total_earn = $total_earning[0]->total_earn;}

				    $final_output['status'] = 'success';
				    $final_output['message'] = 'successfully';
				    $final_output['thisweek_count'] = $thisweek;
				    $final_output['totalbooking_count'] =$total_booking;
				    $final_output['completedbooking_count'] = $completed_booking;
				    $final_output['acceptedbooking_count'] = $accepted_booking;
				    $final_output['total_earning'] = $total_earn;
		    }
		    else
		    {
		    	$final_output['status'] = 'failed';
			    $final_output['message'] = admin_status;	
		    }	
		    	
	  	}
	  	else
		{
			$final_output = $aa;
		}	

		header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

    function product_service_list()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);	

	  		if($aa['data']->admin_status ==1)
		    {
		    	$final_output = $arr1 = $arr = $main_array =  array();
		    	$barber_id = $aa['data']->user_id;
                $product_data = $this->common_model->getData('barber_product',array(),'product_name','ASC');

                if(!empty($product_data))
                {
                	foreach($product_data as $product)
                	{
                		$arr[] = array('product_id'=>$product->product_id,
                					   'product_name'=>$product->product_name,
                					   'product_price'=>$product->price,
                					   'product_quantity'=>$product->quantity
                			);
                	}
                	
                }
                	$current_time = date('H:i'); 
			    	$booking_day = strtolower(date('D'));
			    	$booking_date = date('Y-m-d');

                	$avaibility = $this->check_barber_av($barber_id,$current_time,$booking_day,$booking_date);

                	if($avaibility == 'true')
                	{ 
                		$service_data = $this->common_model->getData('barber_services',array(),'service_name','ASC');

                		if(!empty($service_data))
                		{
                			foreach($service_data as $services)
                			{
                				$arr1[] = array('service_id'=>$services->service_id,
                					   'service_name'=>$services->service_name,
                					   'service_price'=>$services->service_price );
                			}	
                		}	
                	}

                	$main_array = array('products'=>$arr,'services'=>$arr1);

                	if(!empty($arr) OR !empty($arr1))
                	{
                		$final_output['status'] = 'success';
			        	$final_output['message'] = 'successfully';
			        	$final_output['data'] = $main_array;
                	}	
               		else
                	{
	                	$final_output['status'] = 'failed';
				        $final_output['message'] = 'Not Found.';
               		}
		    }
		    else
		    {
		       $final_output['status'] = 'failed';
			   $final_output['message'] = admin_status;	
		    }
	  	}
	  	else
		{
			$final_output = $aa;
		}	

		header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

    function check_barber_av($barber_id = false,$current_time = false,$booking_day = false,$booking_date = false)
    {
    	$booking_datetime = $booking_date.' '.$current_time;

    	$checkbarber = $this->db->query("SELECT * FROM barber_customer_booking WHERE '$booking_datetime' BETWEEN start_booking_time AND end_booking_time AND barber_id = '$barber_id' AND booking_date = '$booking_date' AND booking_day = '$booking_day' AND booking_status IN(0,1)")->row();

		$available = 'true';
		if(!empty($checkbarber))
		{ 
			$available = 'false';
		}	

	    $checkinwebappointment = $this->db->query("SELECT * FROM barber_web_appointment WHERE dummy_barber_id = '$barber_id' AND booking_date = '$booking_date' AND '$booking_datetime' BETWEEN start_booking_time AND end_booking_time AND admin_status = 1")->row();

		if(!empty($checkinwebappointment))
		{
			$available = 'false';
		}

		 return $available;
    }

    function additional_product_service()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);	

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();
		    	$barber_id = $aa['data']->user_id;
	    		$user_email = $json_array->user_email;
	    		$services = $json_array->services;
	    		$products = $json_array->products;
	    		$total_amount = $json_array->total_amount;

	    		$checkemail = $this->db->query("SELECT user_email,user_id FROM barber_user WHERE user_email = '".$user_email."' AND user_type = 1")->row();

	    		if(!empty($checkemail))
	    		{
	    			$starttime1 = $endTime1 = $services_id = '';
	    			if($services != '')
			    	{
			    		$services_id = json_encode($services);

				    	$current_time = date('H:i'); 
				    	$booking_day = strtolower(date('D'));
				    	$booking_date = date('Y-m-d');

	                	$avaibility = $this->check_barber_av($barber_id,$current_time,$booking_day,$booking_date);

	                	if($avaibility == 'true')
	                	{ 
			    			for($i=0;$i<count($services);$i++)
			    			{
			    		  	    $no_of_person[] = $services[$i]->no_of_per;
			    			}	

		    				$total_person = array_sum($no_of_person);

				    		$taking_time_by_barber = 30*$total_person + 30;

				    		$starttime1 = $booking_date.' '.$current_time;

					    	$endTime = strtotime("+".$taking_time_by_barber." minutes", strtotime($starttime1));

					      	$endTime1 = date('Y-m-d H:i', $endTime);
	                	}
	                	else
	                	{
	                		$services_id = '';
	                	}	
	                }	

	                  $booking_code = $this->randno(6);

                	   $arr = array(
	    			 	          'user_id'=>$checkemail->user_id,
	    			 			  'barber_id'=>$barber_id,
	    			 			  'service_id'=>$services_id,
	    			 			  'product_id'=>json_encode($products),
	    			 			  'booking_time'=>$current_time,
	    			 			  'booking_day' =>$booking_day,
	    			 			  'booking_date'=>$booking_date,
	    			 			  'booking_type'=>2,
	    			 			  'booking_status'=>0,
	    			 			  'booking_code'=>$booking_code,
	    			 			  'month'=>date('M'),
	    			 			  'year'=>date('Y'),
	    			 			  'start_booking_time'=>$starttime1,
	    			 			  'end_booking_time'=>$endTime1,
	    			 			  'booking_status_datetime'=>datetime,
	    			 			  'create_date'=>datetime,
	    			 			  'update_date'=>datetime
	    			 			);

				    	$insert = $this->common_model->common_insert("barber_customer_booking",$arr);

				    	if($insert)
				    	{
				    		$user_devicetoken = $this->db->query("SELECT user_device_token,user_device_type FROM barber_user WHERE user_id  = ".$checkemail->user_id."")->row();

					    	$title = 'Addional Booking';	
		            		$msg = "Thank you for using additional services please pay ".$total_amount."";

			    			if(!empty($user_devicetoken->user_device_token) && ($user_devicetoken->user_device_type == 'android'))
            				{
            		 			$message = array("title"=>$title,"booking_id"=>$insert,"user_id"=>$checkemail->user_id,'barber_id'=>$barber_id,"type"=>2,"message" =>$msg,"currenttime"=>militime);

                     			$this->common_model->sendPushNotification($user_devicetoken->user_device_token,$message);
                     			   
            				}
            				else if(!empty($user_devicetoken->user_device_token) && ($user_devicetoken->user_device_type == 'iOS'))
            				{
            						//$pushStatus = $this->common_model->iOSPushNotification($user_devicetoken->user_device_token,$msg,$title,'1',$user_id,$barber_id);
            				}	

            				$insertnoti = $this->common_model->common_insert('barber_notification',array('sender_id'=>$checkemail->user_id,'reciever_id'=>$barber_id,'type'=>'2','title'=>$title,'message'=>$msg,'create_at'=>militime));

			    					
			    			$final_output['status'] = 'success';
			    		    $final_output['message'] = 'Successfully Booked.';
				    	}	

	    		}
	    		else
	    		{
	    			$final_output['status'] = 'failed';
			  		$final_output['message'] = 'Email does not exists';	
	    		}	
		    }
		    else
		    {
		       $final_output['status'] = 'failed';
			   $final_output['message'] = admin_status;	
		    }	
	  	}
	  	else
		{
			$final_output = $aa;
		}	

		header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }
    
	
	function check_version()
	{
		$json = file_get_contents('php://input');
		$json_array = json_decode($json);
		if($json_array->type=='android')
		{
			$check_version = $this->db->select('min_version')->get_where("app_version",array('min_version <=' => $json_array->version_code,'version_id'=>1))->row();
		}else
		{
			$check_version = $this->db->select('min_version')->get_where("app_version",array('min_version <=' => $json_array->version_code,'version_id'=>2))->row();
		}
		if(!empty($check_version))
		{
			$final_output['status'] = 'success';
	    	$final_output['message'] = 'successfully';
		}else
		{
			$final_output['status'] = 'failed';
	    	$final_output['message'] = "";
		}
		header("content-type: application/json");
	    echo json_encode($final_output);	
	}


    function update_lat_long()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();

		    	$barber_id = $aa['data']->user_id;

		        $update_lat_long = $this->common_model->updateData("barber_user",array('user_lat'=>$json_array->lat,'user_lng'=>$json_array->long),array('user_id'=>$barber_id));

		    	if($update_lat_long != false)
		    	{
		    		$final_output['status'] = 'success';
			   		$final_output['message'] = 'updated successfully';	
		    	}
		    	else
		    	{
		    		$final_output['status'] = 'failed';
			   		$final_output['message'] = 'Not updated';	
		    	}	
		    }
		    else
		    {
		    	$final_output['status'] = 'failed';
			    $final_output['message'] = admin_status;	
		    }	
	    }
	  	else
		{
			$final_output = $aa;
		}	

		header("content-type: application/json");
	    echo json_encode($final_output); exit;

    }

    

    function get_customer_status()
    {		
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();
		    	$barber_id = $aa['data']->user_id;
		    	$user_id = $json_array->user_id;
		    	$booking_id = $json_array->booking_id;

		    	$get_status = $this->db->query("SELECT booking_status FROM barber_customer_booking WHERE booking_id  = ".$booking_id." AND user_id = ".$user_id." AND barber_id = ".$barber_id."")->row();

		    	if(!empty($get_status))
		    	{
		    		$data['booking_status'] = $get_status->booking_status;
		    		$final_output['status'] = 'success';
			    	$final_output['message'] = "successfully";
			    	$final_output['data'] = $data;
		    	}
		    	else
		    	{
		    		$final_output['status'] = 'failed';
			    	$final_output['message'] = some_error;	
		    	}	
		    }
		    else
		    {
		    	$final_output['status'] = 'failed';
			    $final_output['message'] = admin_status;	
		    }	
	  	}
	  	else
		{
			$final_output = $aa;
		}	

		header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

 //end check app version (Y)	

	function check_authentication()
	{
	    $response = '';
	 	$headers = $_SERVER['HTTP_SECRET_KEY'];
		if(!empty($headers))
		{
			$check = $this->ChechAuth($headers);
			if($check['status']=="true")
			{
				$final_output['data'] = $check['data'];
				$final_output['status'] = "true";
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['status'] ="false";
			$final_output['message'] = "Unauthorised access";
		}
	    return $final_output;	
	}
	
	function ChechAuth($token)
	{
		$auth = $this->common_model->getDataField('user_id,admin_status,user_name','barber_user',array('user_token'=>$token));
		if(!empty($auth))
		{
			$abc['status'] = "true";
			$abc['data'] = $auth[0];
			return $abc;
		}else
		{
			$abc['status'] = "false";
			return $abc;
		}
	}
	
}
?>