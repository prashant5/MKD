<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Food_temple_user extends MY_Controller {
function __construct() {
		parent::__construct();
		$militime =round(microtime(true) * 1000);
		date_default_timezone_set('Asia/Kolkata');
		$datetime =date('Y-m-d H:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
			
		/*if($this->check_authentication() != 'success')
        die;*/
	}
	
    function registration()
    {
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	if($json_array->emailid!='' && $json_array->mobileno!='' && $json_array->password!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('user',array('emailid'=>$json_array->emailid,'user_type'=>1));
	    		if(!empty($seleuser))
	    		{
    				if($seleuser->ismobileverified ==1)
	    			{	
						if($seleuser->isemailverified ==1)
		    			{	
							if($seleuser->status==1)
    						{
								$final_output['status'] = 'failed';
		    					$final_output['message'] = 'Email address and mobile number already registered.';
			    			}else
				    		{
				    			$final_output['status'] = '1000';
			    				$final_output['message'] = admin_status;
				    		}
		    			}else
		    			{
		    				$final_output['status'] = 'failed';
	    					$final_output['message'] = "Email address already registered! please login.";
		    			}
	    			}else
	    			{
	    				$selectmob = $this->common_model->common_getRow("user",array('mobileno'=>$json_array->mobileno,'mobile_status'=>1,'user_type'=>1,'id !='=>$seleuser->id));
	    				if(!empty($selectmob))
	    				{
							$final_output['status'] = 'failed';
	    					$final_output['message'] = 'Mobile number has been already registered! please try another mobile number.';
	                    }else
	                    {
							//$otp = $this->common_model->randomuniqueCode();
	                    	$otp = '1234';
	                    	$update = $this->common_model->updateData("user",array('mobileno'=>$json_array->mobileno,'verificationcode'=>hash('sha256', $otp),'registrationOn'=>datetime),array('id'=>$seleuser->id));
	                   		$final_output['status'] = 'success';
	    					$final_output['message'] = 'Successfully registered.';
	    					$final_output['user_id'] = $seleuser->id; 
	                    }
	    			}
		    	}else
	    		{
    				$v_code = $this->common_model->randomuniqueCode();
    				$v_Code = $this->common_model->encryptor_ym('encrypt',$v_code);
	    			//$otp = $this->common_model->randomuniqueCode();
		            $otp = '1234';
	    			$selectmob = $this->common_model->common_getRow("user",array('mobileno'=>$json_array->mobileno,'ismobileverified'=>1,'usertype'=>1));
    				if(!empty($selectmob))
    				{
						$final_output['status'] = 'failed';
    					$final_output['message'] = 'Mobile number has been already registered! please try another mobile number.';
                    }else
                    {
						$insert = $this->common_model->common_insert("user",array('firstname'=>$json_array->firstname,'emailid'=>$json_array->emailid,'mobileno'=>$json_array->mobileno,'usertype'=>1,'status'=>1,'isemailverified'=>0,'email_code'=>$v_Code,'verificationcode'=>hash('sha256', $otp),'password'=>sha1($json_array->password)
						,'registrationOn'=>datetime,'deviceid'=>$json_array->deviceid,'devicetype'=>$json_array->devicetype,'devicetoken'=>$json_array->devicetoken));
		    			if(!empty($insert) && $insert != false)
		    			{
							//Send verification mail
		    				// $url = "<a href=".base_url()."customer_api/email_verification/".$this->common_model->encryptor_ym('encrypt',$insert)."/".$v_Code." target='_blank' title='' style='background: #d3a117; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";

		    				// $data = array('email'=>$json_array->user_email,
				      //                     'name'=>$json_array->user_name,
				      //                      'url'=>$url
				      //                     );

	                     // $message = $this->load->view('email_template/template_verify.php',$data,TRUE);
						
					    //$this->load->library('email');
       
				       /* $config = array(
				            'protocol'  => 'smtp',
				            'smtp_host' => 'ssl://smtp.zoho.com',
				            'smtp_port' => 465,
				            'smtp_user' => 'Barbers@Kwikuts.co.uk',
				            'smtp_pass' => 'Kwikuts2017',
				            'mailtype'  => 'html',
				            'charset'   => 'utf-8'
				            );

				            $this->email->initialize($config);
				            $this->email->set_mailtype("html");
				            $this->email->set_newline("\r\n");
          
							$this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
							$this->email->to($json_array->user_email); 
							$this->email->subject('KwiKuts App: Verification Link..!');

							$this->email->message($message);

							$this->email->send();*/

	                       	$final_output['status'] = 'success';
		    				$final_output['message'] = 'Successfully registered.';
		    				$final_output['user_id'] = $insert; 
	        			}else
		    			{
		    				$final_output['status'] = 'failed'; 
						 	$final_output['message'] = some_error;
		    			}
		    		}
		    	}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = request_params;
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}

	function login()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();

	    if(!empty($json_array))
	    { 
	    	if($json_array->emailid!='' && $json_array->password!='')
	    	{
	           $seleuser = $this->common_model->common_getRow('user',array('mobileno'=>$json_array->mobileno,'password'=>sha1($json_array->password),'usertype'=>1));

	    		if(!empty($seleuser))
	    		{
		    		if($seleuser->mobile_status == 1)
		    		{	
							if($seleuser->status == 1)
							{
								$token = bin2hex(openssl_random_pseudo_bytes(16));
        						$token = $token.militime;
	    						$update = $this->common_model->updateData('user',array('devicetype'=>$json_array->devicetype,'deviceid'=>$json_array->deviceid,'devicetoken'=>$json_array->devicetoken,'token'=>$token,'updateOn'=>datetime),array('id'=>$seleuser->id));
	    						
	    						$updatedevicetoken = $this->common_model->updateData('user',array('token'=>'','devicetoken'=>''),array('id !='=>$seleuser->id,'deviceid'=>$json_array->deviceid,'usertype'=>$seleuser->usertype));
	    						
	    						$image = '';
								if(!empty($seleuser->image))
								{
									if (filter_var($seleuser->image, FILTER_VALIDATE_URL)) {
										$image = $seleuser->image;
									}else
									{
										$image = base_url().'uploads/user_image/'.$seleuser->image;
									}
								}

								$object = array(
									'user_id'=>$seleuser->user_id,
									'user_name'=>$seleuser->firstname,
									'user_image'=>$image,
									'user_email'=>$seleuser->emailid,
									'user_mobile_num'=>$seleuser->mobileno,
									'user_device_type'=>$json_array->devicetype,
									'user_device_id'=>$json_array->deviceid,
									'user_device_token'=>$json_array->devicetoken,
									'user_token'=>$token
									);
								$final_output['status'] = 'success';
	    						$final_output['message'] = 'Successfully Login';
	    						$final_output['data'] = $object;
							}else
							{
								$final_output['status'] = '1000';
	    						$final_output['message'] = admin_status;
							}
					}else
		    		{
						$otp = '1234';
						$update = $this->common_model->updateData('user',array('verificationcode'=>hash('sha256', $otp),'updateOn'=>datetime),array('user_id'=>$seleuser->id));

						$arr = (object)array('user_id'=>$seleuser->id,
									 'user_mobile_num'=>$seleuser->mobileno	
									);

						$final_output['status'] = 'failed';
    					$final_output['message'] = "Please verify your mobile";
    					$final_output['data'] = $arr;
		    		}
	    			
		    	}else
		    	{
		    			$final_output['status'] = 'failed';
		    			$final_output['message'] = 'Invalid login credentials';
		    	}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = request_params;
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}

    function otp_verification()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array->otp) && !empty($json_array->user_id))
	    {
	    	$checkotp = $this->common_model->common_getRow("user",array('user_id'=>$json_array->user_id,'usertype'=>1));
	    	if(!empty($checkotp))
	    	{	
				if(hash('sha256', $json_array->otp) == $checkotp->mobile_code)
				{
					if($checkotp->social_id != '')
					{
						$token = bin2hex(openssl_random_pseudo_bytes(16));
        				$token = $token.militime;

        				$updateotp = $this->common_model->updateData("user",array('verificationcode'=>'','ismobileverified'=>1,'updateOn'=>datetime,'user_token'=>$token),array('user_id'=>$json_array->user_id));
					}
					else
					{
						$updateotp = $this->common_model->updateData("user",array('verificationcode'=>'','ismobileverified'=>1,'updateOn'=>datetime),array('id'=>$json_array->user_id));		
						
						$token = $checkotp->user_token;				
					}	

					if($updateotp!=false)
					{
						$image = '';
						if(!empty($checkotp->image))
						{
							if (filter_var($checkotp->image, FILTER_VALIDATE_URL)) {
								$image = $checkotp->image;
							}else
							{
								$image = base_url().'uploads/user_image/'.$checkotp->image;
							}
						}


						$object =  array('user_id'=>$checkotp->id,
									'user_name'=>$checkotp->firstname,
									'user_image'=>$image,
									'user_email'=>$checkotp->emailid,
									'user_mobile_num'=>$checkotp->mobileno,
									'user_device_type'=>$checkotp->devicetype,
									'user_device_id'=>$checkotp->deviceid,
									'user_device_token'=>$checkotp->devicetoken,
									'user_token'=>$token
									);

						$final_output['status'] = 'success';
						$final_output['message'] = 'OTP has been verified successfully.';
						$final_output['data'] = $object;
					}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = some_error;
					}
				}else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = "Otp does not match.";
				}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = 'Account does not exists.';		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = some_error;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	} 


	function customer_social_login()
    {
    	$json = file_get_contents('php://input');
	    $json_array = json_decode($json);

	    $final_output = array();

	    if(!empty($json_array))
	    {
	    	if($json_array->emailid!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('user',array('emailid'=>$json_array->emailid,'usertype'=>1));

				if(!empty($seleuser))
	    		{
	    			if($seleuser->status==1)
	    			{

	    				if($seleuser->ismobileverified==1)
	    				{
	    					$token = bin2hex(openssl_random_pseudo_bytes(16));
        					$token = $token.militime;

	    					$update = $this->common_model->updateData('user',array('devicetype'=>$json_array->devicetype,'user_device_id'=>$json_array->deviceid,'devicetoken'=>$json_array->devicetoken,'token'=>$token,'social_id'=>$json_array->social_id,'firstname'=>$json_array->firstname,'image'=>$json_array->image,'status'=>1,'isemailverified'=>1,'updateOn'=>datetime),array('id'=>$seleuser->id));
	    						
	    					$updatedevicetoken = $this->common_model->updateData('user',array('token'=>'','devicetoken'=>''),array('id !='=>$seleuser->user_id,'deviceid'=>$json_array->device_id,'usertype'=>$seleuser->usertype));

	    					if(!empty($seleuser->mobileno))
	    					{
	    						$user_mobile_num = $seleuser->mobileno;
	    					}
	    					else
	    					{
	    						$user_mobile_num = '';
	    					}	

	    					$object = array(
									'user_id'=>$seleuser->user_id,
									'user_name'=>$seleuser->user_name,
									'user_image'=>$json_array->user_image,
									'user_email'=>$seleuser->user_email,
									'user_mobile_num'=>$user_mobile_num,
									'user_device_type'=>$json_array->user_device_type,
									'user_device_id'=>$json_array->user_device_id,
									'user_device_token'=>$json_array->user_device_token,
									'user_token'=>$token
									
									);
								$final_output['status'] = 'success';
	    						$final_output['message'] = 'Successfully Login';
	    						$final_output['data'] = $object;
	    				}
	    				else
	    				{
	    					$final_output['status'] = 'failed';
	    				    $final_output['message'] = 'Verify OTP.';
	    				    $final_output['user_id'] = $seleuser->id; 
	    				}		

	    			}
	    			else
	    			{
	    				$final_output['status'] = '1000';
	    				$final_output['message'] = admin_status;
	    			}	
	    		}
	    		else
	    		{
	    			$token = bin2hex(openssl_random_pseudo_bytes(16));
        			$token = $token.militime;

	    			$insert = $this->common_model->common_insert("barber_user",array('firstname'=>$json_array->firstname,'emailid'=>$json_array->emailid,'user_type'=>1,'registrationOn'=>datetime,'deviceid'=>$json_array->deviceid,'devicetype'=>$json_array->devicetype,'devicetoken'=>$json_array->devicetoken,'social_id'=>$json_array->social_id,'image'=>$json_array->image,'status'=>1,'isemailverified'=>1));

	    			if(!empty($insert))
		    		{
		    			$object = array(
									'user_id'=>$insert,
									'user_name'=>$json_array->firstname,
									'user_image'=>$json_array->image,
									'user_email'=>$json_array->emailid,
									'user_device_type'=>$json_array->devicetype,
									'user_device_id'=>$json_array->deviceid,
									'user_device_token'=>$json_array->devicetoken,
									'user_token'=>$token
									
									);

		    			    $final_output['status'] = 'success';
		    				$final_output['message'] = 'Verify OTP.';
		    				$final_output['user_id'] = $insert; 
	        		}
	        		else
		    		{
		    				$final_output['status'] = 'failed'; 
						 	$final_output['message'] = some_error;
		    		}
	    		}	
	    	}

	    }
	    else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

  

}	