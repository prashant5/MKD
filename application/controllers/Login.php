<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if($userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		
	}
	
	public function index()
	{ 
		if(isset($_POST['submit']))
		{   
			     $username = $this->input->post ('username');
				 $password = $this->input->post('password');
				 $AdminData = $this->common_model->common_getRow('user',array('emailid'=>$username,'password'=>sha1($password)));

				if (!empty($AdminData)) 
				{
					if($AdminData->status == 0)
					{ 
						$this->session->set_flashdata('msg' ,'Your Account Has Been Suspended for Security Precaution.');	
				  		redirect(base_url('login'));exit;
					}

					$this->session->set_userdata ( array (
							'admin_id'   => $AdminData->id,
							'username'   => $AdminData->firstname,
							'level'   => $getinfo->Level,
							'status'=>$AdminData->status
							));

					
					redirect(base_url().'dashboard');
				}
				else
				{
				  $this->session->set_flashdata('msg' ,'Email or Password Not Matched.');	
				  redirect(base_url('login'));	
				}	
		}
		$this->load->view('admin/index');
	}
	
}
