<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <title>Kwikuts</title>

    <link href="<?php echo base_url();?>theme/css/materialize.css" rel="stylesheet">
    <link href="<?php echo base_url();?>theme/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/css/simplelightbox.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="<?php echo base_url();?>template/assets/global/css/parsley.css" rel="stylesheet">  

    <link href="<?php echo base_url();?>theme/css/jquery-clockpicker.css" rel="stylesheet">
    <link href="<?php echo base_url();?>theme/css/bootstrap-clockpicker.css" rel="stylesheet">

</head>
<body class="form-page">
    <section class="joinus-page">
        <a href="javascript:window.history.go(-1);" class="back-home">
            <i class="fa fa-arrow-left"></i>
        </a>
        <div class="joinus-form appointment-form barber-form">
            <h3 class="title">Book an Appointment</h3>

            <?php 
            if ($this->session->flashdata('success')) { 
            echo "<div class='alert alert-success' style='margin-left:38%;'>", $this->session->flashdata('success') ,"</div>";
            }else if($this->session->flashdata('failed')){
            echo "<div class='alert alert-danger' style='margin-left:38%;'>", $this->session->flashdata('failed') ,"</div>";
            } 
           ?>

            <form action="<?php echo base_url('home/make_appointment');?>" method="post" data-parsley-validate='' id="form11">
                <div class="join-form">
                    <div class="input-field">
                        <i class="fa fa-user"></i>
                        <input type="text" name="fname" class="validate" data-parsley-error-message="First Name is required" required="">
                        <label for="first_name">First Name</label>
                    </div>
                    <div class="input-field">
                        <i class="fa fa-envelope-o"></i>
                        <input type="text" name="email" class="validate" data-parsley-type="email" data-parsley-error-message="Email is required" required="">
                        <label for="first_name">Email</label>
                    </div>
                    <div class="input-field">
                        <i class="fa fa-phone"></i>
                        <input type="text" name="contact_no" class="validate" data-parsley-type="digits" data-parsley-error-message="Contact No is required" required="">
                        <label for="first_name">Contact</label>
                    </div>
                   <div class="input-field">
                        <i class="fa fa-calendar"></i>
                        <input type="text" value="" name="booking_date" readonly="readonly" id="date1" data-parsley-required-message="Date is required" class="validate" required="">
                        <label for="first_name">Date</label>
                    </div>

                    <div class="input-field">
                       <i class="fa fa-clock-o"></i>
                            <input type="text" name="booking_time" readonly="readonly" class="validate clockpicker" data-parsley-required-message="Time is required" data-autoclose="true" required>
                         <label for="first_name">Time</label>
                    </div>

                    <div class="input-field">
                        <i class="fa fa-map-marker"></i>
                        <input type="text" name="address" class="validate" data-parsley-error-message="Address is required" required="">
                        <label for="first_name">Address</label>
                    </div>
                    <div class="form-services">
                        <input type="checkbox" name="service[]" data-parsley-maxcheck="1" id="hair-cut" value="Haircut" data-parsley-error-message="Please select anyone of the listed services" required=""/>
                        <label for="hair-cut">Haircut</label>
                        <input type="checkbox" name="service[]" data-parsley-maxcheck="1" data-parsley-error-message="Please select anyone of the listed services" id="beard-trimming" value="Beard Trim" required=""/>
                        <label for="beard-trimming">Beard Trim</label>
                         <input type="checkbox" data-parsley-maxcheck="1" name="service[]" data-parsley-error-message="Please select anyone of the listed services" id="Haircut-Beard-Trim" value="Haircut-Beard-Trim" required=""/>
                        <label for="Haircut-Beard-Trim">Haircut & Beard Trim</label>
                    </div>
                    <button id="submit" type="submit" name="submit" class="btn waves-effect waves-light">Book</button>
                </div>
            </form>
        </div>
    </section>


    <style>
        .ui-datepicker-month,.ui-datepicker-year{
            display: inline-block;
            height: 25px;
            padding: 0 10px;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js "></script>
    <script src="<?php echo base_url()?>template/assets/global/js/parsley.min.js"></script>
    <script src="<?php echo base_url()?>theme/js/jquery-clockpicker.min.js"></script>
    
    
    <script src="<?php echo base_url();?>theme/js/materialize.min.js "></script>
    <script src="<?php echo base_url();?>theme/js/simple-lightbox.min.js "></script>
    <script src="<?php echo base_url();?>theme/js/owl.carousel.min.js "></script>
    <script src="<?php echo base_url();?>theme/js/youtubepopup.js"></script>
    <script src="<?php echo base_url();?>theme/js/jquery.navScroll.min.js"></script>
    <script src="<?php echo base_url();?>theme/js/custom.js "></script>
    <!--<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>-->
    <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">-->
    <!--<script src="<?php //echo base_url()?>theme/js/jquery-clockpicker.min.js"></script>-->

    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  
    <script type="text/javascript">
  $('#form11').parsley();  
</script>
<script>
  $( function() {
    $( "#date1").datepicker({

       changeMonth: true,
       changeYear: true,
       dateFormat:'yy-mm-dd',
       minDate: 0

    });
  } );
  </script>
  <script type="text/javascript">
$('.clockpicker').clockpicker({
    twelvehour: true,
    placement: 'top',
    donetext: 'Done',
    afterHourSelect :''
});
</script>

 
</body>

</html>