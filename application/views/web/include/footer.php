<footer>
<div class="footer-logo center">
            <img src="<?php echo base_url();?>theme/images/logo.png" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col m3 s12">
                    <div class="f-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li>
                                <a href="<?php echo base_url('terms-condition')?>" title="Terms & Conditions">Terms & Conditions</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('supply-services')?>" title="Terms & Conditions">T & C for Supply services</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('privacy-policy')?>" title="Privacy Policy">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('faq')?>" title="FAQ">FAQ</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col m6 s12 center">
                    <div class="download-btn">
                        <a href="#" title="App Store" class="waves-effect waves-light">
                            <i class="fa fa-apple"></i> App Store</a>

                        <a href="#" title="Play Store" class="waves-effect waves-light">
                            <i class="fa fa-play"></i> Play Store</a>
                    </div>

                    <ul class="social-btns">
                        <li>
                            <a href="#" title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Linkedin">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col m3 s12">
                    <div class="f-links right-align">
                        <h4>Working Hours</h4>
                        <ul>
                            <li>
                                Monday to Sunday
                            </li>
                            <li>
                                8 AM - 11 PM
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col m12 s12 center">
                    <p class="copyright">© <?php echo date('Y'); ?> kwikuts All rights reserved.</p>
                </div>
            </div>
        </div>
</footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js "></script>
    <script src="<?php echo base_url();?>theme/js/materialize.min.js "></script>
    <script src="<?php echo base_url();?>theme/js/simple-lightbox.min.js "></script>
    <script src="<?php echo base_url();?>theme/js/owl.carousel.min.js "></script>
    <script src="<?php echo base_url();?>theme/js/youtubepopup.js"></script>
    <script src="<?php echo base_url();?>theme/js/jquery.navScroll.min.js"></script>
    <script src="<?php echo base_url();?>theme/js/custom.js "></script>        