
    <link href="<?php echo base_url();?>theme/css/materialize.css" rel="stylesheet">
    <link href="<?php echo base_url();?>theme/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/css/simplelightbox.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="<?php echo base_url();?>theme/images/favicon.png">