<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Rating Review</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-star"></i>Rating & Review</div>
                                    <div class="actions">
                                          <a title="click here to Delete" type="button" id="deactivate_records" class="btn green pull-right">Delete<i class="fa fa-trash" aria-hidden="true"></i></a>   
                                    </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">

                                        <thead>
                                            <tr>
                                                <th><center>&nbsp;<input type="checkbox" id="select_all"></center></th>
                                                <th><center>Rating</center></th>
                                                <th><center>Review</center></th>
                                                <th><center>By User</center></th>
                                                <th><center>To Barber</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center></center></th> 
                                                <th><center>Rating</center></th>
                                                <th><center>Review</center></th>
                                                <th><center>By User</center></th>
                                                <th><center>To Barber</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($rating_data))
                                        { 
                                            foreach($rating_data as $key)
                                            { $date = substr($key->create_date,0,10);
                                              ?>
                                            <tr id="xxx<?php echo $key->rating_id;?>">
                                                <td><center><input type="checkbox" class="emp_checkbox" data-emp-id="<?php echo $key->rating_id; ?>"></center>
                                                </td>  
                                                <td><center>
                                                <?php 
                                                  $input = $key->rating;
                                                  if (strpos($input,'.') !== false) {

                                                      $num = (int)$input;
                                                      for($i=0;$i<$num;$i++)
                                                      {
                                                         echo '<i class="fa fa-star" aria-hidden="true"></i>&nbsp';
                                                      }
                                                      echo '<i class="fa fa-star-half" aria-hidden="true"></i>';    
                                                      }else {
                                                      for($i=0;$i<$input;$i++)
                                                      {
                                                         echo '<i class="fa fa-star" aria-hidden="true"></i>&nbsp';
                                                      }
                                                    }
                                                   ?>
                                                </center></td> 
                                                <td><center><?php echo $key->review;?></center></td>
                                                <td><center>
                                                   <?php $userdata = $this->common_model->common_getRow('barber_user',array('user_id'=>$key->user_id,'user_type'=>1)); 

                                                 $username = '';
                                                  if(!empty($userdata))
                                                  {
                                                     $username =  $userdata->user_name;
                                                  }
                                                  echo $username;
                                                  ?>
                                                </center></td>
                                                <td><center>
                                                    
                                                  <?php $barber = $this->common_model->common_getRow('barber_user',array('user_id'=>$key->barber_id,'user_type'=>2)); 

                                                 $name = '';
                                                  if(!empty($barber))
                                                  {
                                                     $name =  $barber->user_name;
                                                  }
                                                  echo $name;
                                                  ?>
                                                </center></td>
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class=""></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
             <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
             </a>
        </div>
       <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>
<script type="text/javascript">
$('#select_all').on('click', function(e) {
if($(this).is(':checked',true)) {
$(".emp_checkbox").prop('checked', true);
}
else {
$(".emp_checkbox").prop('checked',false);
}
// set all checked checkbox count
$("#select_count").html($("input.emp_checkbox:checked").length+" Selected");
});
// set particular checked checkbox count
$(".emp_checkbox").on('click', function(e) {
$("#select_count").html($("input.emp_checkbox:checked").length+" Selected");
});
</script>

<script>
//delete selected records
$('#deactivate_records').on('click', function(e) {
var employee = [];
$(".emp_checkbox:checked").each(function() { 
employee.push($(this).data('emp-id'));
});
if(employee.length <=0) { alert("Please select minimum one record."); } else { WRN_PROFILE_DELETE = "Are you sure you want to Delete "+(employee.length>1?"these":"this")+" row?";
var checked = confirm(WRN_PROFILE_DELETE);
if(checked == true) {
var selected_values = employee.join(",");
$.ajax({
type: "POST",
url: "<?php echo base_url('review/delete_rating')?>",
cache:false,
data: 'rating_id='+selected_values,
success: function(response) {
var emp_ids = response.split(",");

for (var i=0; i < emp_ids.length; i++)
{ 
    var str = $.trim(emp_ids[i]);
    var idz = "xxx"+str;
    $("#"+idz).hide();
}

        }

     });

   }
 }
});

</script>






     


