<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Barber</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('test/captcha')?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" placeholder="Barber Name" name="b_name" class="form-control" data-parsley-required-message="Barber Name is required" data-parsley-pattern="^[A-Za-z ]*$" />
                             <?php echo form_error('b_name', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Image<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="file"  name="image" id="inputfile" class="form-control" />
                            <?php if($this->session->flashdata('error_pic')){ echo "<div style='color:red;list-style-type: none;font-size: 0.9em;line-height: 0.9em;'>",$this->session->flashdata('error_pic'),"</div>"; }?>
                        </div>
                        <span id="file411" style="margin-left: 27%;list-style-type: none;font-size: 0.9em; line-height: 0.9em;"></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Email<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="text" id="email" placeholder="Barber Email" name="email" class="form-control" data-parsley-type="email" data-parsley-required-message="Email is required" />
                            <div id="err" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></div>
                            <?php echo form_error('email', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">contact No.<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="text"  name="contact_no" placeholder="Conatact No" id="mobile_no" class="form-control" data-parsley-type="digits" data-parsley-required-message="contact no is required" />
                             <div id="err1" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></div>
                            <?php echo form_error('email', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address<span class="required" required> * </span></label>
                        <div class="col-md-9">
                            <input type="text" name="address" placeholder="Address" class="form-control" data-parsley-required-message="Address is required"/>
                            <?php echo form_error('address', "<span class='error'>", "</span>"); ?>
                            <?php if($this->session->flashdata('invalid_address')){ echo "<div style='color:red;list-style-type: none;font-size: 0.9em;line-height: 0.9em;'>",$this->session->flashdata('invalid_address'),"</div>"; }?>
                        </div>
                    </div>
                     <div class="form-group">

                      <label class="control-label col-md-3"></label>
                      <div class="col-md-9">
                          <div class="g-recaptcha" data-sitekey="6LekRz4UAAAAAB4goP2SKgQxlcWrNFvIiNeseZH_" required></div>
                          
                      </div>
                     </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" name="submit" id="submit" value="Submit" >
                            <a href="<?php echo base_url()?>barber/add_barber"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>

            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
     <script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<!-- <script>
   $("#submit").click(function() {
    var fileExtension = ['jpeg', 'jpg', 'png'];
    var inputfile =  $("#inputfile").val();

   if(inputfile == '') 
   {
      $("#file411").html('Image is required').css("color", "red");
       setTimeout(function(){ 
        $('#file411').html('');
        },4000);
      return false;
   }
   else if(inputfile != '' && $.inArray($("#inputfile").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        
            //alert("Only formats are allowed : "+fileExtension.join(', '));
        $("#file411").html('Only formats are allowed :' +fileExtension.join(', ')).css("color", "red");
        setTimeout(function(){ 
        $('#file411').html('');
        },4000);
        return false;
    }


    return true;
  });
 </script> -->
    </body>

</html>