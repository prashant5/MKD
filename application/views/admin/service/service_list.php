<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Barber|Service </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container"> 
             <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-star"></i>Services</div>
                                    <div class="actions">
                                           
                                    </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Service Name</center></th>
                                                <th><center>Price</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Service Name</center></th>
                                                <th><center>Price</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($service_data))
                                        {   $i = 0;
                                            foreach($service_data as $key)
                                            { $i++; ?>
                                            <tr id="xxx<?php echo $key->service_id;?>">
                                                <td><center><?php echo $i;?></center></td>   
                                                <td><center><?php echo $key->service_name;?></center></td>
                                                <td><center><span id="price<?php echo $key->service_id; ?>"><?php echo '£ '.$key->service_price;?></span></center></td>
                                                <td><center><span id="price1<?php echo $key->service_id;?>"><a class="btn green btn-outline sbold aaa" data-price="<?php echo $key->service_price; ?>" data-id="<?php echo $key->service_id; ?>" data-toggle="modal" href="#basic">Update Price</a></span></center></td>
                                            </tr>
                                            <?php  
                                            } }
                                          else
                                          {?>
                                         <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                         </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">Update Price</h4>
                      </div>
                     <!--  <form method="post">  -->
                      <input type="hidden" name="service_id" id="service_id" value="">
                      <div class="modal-body"> 
                      <div class="form-group" style="margin-left: 65px;">
                           <div class="col-md-8">
                               <input type="text" id="setval" class="form-control" name="price" value="">
                           </div>
                           <div class="col-md-4">
                               <!-- <button type="button" class="btn green">Update</button> -->
                               <input type="submit" name="submit" id="submit" class="btn green" value="Update">
                           </div>
                           <span id="err" style="margin-left: 18px;list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></span>
                      </div>
                      </div>
                    <!--   </form> -->
                      <div class="modal-footer">
                          <!-- <button type="button" class="btn dark btn-outline" data-dismiss="modal"></button> -->
                         <!--  <button type="button" class="btn green">Save changes</button> -->
                      </div>
                  </div>
                  <!-- /.modal-content -->
              </div>
            </div>
        </div>
      <?php $this->load->view("admin/footer");?>
    </body>
</html>
<script>
    $(document).on("click", ".aaa", function () {  
     var price = $(this).attr('data-price');  
   
     var id = $(this).attr('data-id');  

     $('#setval').val(price);
     $('#service_id').val(id);
});

    $(document).ready(function(){
      $("#submit").click(function(){ 

          var price  = $("#setval").val();
          var service_id = $('#service_id').val();
          if(price == '')
          {
              $("#err").html('Price Required.').css('color','red');
               setTimeout(function(){ 
                $('#err').html('');
                },4000);
                return false;
          }

          $.ajax({
                    type:'POST',
                    url: '<?php echo base_url('services/update_price')?>',
                    data: "price="+price+"&service_id="+service_id,
                    dataType: 'json',
                    success: function(data){ 

                        $("#price"+service_id).html(data.service_price);
                        $("#price1"+service_id).html('<a class="btn green btn-outline sbold aaa" data-price="'+data.service_price+'" data-id= "'+data.service_id+'" data-toggle="modal" href="#basic">Update Price</a>');

                        $('#basic').modal('hide');
                    },
        
                });
       });

    });
</script>






     


