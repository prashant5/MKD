<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Admin|User</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"></div>
        <div class="page-container"> 
             <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Admin User
                                    </div>
                                    <div class="actions">
                                    <a href="<?php echo base_url()?>admin_user/add_admin"><button type="button" class="btn green delete pull-right"><i class="fa fa-plus"></i><span> ADD ADMIN</span></button></a>

                                  </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Image</center></th>
                                                <th><center>Name</center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Status</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>Image</center></th>
                                                <th><center>Name</center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Status</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($Admin_user))
                                        {   $i = 0;
                                            foreach($Admin_user as $key)
                                            { $i++; ?>
                                             <tr id="xxx">
                                                <td><center><?php echo $i;?></center></td>   
                                                <td><center><?php if($key->image){ $image = $key->image;}else { $image = 'default-medium.png'; } ?>
                                                <img src="<?php echo base_url('uploads/admin_image1/'.$image); ?>" width="60px" height="60px" class="img-circle">   
                                              </center></td>
                                                <td><center><?php echo $key->name;?></center></td>
                                                <td><center><?php echo $key->email;?></center></td>
                                                <td><select class="form-control" name="status" onchange="changestatus(this.value,'<?php echo $key->admin_id;?>')">
                                                  <option value="1"<?php if($key->status=='1') echo 'selected';?>>Inactive</option>    
                                                  <option value="0"<?php if($key->status=='0') echo 'selected';?>>Active</option>      
                                                </select></td>
                                            </tr>
                                            <?php  
                                            } }
                                          else
                                          {?>
                                         <tr class="even pointer">
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ><center><?php echo "Record not found";?></center></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                         </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">Update Price</h4>
                      </div>
                     <!--  <form method="post">  -->
                      <input type="hidden" name="service_id" id="service_id" value="">
                      <div class="modal-body"> 
                      <div class="form-group" style="margin-left: 65px;">
                           <div class="col-md-8">
                               <input type="text" id="setval" class="form-control" name="price" value="">
                           </div>
                           <div class="col-md-4">
                               <!-- <button type="button" class="btn green">Update</button> -->
                               <input type="submit" name="submit" id="submit" class="btn green" value="Update">
                           </div>
                           <span id="err" style="margin-left: 18px;list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></span>
                      </div>
                      </div>
                    <!-- </form> -->
                      <div class="modal-footer">
                         
                      </div>
                  </div>
              </div>
            </div>
        </div>
      <?php $this->load->view("admin/footer");?>
    </body>
<script type="text/javascript">
  function changestatus(status,id)
  {  
      var str = "id="+id+"&status="+status;
      var r = confirm('Are you really want to change status?');
      if(r==true)
      {
          $.ajax({
            type:"POST",
             url:"<?php echo base_url('admin_user/change_status')?>/",
             data:str,
             success:function(data)
             {   
                 if(data==1000)
                 {
                      location.reload();
                 }
             }
          });
      }
  }
</script>
</html>