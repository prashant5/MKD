<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1');?>
        <div class="clearfix"> </div>
        <div class="page-container">
           <?php $this->load->view('admin/new_sidebar1');?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Category</div>
                                            </div>
        <div class="portlet-body form">
        <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Category Name<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Category Name" name="category_name" class="form-control" data-parsley-required-message="Product Name is required" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Sequence<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Sequence" data-parsley-type="digits" name="sequence" class="form-control" data-parsley-required-message="Product Quantity is required" required/>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required" required> * </span></label>
                        <div class="col-md-7">
                            <textarea rows="5" cols="10" class="form-control" placeholder="Description" data-parsley-required-message="Description is required" name="description" required=""></textarea>
                        </div>
                    </div> -->
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit" >
                            <a href="javascript:history.go(-1)"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
            </div>
                </div>
                    </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>
    </body>
    <script type="text/javascript">
    $(document).ready(function(){
        $('body').on("click", '#submit', function() {
        /* var paraid=$('#parameterId').val();*/
        //var name=$('#name').val();
        
        var base_url = '<?php echo base_url(); ?>';
        url ="product/add_category";
        $.ajax({
        type:"POST",
        url:base_url + url,
        data:data,
        contentType: false,
        cache: false,
        processData: false,
        success: function(result){
            if(result ==true){
              $('#comassage').html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Error!</strong> try again.</div>') 
              setTimeout(function(){ 
                $('#comassage').html('');
              },2000);
              return false;
            }else{
                $('#addReviewForm').trigger('reset');
                $('#comassage').html('<div class="alert alert-success  alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Successfully</strong> review send successfully and admin will aprove soon.</div>')
                setTimeout(function(){ 
                $('#comassage').html('');
                $('.close').click();
                },3000);
              return true;
            }
          }
        });
        });
    });
    </script>

</html>