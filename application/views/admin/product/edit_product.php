<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
           <?php $this->load->view('admin/new_sidebar1'); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Product</div>
                                            </div>
        <div class="portlet-body form">
        <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <?php if(!empty($edit_product)) {?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Name<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Product Name" name="product_name" class="form-control" data-parsley-required-message="Product Name is required" value ="<?php echo $edit_product->product_name;?>" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Quantity<span class="required">*</span></label>
                        <div class="col-md-7">
                           
                            <input type="text" placeholder="Product Quantity" name="quantity" class="form-control" data-parsley-required-message="Product Quantity is required"  value ="<?php echo $edit_product->quantity;?>" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Price<span class="required"> * </span></label>
                        <div class="col-md-7">
                             <div class="input-group">
                           <span class="input-group-addon">
                              £
                           </span>
                            <input type="text"  name="price" placeholder="Product Price" class="form-control" data-parsley-type="digits" data-parsley-required-message="Product Price" required  value="<?php echo $edit_product->price;?>"/></div>
                             <div id="err1" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required" required> * </span></label>
                        <div class="col-md-7">
                            <textarea rows="5" cols="10" class="form-control" placeholder="Description" data-parsley-required-message="Description is required" name="description" required=""><?php echo $edit_product->description;?> </textarea>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit">
                            <a href="javascript:history.go(-1)"><button type="button" class="btn default">Cancel</button>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            <?php } ?>
        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>
    </body>

</html>