 <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200"     style="padding-top: 20px">
                           
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <li class="nav-item start <?php if($this->uri->segment(1)=='dashboard'){ echo 'active';} ?>">
                                <a href="<?php echo base_url('dashboard');?>" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(1)=='employe'){ echo 'active';} ?>">
                                 <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span class="title">Employe</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(2)=='add_employe'){ echo 'active';} ?>">
                                        <a href="<?php echo base_url('employe/add_employe');?>" class="nav-link">
                                            <span class="title">Add Employe</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                             <li class="nav-item <?php if($this->uri->segment(1)=='user'){ echo 'active';} ?>">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span class="title">USER</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="<?php echo base_url('user/verified')?>" class="nav-link ">
                                            <span class="title">Verified User</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?php echo base_url('user/unverified')?>" class="nav-link ">
                                            <span class="title">Unverified User</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?php echo base_url('user/block_user')?>" class="nav-link ">
                                            <span class="title">Block User</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(1)=='user'){ echo 'active';} ?>">
                                 <a href="javascript:;" class="nav-link nav-toggle">
                                   <i class="fa fa-bars" aria-hidden="true"></i>
                                    <span class="title">Category</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(2)=='category'){ echo 'active';} ?>">
                                        <a href="<?php echo base_url('product/category');?>" class="nav-link">
                                            <span class="title">Category</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(1)=='product'){ echo 'active';} ?>">
                                 <a href="<?php echo base_url('product');?>">
                                   <i class="fa fa-bars" aria-hidden="true"></i>
                                    <span class="title">Product</span>
                                    <!-- <span class="arrow"></span> -->
                                </a>
                                <!-- <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(2)=='category'){ echo 'active';} ?>">
                                        <a href="<?php echo base_url('product/category');?>" class="nav-link">
                                            <span class="title">Category</span>
                                        </a>
                                    </li>
                                </ul> -->
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(1)=='menu' && $this->uri->segment(2)!='subscription'){ echo 'active';} ?>">
                                 <a href="<?php echo base_url('menu');?>">
                                   <i class="fa fa-bars" aria-hidden="true"></i>
                                    <span class="title">Manage Menu</span>
                                    <!-- <span class="arrow"></span> -->
                                </a>
                                <!-- <ul class="sub-menu">
                                    <li class="nav-item <?php if($this->uri->segment(2)=='category'){ echo 'active';} ?>">
                                        <a href="<?php echo base_url('product/category');?>" class="nav-link">
                                            <span class="title">Category</span>
                                        </a>
                                    </li>
                                </ul> -->
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(2)=='subscription'){ echo 'active';} ?>">
                                 <a href="<?php echo base_url('menu/subscription');?>">
                                   <i class="fa fa-bars" aria-hidden="true"></i>
                                    <span class="title">Subscription plan</span>
                                    <!-- <span class="arrow"></span> -->
                                </a>
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(1)=='common' && $this->uri->segment(2)=='add_banner'){ echo 'active';} ?>">
                                 <a href="<?php echo base_url('common/add_banner');?>">
                                   <i class="fa fa-bars" aria-hidden="true"></i>
                                    <span class="title">Banner</span>
                                    <!-- <span class="arrow"></span> -->
                                </a>
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(2)=='add_validity_plan'){ echo 'active';} ?>">
                                 <a href="<?php echo base_url('common/validity_plan');?>">
                                   <i class="fa fa-bars" aria-hidden="true"></i>
                                    <span class="title">Validity Plan</span>
                                    <!-- <span class="arrow"></span> -->
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>