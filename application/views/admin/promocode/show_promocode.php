<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Barber|Promocode</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container"> 
             <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Promocode
                                    </div>
                                    <div class="actions">
                                    <a href="<?php echo base_url()?>promocode/add"><button type="button" class="btn green delete pull-right"><i class="fa fa-plus"></i><span>Add Promocode</span></button></a>       
                                    </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>title</center></th>
                                                <th><center>Promocode</center></th>
                                                <th><center>Start Date</center></th>
                                                <th><center>End Date</center></th>
                                                <th><center>Discount(%)</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>title</center></th>
                                                <th><center>Promocode</center></th>
                                                <th><center>Start Date</center></th>
                                                <th><center>End Date</center></th>
                                                <th><center>Discount(%)</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($promocode))
                                        {   $i = 0;
                                            foreach($promocode as $key)
                                            { $i++; ?>
                                             <tr id="xxx<?php echo $key->promocode_id;?>">
                                                <td><center><?php echo $i;?></center></td>   
                                                <td><center><?php echo $key->title;?></center></td>
                                                <td><center><?php echo $key->promocode;?></center></td>
                                                <td><center><?php echo substr($key->start_date,0,10);?></center></td>
                                                <td><center><?php echo substr($key->end_date,0,10);?></center></td>
                                                <td><center><?php echo $key->discount ;?></center></td>
                                                <td><center>
                                                  <select class="form-control" onchange="changestatus('<?php echo $key->promocode_id; ?>',this.value)">
                                                        <option value="1"<?php if($key->admin_status == 1){ echo 'selected';} ?>>Active</option>
                                                        <option value="0"<?php if($key->admin_status == 0){ echo 'selected';} ?>>Inactive</option>
                                                 </select>
                                                </center></td>
                                            </tr>
                                            <?php  
                                            } }
                                          else
                                          {?>
                                         <tr class="even pointer">
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ><center><?php echo "Record not found";?></center></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                         </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">Update Price</h4>
                      </div>
                     <!--  <form method="post">  -->
                      <input type="hidden" name="service_id" id="service_id" value="">
                      <div class="modal-body"> 
                      <div class="form-group" style="margin-left: 65px;">
                           <div class="col-md-8">
                               <input type="text" id="setval" class="form-control" name="price" value="">
                           </div>
                           <div class="col-md-4">
                               <!-- <button type="button" class="btn green">Update</button> -->
                               <input type="submit" name="submit" id="submit" class="btn green" value="Update">
                           </div>
                           <span id="err" style="margin-left: 18px;list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></span>
                      </div>
                      </div>
                    <!--   </form> -->
                      <div class="modal-footer">
                         
                      </div>
                  </div>
              </div>
            </div>
        </div>
      <?php $this->load->view("admin/footer");?>
    </body>
<script type="text/javascript">
  function changestatus(id,status)
  {  
      var str = "id="+id+"&status="+status;

      alert(str);
     
      var r = confirm('Are you really want to change status?');
      if(r==true)
      {
          $.ajax({
            type:"POST",
             url:"<?php echo base_url('promocode/change_status')?>/",
             data:str,
             success:function(data)
             {   
                 if(data==1000)
                 {
                      location.reload();
                 }
             }
          });
      }
  }
</script>
</html>