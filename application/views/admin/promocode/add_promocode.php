<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Promocode</div>
                                               
                                            </div>
         <div class="portlet-body form">
          <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
          ?>
          <form  name="myform" action="" id="form11" class="form-horizontal form-row-seperated" method="post" data-parsley-validate='' >
           <input type="hidden" name="length" value="6">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Title<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Title" name="title" class="form-control" data-parsley-required-message="Title is required" required/>
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label col-md-3">Promo Code<span class="required"> * </span></label>
                        <div class="col-md-7">
                        <input name="promocode" onkeydown="upperCaseF(this)" maxlength="6" class="form-control" type="text" placeholder="Promo Code" data-parsley-required-message="Promo Code is required" id="promo_code" required="">
                        <span style="margin-left: 63%;"><span class="required">*</span>Please do not start with Zero(0)</span>
                        <span  class="input-group-btn refresh-btn" style="position: absolute;top: -4px;right: 0;"><button type="button" onClick="generate();" class="btn btn-default btn-lg getNewPass" style="padding: 10px;"><span class="fa fa-refresh"></span></button></span>
                         <div id="err" style="list-style-type: none;font-size: 1.9em; line-height: 0.9em;"></div>
                        </div>
                   </div>
                  <div class="form-group" > 
                        <label class="control-label col-md-3">Start Date<span class="required"> * </span></label>
                        <div class="col-md-2">
                           <input type="text" value="<?php echo date('Y-m-d');?>" name="start_date" readonly="readonly" id="date1" data-parsley-required-message="Start date is required" class="form-control" required>
                        </div>
                        <label class="control-label col-md-3">End Date<span class="required"> * </span></label>
                        <div class="col-md-2">
                           <input type="name" name="end_date" id="date2" readonly="readonly"  data-parsley-required-message="End date is required" class="form-control" required value="">
                        </div>
                    </div>
                    <span id="error" style="list-style-type:none;font-size:0.9em;line-height:0.9em;margin-left:27%;"></span>
                    <div class="form-group">
                        <label class="control-label col-md-3">Discount (%)<span class="required"> * </span></label>
                        <div class="col-md-7">
                           
                            <input type="text" min="1" max="100"  data-parsley-required-message="Discount is required" placeholder="Discount" name="discount"  class="form-control"  required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Max Discount<span class="required"> * </span></label>
                        <div class="col-md-7">
                           
                            <input type="text" data-parsley-required-message="Max Discount is required" placeholder="Max Discount" name="max_discount"  class="form-control"  required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required"> * </span></label>
                        <div class="col-md-7">
                             <textarea class="form-control" placeholder="Description" data-parsley-required-message="Description is required" name="description" rows="5" cols="5" required></textarea>
                            <div id="err"></div>  <?php echo form_error('name',"<div id='error'>", "</div>") ?>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit" >
                            <a href="javascript:window.history.go(-1);"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
               
            </div>
        </div>
       
     <?php $this->load->view('admin/footer'); ?>
     <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

<script type="text/javascript">
  $('#form11').parsley();  

  function upperCaseF(a){
    setTimeout(function(){
        a.value = a.value.toUpperCase();
    }, 1);
}

function randomPassword(length) {
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

function generate() {
    myform.promocode.value = randomPassword(myform.length.value);
}

$(document).ready(function(){
    $("#submit").click(function(){ 
        var promo_code  = $("#promo_code").val();

        var str = 0;
             $.ajax({
                    type:'POST',
                    url: '<?php echo base_url();?>promocode/check_promocode',
                    data: "promo_code="+promo_code,
                    async: false,
                    success: function(data){
                        if(data==10000)
                        {
                          str = 1;  
                          $("#err").html('Promo Code Already Exists').css('color','red');
                           setTimeout(function(){ 
                           $('#err').html('');
                            },4000);
                        }
                        else
                        { 
                          $("#err").html('');
                        }
                    },
                });
             if(str == 1)
             {
                return false;
             }   
        });
  });
</script>
<script type="text/javascript">
 $( function() {
    $( "#date1" ).datepicker({

       onSelect: function () {
        var end = $('#date2').datepicker('getDate');
        var start   = $('#date1').datepicker('getDate');

        if(end != null)
        { 
            if(start > end )
            {
              $("#error").html('Start date should be less from end date').css("color","red");
               setTimeout(function(){ 
                           $('#error').html('');
                            },4000);

              return false;
            }
            else if(start < end )
            { 
              $("#error").html().css();
              return true;
           }  
        }  
    },

       changeMonth: true,
       changeYear: true,
       dateFormat:'yy-mm-dd',
       minDate: 0 
    });
  });

 $("#date2").datepicker({
    onSelect: function () {
        var end = $('#date2').datepicker('getDate');
        var start   = $('#date1').datepicker('getDate');

        if(start > end )
        {
             $("#error").html('Start date should be less from end date').css("color","red");
             setTimeout(function(){ 
                           $('#error').html('');
                            },4000);

             return false;
        }
        else if(start < end )
        {
              $("#error").html().css();
              return true;
        }  
    },
       changeMonth: true,
       changeYear: true,
       dateFormat:'yy-mm-dd',
       minDate: 0 
});

</script>

  </body>

</html>