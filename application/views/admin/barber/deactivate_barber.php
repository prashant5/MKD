<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Deactivate | Barber</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
           <?php $this->load->view("admin/new_header1"); ?>
            <!-- END HEADER INNER -->
        </div>
     
        <div class="clearfix"></div>
      
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <?php $this->load->view("admin/new_sidebar1"); ?>
         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Deactivated Barber</div>
                                    <div class="actions">
                                          <a title="click here to Deactivate" type="button" id="deactivate_records" class="btn green pull-right">Active Barber<i class="fa fa-toggle-on" aria-hidden="true"></i></a>   
                                    </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>&nbsp;<input type="checkbox" id="select_all"></center></th>
                                                <th><center>Image</center></th>
                                                <th><center>Name </center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Mobile No.</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center></center></th> 
                                                <th><center>Image</center></th>
                                                <th><center>Name </center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Mobile No.</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($user_data))
                                        {
                                            foreach($user_data as $key)
                                            { $date = substr($key->create_date,0,10);
                                              ?>
                                            <tr id="xxx<?php echo $key->user_id;?>">
                                                <td><center><input type="checkbox" class="emp_checkbox" data-emp-id="<?php echo $key->user_id; ?>"></center></td>   
                                                    <td><center><?php if($key->user_image){ $image = $key->user_image;}else{ $image  = 'default-medium.png'; };?>
                                                      <img src="<?php echo base_url('uploads/barber_image/'.$image); ?>" width="60px" height="60px" class="img-circle">  
                                                    </center> </td>
                                                    <td><center><?php echo $key->user_name;?><br>
                                                        <span class="label label-sm label-success badge"><?php echo 'Date - '. $date;?></span> 
                                                    </center></td>
                                                    <td><center><?php echo $key->user_email;?></center></td>
                                                    <td><center><?php echo $key->user_mobile_num;?></center></td>
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class=""></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>
<script type="text/javascript">
$('#select_all').on('click', function(e) {
if($(this).is(':checked',true)) {
$(".emp_checkbox").prop('checked', true);
}
else {
$(".emp_checkbox").prop('checked',false);
}
// set all checked checkbox count
$("#select_count").html($("input.emp_checkbox:checked").length+" Selected");
});
// set particular checked checkbox count
$(".emp_checkbox").on('click', function(e) {
$("#select_count").html($("input.emp_checkbox:checked").length+" Selected");
});
</script>

<script>
    // delete selected records
$('#deactivate_records').on('click', function(e) {
var employee = [];
$(".emp_checkbox:checked").each(function() { 
employee.push($(this).data('emp-id'));
});
if(employee.length <=0) { alert("Please select minimum one row."); } else { WRN_PROFILE_DELETE = "Are you sure you want to Active "+(employee.length>1?"these":"this")+" row?";
var checked = confirm(WRN_PROFILE_DELETE);
if(checked == true) {
var selected_values = employee.join(",");
$.ajax({
type: "POST",
url: "<?php echo base_url('barber/activate_barber')?>",
cache:false,
data: 'user_id='+selected_values,
success: function(response) {
// remove deleted employee rows
var emp_ids = response.split(",");

    for (var i=0; i < emp_ids.length; i++)
    { 
        var str = $.trim(emp_ids[i]);
        var idz = "xxx"+str;
        $("#"+idz).hide();
    }

        }

     });

   }
 }
});

</script>



     


