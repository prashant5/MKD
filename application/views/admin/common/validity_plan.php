<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Menu</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
            <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Menu</div>
                                    <div class="actions">
                                       <a title="Add Barber" href="<?php echo base_url('common/add_validity_plan');?>" type="button" id="" class="btn white pull-right">Add validity plan<i class="fa fa-plus" aria-hidden="true"></i></a> 
                                    </div>    
                                </div>
                            </div>
                            <div class="portlet-body" >
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>Minimum Amount</center></th>
                                                <th><center>Maximum Amount</center></th>
                                                <th><center>Total Days</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th><center>Minimum Amount</center></th>
                                                <th><center>Maximum Amount</center></th>
                                                <th><center>Total Days</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($validity))
                                        {
                                            foreach($validity as $plan)
                                            { ?>
                                            <tr>
                                                  <td id="min<?php echo $plan->ValidityId; ?>"><center><?php echo $plan->MinAmount;?></center></td>
                                                  <td id='mintext<?php echo $plan->ValidityId; ?>' style='display:none'><input class='form-control' type='text' id='edtname<?php echo $plan->ValidityId; ?>' value='<?php echo $plan->MinAmount;?>'></td>
                                                  <td id="max<?php echo $plan->ValidityId; ?>"><center><?php echo $plan->MaxAmount;?></center></td>
                                                  <td id='maxtext<?php echo $plan->ValidityId; ?>' style='display:none'><input class='form-control' type='text' id='edtmaxname<?php echo $plan->ValidityId; ?>' value='<?php echo $plan->MaxAmount;?>'></td>
                                                  <td id="day<?php echo $plan->ValidityId; ?>"><center><?php echo $plan->ValidityDays;?></center></td>
                                                  <td id='daytext<?php echo $plan->ValidityId; ?>' style='display:none'><input class='form-control' type='text' id='edtdayname<?php echo $plan->ValidityId; ?>' value='<?php echo $plan->ValidityDays;?>'></td>
                                                  <td>
                                                    <a id='edit<?php echo $plan->ValidityId; ?>' onclick='editmain(<?php echo $plan->ValidityId; ?>);' class='btn btn-info' href='javascript:void(0);' title='Edit status' data-rel='tooltip'  >
                                                    <i class='fa fa-edit'></i> </a>

                                                     <a id='save<?php echo $plan->ValidityId; ?>' onclick='savemain(<?php echo $plan->ValidityId; ?>);'  href='javascript:void(0);' title='Save' data-rel='tooltip' style='cursor:pointer;display:none;' class='btn btn-info'  >
                                                    <i class='fa fa-check '></i></a>
                                                    </td>
                                            </tr>
                                                <?php  

                                            } 
                                        }
                                              else
                                              { ?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            <div id="menu_data"></div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
       
      <?php $this->load->view("admin/footer"); ?>
    </body>
<script type="text/javascript">
    function deletemain(data,depstatus)
    { 
        if(depstatus == 1)
        {
            var r = confirm('Are you really want to Inactive this Department ?');
        }else
        {
            var r = confirm('Are you really want to Active this Department ?');
        } 
        
        if(r==true)
        {
            $.ajax({
                url:'delete.php?status=1&id='+data+'&depstatus='+depstatus,
                success:function(data){
                   
                    if(data==1000)
                    {
                        if(depstatus == 1)
                        {
                            alert("Department Successfully Inactive");
                        }else
                        {
                            alert("Department Successfully Actvie");
                        }
                        window.location="show_department.php";
                    }
                }
            });
        }
    }

    function editmain(data)
    {
        $('#min'+data).hide();
        $('#max'+data).hide();
        $('#day'+data).hide();
        $('#edit'+data).hide();
        $('#mintext'+data).show();
        $('#maxtext'+data).show();
        $('#daytext'+data).show();
        $('#save'+data).show();
    }

    function savemain(id)
    {
        //document.getElementById('min'+id).innerHTML=$('#edtname'+id).val();
        //$('#txtname'+id).hide();
       
        $('#min'+id).show();
        $('#max'+id).show();
        $('#day'+id).show();
        $('#edit'+id).show();
        $('#mintext'+id).hide();
        $('#maxtext'+id).hide();
        $('#daytext'+id).hide();
        $('#save'+id).hide();
        var minval = $('#edtname'+id).val();
        var maxval = $('#edtmaxname'+id).val();
        var days = $('#edtdayname'+id).val();
        if(minval != '' && maxval !='' && days !='')
        {
            var data = "id="+id+"&minval="+minval+"&maxval="+maxval+"&day="+days;
            var base_url = '<?php echo base_url(); ?>';
            var url ="common/edit_validity_plan/";
            $.ajax({
                type:"POST",
                url: base_url+url,
                data:data,
                //dataType: "json",
                //contentType: "application/json; charset=utf-8",,
                cache: false,
                //processData: false,
                //beforeSend: function(){ alert(url); },
                success: function(result){
                    if(result == 1000){
                        window.location="validity_plan";
                    }else
                    {

                    }
                  }
            });
        }else
        {
            alert("Please fill all fields before save");
            window.location="validity_plan";
            //return false;   
        }

    }
    
    </script>
</html>
