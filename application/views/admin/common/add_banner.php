<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Add Banner</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
            <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="container">
                        <?php if($this->session->flashdata('error_pic')){?>
                            <div class="alert alert-danger">
                                <button class="close" data-close="alert"></button>
                                <span> <?php echo $this->session->flashdata('error_pic');?></span>
                            </div>
                        <?php }?>
                        <?php if($this->session->flashdata('success')){?>
                            <div class="alert alert-success">
                                <button class="close" data-close="alert"></button>
                                <span> <?php echo $this->session->flashdata('success');?></span>
                            </div>
                        <?php }?>
                        <div class="row">
                        <div class="col-md-6">                   
                            <p><?php echo $this->session->flashdata('statusMsg'); ?></p>
                            <form enctype="multipart/form-data" action="" method="post">
                                <div class="form-group">
                                    <label>Choose Images</label>
                                    <input type="file" width="100px" class="form-control" name="bannerFiles[]" multiple/>
                                </div>
                                <div class="form-group col-md-3">
                                    <input class="form-control btn btn-success" type="submit"  name="fileSubmit" value="UPLOAD"/>
                                </div>
                            </form>
                        </div>
                    </div>
                    <h4 style="color:">BANNER IMAGE LIST</h4> <br>   
                        <div class="row">
                            <ul class="gallery">
                                <?php
                                if(!empty($banerimage)): foreach($banerimage as $file): ?>
                                <span class="item">
                                    <img src="<?php echo base_url('uploads/banner/'.$file->BannerUrl); ?>" alt="" height="100px" width="100px" style="margin-bottom:1%">
                                    <!-- <p>Uploaded On <?php //echo date("j M Y",strtotime($file['created'])); ?></p> -->
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php endforeach; else: ?>
                                <!-- <p>Image(s) not found.....</p> -->
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
       
      <?php $this->load->view("admin/footer"); ?>
    </body>
</html>








     


