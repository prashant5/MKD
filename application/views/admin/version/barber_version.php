<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Barber Version</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?> 
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       <?php $this->load->view("admin/new_header1"); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
           <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-text-o"></i>Android Version</div>
                                             </div>
        <div class="portlet-body form">
           <?php 
           if($this->session->flashdata('success1'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success1'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed1'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url().'version/update_b'?>" class="form-horizontal form-row-seperated" method="post" id="form11" data-parsley-validate=''>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Min Version</label>
                        <div class="col-md-7">
                            <input type="number" id="min_android_version"  placeholder="Min Version" name="min_android_version" value="<?php echo $update_version->android_min_version;?>" min=0 class="form-control" data-parsley-type="integer" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Max Version</label>
                        <div class="col-md-7">
                            <input type="number" id="max_android_version"  placeholder="Max Version" name="max_android_version" value="<?php echo $update_version->android_max_version;?>" min=0 class="form-control" data-parsley-type="integer" required/>
                        </div>
                        <span id="ab1" style="color:red;margin-left:270px;list-style-type: none;font-size: 0.9em;line-height: 0.9em;"></span>

                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" name="submit1" class="btn green" value="Submit" id="submit">
                            &nbsp&nbsp&nbsp
                       </div>
                   </div>
                </div>   
            </form>
            <!-- END FORM-->
            </div>
        </div>
        <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-text-o"></i>IOS Version
            </div>
         </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
           <?php 
           if($this->session->flashdata('success2'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success2'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed2'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url().'version/update_b'?>" class="form-horizontal form-row-seperated" method="post" id="form111" name="form1" data-parsley-validate=''>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Min Version</label>
                        <div class="col-md-9">
                            <input type="number"  min=0 id="min_ios_vesion" placeholder="Min Version" name="min_ios_version" value="<?php echo $update_version->ios_min_version;?>" data-parsley-type="integer"  class="form-control" required/>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Max Version</label>
                        <div class="col-md-9">
                            <input type="number" id="max_ios_vesion" min=0  placeholder="Max Version" name="max_ios_version" value="<?php echo $update_version->ios_max_version;?>" data-parsley-type="integer" class="form-control" required/>
                        </div>
                        <span id="ab11" style="color:red;margin-left:270px;list-style-type: none;font-size: 0.9em;line-height: 0.9em;"></span>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" name="submit2" class="btn green" value="Submit" id="submit111">
                            &nbsp&nbsp&nbsp
                       </div>
                   </div>
                </div>   
            </form>
            <!-- END FORM-->
            </div>
        </div>

    </div>
</div>
    </div>
        </div>
            </div>
                </div>
                    </div>
                        </div>
      
        <?php $this->load->view("admin/footer"); ?>
       

<script type="text/javascript">
  $('#form11').parsley();

</script>

<script type="text/javascript">
  $('#form111').parsley();

</script>


<script>
function CheckDecimal(inputtxt)   
{   
var decimal=  /^[-+]?[0-9]+\.[0-9]+$/;   
if(inputtxt.value.match(decimal))   
{   
alert('Correct, try another...')  
return true;  
}  
else  
{   
alert('Wrong...!')  
return false;  
}  
}   

</script>

<script type="text/javascript">
    
     $("#submit").click(function() { 

       var android_min =  $("#min_android_version").val();
       var android_max =  $("#max_android_version").val();

           if(android_min > android_max)
           {
                $('#ab1').html('Min version should be less from max version.');
                setTimeout(function(){ 
                $('#ab1').html('');
                },4000);
                return false;
           }
           return true;
     });

</script>

<script type="text/javascript">
    
      $("#submit111").click(function() {  

       var ios_min =  $("#min_ios_vesion").val();
       var ios_max =  $("#max_ios_vesion").val();
          
           if(ios_min > ios_max)
           {
                $('#ab11').html('Min version should be less from max version.');
                setTimeout(function(){ 
                $('#ab11').html('');
                },4000);
                return false;
           }
           return true;
     });

</script>

</body>
</html>
