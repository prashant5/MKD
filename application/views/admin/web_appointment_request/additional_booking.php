<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Additional|Booking</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container"> 
             <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Appointment Request From App
                                    </div>
                                    <div class="actions">
                                    </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>User Name</center></th>
                                                <th><center>Services</center></th>
                                                <th><center>Products</center></th>
                                                <th><center>Barber Name</center></th>
                                                <th><center>Booking Date</center></th>
                                                <th><center>Booking Status</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>User Name</center></th>
                                                <th><center>Services</center></th>
                                                <th><center>Products</center></th>
                                                <th><center>Barber Name</center></th>
                                                <th><center>Booking Date</center></th>
                                                <th><center>Booking Status</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($additional_booking))
                                        {   $i = 0;
                                            foreach($additional_booking as $key)
                                            {   
                                              $i++; ?>
                                              <tr id="xxx">
                                                <td><center><?php echo $i;?></center></td>   
                                                <td><center><?php
                                                    $username = $this->common_model->common_getRow('barber_user',array('user_id'=>$key->user_id,'user_type'=>1));

                                                    if(!empty($username->user_name)){ echo $username->user_name;} else {   echo ''; }
                                                    ?></center></td>
                                                <td><center><?php
                                                        $loop = array();
                                                        if(!empty($key->service_id))
                                                        {
                                                           $services = json_decode($key->service_id);
                                                            for($j=0;$j< count($services);$j++)
                                                            {
                                                               $servicename = $this->common_model->common_getRow('barber_services',array('service_id'=>$services[$j]->service_id));

                                                                $loop[] = $servicename->service_name;
                                                            }
                                                            echo $arr = implode(',</br>',$loop);
                                                        }

                                                 ?></center></td>    
                                                <td><center>
                                                    <?php 
                                                       $pro_arr = array();
                                                        if(!empty($key->product_id))
                                                        {
                                                            $product = json_decode($key->product_id);
                                                            for($j=0;$j< count($product);$j++)
                                                            {
                                                               $productname = $this->common_model->common_getRow('barber_product',array('product_id'=>$product[$j]->product_id));

                                                                $pro_arr[] = $productname->product_name;
                                                            }
                                                            echo $pr = implode(',</br>',$pro_arr);
                                                        }
                                                    ?>
                                                </center></td>    
                                                <td><center><?php 
                                                    $barbername = $this->common_model->common_getRow('barber_user',array('user_id'=>$key->barber_id,'user_type'=>2));

                                                    if($barbername->user_name)
                                                    {
                                                        echo $barbername->user_name;  
                                                    }
                                                    else
                                                    {
                                                        echo '';
                                                    }    

                                                ?></center></td>
                                                <td><center><?php echo '<a href="javascript:;" class="btn btn-xs green"><i class="fa fa-calendar-plus-o"></i> '.$key->booking_date.'</a>' ;?></center></td>
                                                <td><center><?php if($key->booking_status == 0){ echo '<a href="javascript:;" class="btn btn-xs purple">Pending</a>';} else if($key->booking_status == 1){ echo '<a href="javascript:;" class="btn btn-xs yellow">Confirmed</a>
';} else if($key->booking_status == 2) { echo '<a href="javascript:;" class="btn btn-xs green">Completed</a>';} else if($key->booking_status == 3) { echo '<a href="javascript:;" class="btn btn-xs red">Cancelled</a>';} ?></center></td>

                                            </tr>
                                            <?php  
                                            } }
                                          else
                                          {?>
                                         <tr class="even pointer">
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ><center><?php echo "Record not found";?></center></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                         </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer");?>
    </body>
</html>