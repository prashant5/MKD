<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Appointment|Request</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container"> 
             <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Appointment Request
                                    </div>
                                    <div class="actions">
                                    </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>User Name</center></th>
                                                <th><center>User Email</center></th>
                                                <th><center>User Contact</center></th>
                                                <th><center>Booking Datetime</center></th>
                                                <th><center>Service</center></th>
                                                <th><center>Assign</center></th>
                                                <th><center>Assigned Barber</center></th>
                                                <th><center>Status</center></th>
                                                <th><center>Change Status</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>User Name</center></th>
                                                <th><center>User Email</center></th>
                                                <th><center>User Contact</center></th>
                                                <th><center>Booking Datetime</center></th>
                                                <th><center>Service</center></th>
                                                <th><center>Assign</center></th>
                                                <th><center>Assigned Barber</center></th>
                                                <th><center>Status</center></th>
                                                <th><center>Change Status</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($Appointment_request))
                                        {   $i = 0;
                                            foreach($Appointment_request as $key)
                                            { $i++; ?>
                                             <tr id="xxx">
                                                <td><center><?php echo $i;?></center></td>   
                                                <td><center><?php echo $key->user_name;?></center></td>
                                                <td><center><?php echo $key->user_email;?></center></td>
                                                <td><center><?php echo $key->user_contact;?></center></td>
                                                <td><center><?php echo '<a href="javascript:;" class="btn btn-xs green"><i class="fa fa-calendar-plus-o"></i> '.$key->booking_date.'</a>' ;?>
                                                <?php echo '<a style="margin-top:5px;"href="javascript:;" class="btn btn-xs green"><i class="fa fa-clock-o"></i>'.date('h:i A',strtotime($key->booking_time)).'</a>';?>
                                                </center></td>
                                                <td><center><?php echo $key->services_for;?></center></td>
                                                <td><center><a class="btn green btn-outline sbold aaa" data-toggle="modal" href="<?php echo base_url('Appointment_request/available_barber/'.$key->booking_id.'/w');?>">Assign barber</a></center></td>
                                                <td><center><?php if($key->dummy_barber_id == 0){ echo 'Not Yet';}else { 
                                                  $barber_info = $this->common_model->common_getRow('barber_user',array('user_id'=>$key->dummy_barber_id,'user_type'=>'2'));
                                                 
                                                  echo $barber_info->user_name;
                                                  } ?></center></td>
                                                <td><center><?php if($key->admin_status == 0){ echo '<a href="javascript:;" class="btn btn-xs purple">Pending</a>';} else if($key->admin_status == 1){ echo '<a href="javascript:;" class="btn btn-xs yellow">Confirmed</a>
';} else if($key->admin_status == 2) { echo '<a href="javascript:;" class="btn btn-xs green">Completed</a>';} else if($key->admin_status == 3) { echo '<a href="javascript:;" class="btn btn-xs red">Cancelled</a>';} ?></center></td>
                                            <td><center><select class="form-control" onchange="changestatus('<?php echo $key->booking_id; ?>',this.value)">
                                                        <option value="">Select</option>
                                                        <option value="2"<?php if($key->admin_status == 0){ echo 'disabled';}else if($key->admin_status == 2){ echo 'selected';} ?>>Completed</option>
                                                        </select>
                                                </center>
                                             </td>
                                            </tr>
                                            <?php  
                                            } }
                                          else
                                          {?>
                                         <tr class="even pointer">
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ><center><?php echo "Record not found";?></center></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                         </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">Update Price</h4>
                      </div>
                     <!--  <form method="post">  -->
                      <input type="hidden" name="service_id" id="service_id" value="">
                      <div class="modal-body"> 
                      <div class="form-group" style="margin-left: 65px;">
                           <div class="col-md-8">
                               <input type="text" id="setval" class="form-control" name="price" value="">
                           </div>
                           <div class="col-md-4">
                               <!-- <button type="button" class="btn green">Update</button> -->
                               <input type="submit" name="submit" id="submit" class="btn green" value="Update">
                           </div>
                           <span id="err" style="margin-left: 18px;list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></span>
                      </div>
                      </div>
                    <!--</form> -->
                      <div class="modal-footer">
                         
                      </div>
                  </div>
              </div>
            </div>
        </div>
      <?php $this->load->view("admin/footer");?>
    </body>
<script type="text/javascript">
  function changestatus(booking_id,status)
  {  
      var str = "id="+booking_id+"&status="+status;
      //alert(str);
      var r = confirm('Are you really want to change status?');
      if(r==true)
      {
          $.ajax({
            type:"POST",
             url:"<?php echo base_url('Appointment_request/change_status')?>/",
             data:str,
             success:function(data)
             {   
                 if(data==1000)
                 {
                      location.reload();
                 }
             }
          });
      }
  }
</script>
</html>