<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
         <style>
        .error{
            color:#ff3355;
        }
        </style>
        <meta charset="utf-8" />
        <title>Edit|Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
       
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
      
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />

        <!--==partley css==-->
        <link href="<?php echo base_url();?>template/assets/global/css/parsley.css" rel="stylesheet">  
       
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar');?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                   
                   <!--  <div class="page-head">
                      
                        <div class="page-title">
                            <h1>Edit Doctor Profile 
                                
                            </h1>
                        </div>
                       
                    </div> -->
                    <!-- <ul class="page-breadcrumb breadcrumb">
                     
                    </ul> -->
                  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Doctor Profile</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>


           <?php if(!empty($doctor_data)){ ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate=''>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Doctor Name<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="text" placeholder="Doctor Name" name="Doctor_name" class="form-control" value="<?php echo $doctor_data->Name;?>" required/>
                             <?php echo form_error('Doctor_name', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Email<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="text" name="email" placeholder="Email Address" class="form-control" value="<?php echo $doctor_data->Email;?>" data-parsley-type="email" required/>
                            <?php echo form_error('email', "<span class='error'>", "</span>"); ?>
                            <?php if($this->session->flashdata('error')){ echo "<div style='color:red;'>",$this->session->flashdata('error'),"</div>"; }?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">DOB<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="text" id="datepicker" name="DOB" placeholder="Date of Birth" class="form-control" value="<?php $date = str_replace('/', '-', $doctor_data->DOB); echo date('d-m-Y', strtotime($date));?>" required/>
                             <?php echo form_error('DOB', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Contact No.<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="text" name="contact_no" placeholder="Contact No" data-parsley-type="digits" class="form-control" value="<?php echo $doctor_data->Phone_No;?>" required/>
                             <?php echo form_error('contact_no', "<span class='error'>", "</span>"); ?>
                              <?php if($this->session->flashdata('error_phone')){ echo "<div style='color:red;'>",$this->session->flashdata('error_phone'),"</div>"; }?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Profile Pictue<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="file" name="profile_pic" placeholder="Profile pic" class="form-control" value="" />

                           <?php  if($doctor_data->Profile_Pic) 
                                  {
                                       $image = $doctor_data->Profile_Pic;
                                  }
                                  else
                                  {
                                        $image = 'image-not-found.gif';
                                  } ?>
                             <img src="<?php echo base_url('uploads/doctor_pic/'.$image);?>" height="70px" width="70px">
                            <?php if($this->session->flashdata('error_pic')){ echo "<div style='color:red;'>",$this->session->flashdata('error_pic'),"</div>"; }?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Address<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="text" name="address" placeholder="Address" class="form-control" value="<?php echo $doctor_data->Address;?>"  required/>
                             <?php echo form_error('address', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Appointment Contact No.</label>
                        <div class="col-md-9">
                            <input type="text" name="appointment_contact" data-parsley-type="digits" placeholder="Appointment Contact No" class="form-control" value="<?php echo $doctor_data->Appoint_Book_Num;?>" required/>
                             <?php echo form_error('appointment_contact', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Emergency Contact No.</label>
                        <div class="col-md-9">
                            <input type="text" name="emergency_contact" data-parsley-type="digits" placeholder="Emergency Contact No" class="form-control" value="<?php echo $doctor_data->Emergency_contact;?>"/>
                             <?php echo form_error('emergency_contact', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">MICR NO<span class="required"> * </span></label>
                        <div class="col-md-9">
                            <input type="text" name="micr_no" data-parsley-type="digits" placeholder="MICR NO" class="form-control" value="<?php echo $doctor_data->MICR_NO;?>" required/>
                             <?php echo form_error('appointment_contact', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Degree</label>
                        <div class="col-md-9">
                            <input type="text" name="degree" placeholder="Degree" class="form-control" value="<?php echo $doctor_data->Degree;?>" required/>
                             <?php echo form_error('degree', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" value="Submit">
                            <a href="<?php echo base_url()?>doctor/edit_profile"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            <?php } ?>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
      
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/js/parsley.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
       
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
       
        <script src="<?php echo base_url()?>template/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
      
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/ckeditor/ckeditor.js"></script>
       
<script type="text/javascript">
  $('#form11').parsley();  
</script>
<script>
 $( function() {
   $( "#datepicker" ).datepicker({
     dateFormat : 'dd/mm/yy',
     changeMonth: true,
      maxDate: '-1d'

   });
 } );
 </script>
        
    </body>

</html>