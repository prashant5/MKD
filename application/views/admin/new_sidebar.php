 <div class="page-sidebar-wrapper">
    
    <div class="page-sidebar navbar-collapse collapse">
        
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item">
                <a href="<?php echo base_url('user')?>" class="nav-link ">
                    <i class="icon-diamond"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
             <li class="nav-item">
                <a href="#" class="nav-link ">
                    <i class="glyphicon glyphicon-user"></i>
                    <span class="title">user</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="<?php echo base_url('user/verified')?>" class="nav-link ">
                            <span class="title">Verified User</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('user/unverified')?>" class="nav-link ">
                            <span class="title">Unverified User</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>