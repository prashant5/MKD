<!DOCTYPE html>
<html lang="en">
    <head>
    <link href="<?php echo base_url()?>template/assets/global/css/jquery.multiselect.css" rel="stylesheet" type="text/css">
    <?php $this->load->view("admin/head.php"); ?>
    <style type="text/css">
        .label {
            color: #190606;
        }
            
    </style>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
           <?php $this->load->view('admin/new_sidebar1'); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Manage Menu</div>
                                            </div>
        <div class="portlet-body form">
        <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('menu/manage_menu');?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Main Category<span class="required">*</span></label>
                        <div class="col-md-7">
                            <select class="form-control" name="category" data-parsley-required-message="Category selection is required" required>
                                <option value="">Select Category</option>
                                <option value="1">BREAKFAST</option>
                                <option value="2">LUNCH</option>
                                <option value="3">DINNER</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Day<span class="required">*</span></label>
                        <div class="col-md-7">
                            <select class="form-control" name="week_name" data-parsley-required-message="Category selection is required" required>
                                <option value="">Select Category</option>
                                <option value="1">Monday</option>
                                <option value="2">Tuesday</option>
                                <option value="3">Wednesday</option>
                                <option value="4">Thusday</option>
                                <option value="5">Friday</option>
                                <option value="6">Saturday</option>
                                <option value="7">Sunday</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Product<span class="required">*</span></label>
                        <div class="col-md-7">
                            <select name="langOptgroup[]" class="form-control" multiple id="langOptgroup" required>
                                <?php 
                                    if(!empty($products))
                                    {
                                        foreach ($products as $key) { 
                                            ?>
                                            <optgroup label="<?php echo $key->CategoryName; ?>">
                                                <?php
                                                    if(!empty($key->proname))
                                                    {
                                                        $pro_name = $key->proname;
                                                        $pro_id = $key->proid;
                                                        $explode = explode(',', $pro_name);
                                                        $explodeid = explode(',', $pro_id);
                                                        for ($i=0; $i < count($explode) ; $i++) { ?>
                                                            <option value="<?php echo $explodeid[$i]; ?>"><?php echo $explode[$i]; ?></option>
                                                        <?php }
                                                    }
                                                ?>
                                            </optgroup>
                                        <?php }
                                    }
                                ?>
                            </select>
                            <span data-parsley-required-message="Category selection is required"> </span>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit" >&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="javascript:history.go(-0)"><button type="button" class="btn default">Clear</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
            </div>
                </div>
                    </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>
    </body>
    <!-- <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js"></script> -->
    <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.multiselect.js"></script>
    <script type="text/javascript">
       $('#langOptgroup').multiselect({
            columns: 4,
            placeholder: 'Select Languages',
            search: true,
            selectAll: true
        });
    </script>
</html>