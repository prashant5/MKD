<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
           <?php $this->load->view('admin/new_sidebar1'); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Employe</div>
                                            </div>
        <div class="portlet-body form">
           <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">


                    <?php if($this->session->userdata('level') == 1){ ?>
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Restaurant<span class="required">*</span></label>
                        <div class="col-md-7">
                            <select class="form-control" name="store_id" required data-parsley-required-message="Restaurant Name is required">

                                    <option value="">Select</option>
                                    <?php $stores = $this->db->query("SELECT * FROM `Store` WHERE `Orgadmin_id` = ".$this->session->userdata('admin_id')."")->result(); 
                                     foreach($stores as $store)
                                     { ?>
                                        <option value="<?php echo $store->Id;?>"><?php echo $store->Name;?></option>
                                    <?php  }   ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="form-group">
                        <label class="control-label col-md-3">First Name<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="First Name" name="first_name" class="form-control" data-parsley-required-message="First Name is required" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Last Name<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Last Name" name="last_name" class="form-control" data-parsley-required-message="Last Name is required" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Employe Title<span class="required">*</span></label>
                        <div class="col-md-7">
                            <select class="form-control" name="EmpTitleId">
                                     <option value="">Select Title</option>
                                    <?php foreach($employetitle as $title){ ?>
                                      <option value="<?php echo $title->Id;?>"><?php echo $title->TitleName;?></option>
                                    <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Email<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Email" name="emailid" class="form-control" data-parsley-required-message="Last Name is required" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Image</label>
                        <div class="col-md-7">
                            <input type="file" name="image" class="form-control">
                             <span style="color:red;margin-left:440px;list-style-type: none;font-size: 0.9em;line-height: 0.9em;"><?php if($this->session->flashdata('error_error')){ echo $this->session->flashdata('error_error'); }?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Mobile<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Mobile" id="mobile_no" name="mobileno" class="form-control" data-parsley-required-message="Last Name is required" required/>
                            <div id="err" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;"></div>

                        </div>
                    </div>
                   
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit" >
                            <a href="javascript:history.go(-1)"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>
    </body>

</html>
<script>
$(document).ready(function(){
    $("#submit").click(function(){ 
        var mobile_no = $("#mobile_no").val();
        var str = 0;
             $.ajax({
                    type:'POST',
                    url: '<?php echo base_url();?>employe/check_mobile',
                    data: "mobile_no="+mobile_no,
                    async: false,
                    success: function(data){
                        if(data==10000)
                        {
                          str = 1;  
                          $("#err").html('Mobile No. already exists').css('color','red');
                        }
                        else
                        { 
                          $("#err").html('');
                        }

                    },
        
                });
             if(str == 1)
             {
                setTimeout(function(){ 
                 $('#err').html('');
                 },4000);
                return false;
             }   
           
        });
});

</script>   